#include <gtest/gtest.h>

#include <tcle/core/utils.h>
#include <tcle/ccl/hawick.h>
#include <tcle/ccl/stava.h>
#include <tcle/ccl/kalentev.h>
#include <tcle/ccl/jung.h>
#include <tcle/ccl/tcle.h>

namespace lcg {
namespace tcle {
namespace testing {

template <typename T>
class ccl_algorithm_test :
		public ::testing::Test/*WithParam<ThreadLayout>*/,
		public fit_threads<label_image::width, label_image::height> {

protected:
	virtual ~ccl_algorithm_test() {}
};

TYPED_TEST_CASE_P(ccl_algorithm_test);

TYPED_TEST_P(ccl_algorithm_test, BlankimageTest) {
	pinned<binary_image> binary;
	memset(&binary, binary_image::BACKGROUND, sizeof(binary_image));
	static hosted<label_image> expected;
	memset(&expected, label_image::BACKGROUND, sizeof(label_image));

	TypeParam labeler(this->grid, this->block);

	static hosted<label_image> img = labeler.ccl(binary);
	cudaDeviceSynchronize();

	ASSERT_EQ(0, memcmp(expected->data, img->data, sizeof(label_image)));
}

TYPED_TEST_P(ccl_algorithm_test, WholeimageTest) {
	pinned<binary_image> binary;
	memset(&binary, 0, sizeof(binary_image));
//	static hosted<label_image> expected; expected.memset(0);

	TypeParam labeler(this->grid, this->block);

	static hosted<label_image> img = labeler.ccl(binary);
	cudaDeviceSynchronize();

	const label_image::type mask = label_image::NO_FLAGS;

	for (int y = 0; y < label_image::height; y++)
		for (int x = 0; x < label_image::width; x++)
			ASSERT_EQ(0, (*img)(x, y) & mask);

	//	ASSERT_EQ(0, memcmp(expected->data, img->data, sizeof(label_image)));
}

TYPED_TEST_P(ccl_algorithm_test, CenteredrectangleTest) {
	pinned<binary_image> binary;
	memset(&binary, binary_image::BACKGROUND, sizeof(binary_image));
//	static hosted<label_image> expected; expected.memset(label_image::BACKGROUND);

	const int width  = label_image::width / 2;
	const int height = label_image::height / 2;
	const int x0 = (label_image::width - width ) / 2;
	const int y0 = (label_image::height - height) / 2;
	const label_image::type label = x0 + y0 * label_image::width;

	for (int y = y0; y < height; y++)
		for (int x = x0; x < width; x++) {
			(*binary)(x, y) = 0;
//			expected->x, y) = label;
		}

	TypeParam labeler(this->grid, this->block);

	static hosted<label_image> img = labeler.ccl(binary);
	cudaDeviceSynchronize();

	const label_image::type mask = label_image::NO_FLAGS;

	for (int y = y0; y < height; y++)
		for (int x = x0; x < width; x++)
			ASSERT_EQ(label, (*img)(x, y) & mask);

//	ASSERT_EQ(0, memcmp(expected->data, img->data, sizeof(label_image)));
}

REGISTER_TYPED_TEST_CASE_P(ccl_algorithm_test,
		BlankimageTest,
		WholeimageTest,
		CenteredrectangleTest
);

typedef ::testing::Types<
		hawick  ::global_propagation,
		hawick  ::local_propagation,
		hawick  ::directional_propagation,
		hawick  ::label_equivalence,
		stava   ::tile_merging,
		jung    ::label_equivalence,
		kalentev::label_equivalence
> Traditionalccl_algorithms;

typedef ::testing::Types<
		tcle::label_equivalence,
		tcle::time_coherent_label_equivalence
> all_ccl_props_algorithms;

typedef ::testing::Types<
		hawick  ::global_propagation,
		hawick  ::local_propagation,
		hawick  ::directional_propagation,
		hawick  ::label_equivalence,
		stava   ::tile_merging,
		jung    ::label_equivalence,
		kalentev::label_equivalence,

		tcle::label_equivalence,
		tcle::time_coherent_label_equivalence
> all_ccl_algorithms;

INSTANTIATE_TYPED_TEST_CASE_P( , ccl_algorithm_test, all_ccl_algorithms);
//INSTANTIATE_TYPED_TEST_CASE_P( , ccl_algorithm_test, all_ccl_props_algorithms);

} /* namespace testing */
} /* namespace tlce*/
} /* namespace lcg */
