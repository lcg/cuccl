#include <gtest/gtest.h>

#include <tcle/core/wrappers.h>
#include <tcle/core/image.h>
#include <tcle/core/utils.h>
#include <tcle/core/vector.h>
#include <tcle/meta/fit_threads.h>
#include <tcle/ccl/tcle.h>

namespace lcg {
namespace tcle {
namespace testing {

class error_ellipse_test :
		public ::testing::TestWithParam<rectangle> {
protected:

	virtual ~error_ellipse_test() {}

	virtual void setUp() {
	}
};

/**
 *
 */
//TEST_P(CovarianceMatrixDiagTest, Aligned_rectangle_Correctness_Test) {
//	const rectangle rect = TestWithParam<rectangle>::GetParam();
//	const int x0 = rect.x, y0 = rect.y;
//
//	static lcg::label_equivalence::labels_and_props result = tradccl_algorithm.cclprops(binary[0]);
//	props  = result.second;
//	cudaDeviceSynchronize();
//
////	lcg::error_ellipse covmat(props->variance(x0, y0),
////			props->centroid(x0, y0), props->area(x0, y0));
//
////	lcg::error_ellipse covmat(
////			make_ulonglong3(),
////			make_ulonglong2(),
////			rect.area()
////	);
//
//	int area = props->area(x0, y0);
//	area = (area == 0) ? 1 : area;
////	ulonglong2 centroid = props->centroid(x0, y0) / area;
////	ulonglong3 variance = props->variance(x0, y0) / area;
////	variance = variance / area;
////	variance.x -= sqr(centroid.x);
////	variance.y -= sqr(centroid.y);
////	variance.z -= centroid.x * centroid.y;
//
//	ASSERT_EQ      (rect.area()      , area      );
////	ASSERT_FLOAT_EQ(rect.centroid().x, centroid.x);
////	ASSERT_FLOAT_EQ(rect.centroid().y, centroid.y);
////	ASSERT_FLOAT_EQ(rect.variance().x, variance.x);
////	ASSERT_FLOAT_EQ(rect.variance().y, variance.y);
////	ASSERT_FLOAT_EQ(0                , variance.z);
//
////	ASSERT_FLOAT_EQ(rect.variance().z, props->variance(x0, y0).z);
//
////	ASSERT_FLOAT_EQ(variance.x / area - sqr(centroid.x / area), covmat.covmatrix[0][0]);
////	ASSERT_FLOAT_EQ(0.0f                                      , covmat.covmatrix[0][1]);
////	ASSERT_FLOAT_EQ(0.0f                                      , covmat.covmatrix[1][0]);
////	ASSERT_FLOAT_EQ(variance.y / area - sqr(centroid.y / area), covmat.covmatrix[1][1]);
//
////	ASSERT_FLOAT_EQ(covmat.eigenval[0], covmat.covmatrix[0][0]);
////	ASSERT_FLOAT_EQ(covmat.eigenval[1], covmat.covmatrix[1][1]);
//
////	ASSERT_FLOAT_EQ(covmat.eigenvec[0].x, 1);
////	ASSERT_FLOAT_EQ(covmat.eigenvec[0].y, 0);
////	ASSERT_FLOAT_EQ(covmat.eigenvec[1].x, 0);
////	ASSERT_FLOAT_EQ(covmat.eigenvec[1].y, 1);
//}
//
//INSTANTIATE_TEST_CASE_P(FirstTest, CovarianceMatrixDiagTest, ::testing::Values(
//		rectangle(  10, 4, 0, 0),
//		rectangle(  20, 4, 0, 0),
//		rectangle(  40, 4, 0, 0),
//		rectangle(  80, 4, 0, 0),
//		rectangle( 160, 4, 0, 0),
//		rectangle( 320, 4, 0, 0),
//		rectangle( 640, 4, 0, 0),
//		rectangle(1080, 4, 0, 0),
//
//		rectangle(  10, 320, 500, 500),
//		rectangle(  20, 320, 500, 500),
//		rectangle(  40, 320, 500, 500),
//		rectangle(  80, 320, 500, 500),
//		rectangle( 160, 320, 500, 500),
//		rectangle( 320, 320, 500, 500),
//		rectangle( 640, 320, 500, 500),
////		rectangle(1080, 320, 500, 500),
//
////		rectangle(  10, 4, 15, 15),
////		rectangle(  20, 4, 15, 15),
////		rectangle(  40, 4, 15, 15),
////		rectangle(  80, 4, 15, 15),
////		rectangle( 160, 4, 15, 15),
////		rectangle( 320, 4, 15, 15),
////		rectangle( 640, 4, 15, 15),
////		rectangle(1080, 4, 15, 15),
////
////		rectangle(  10, 4, 500, 500),
////		rectangle(  20, 4, 500, 500),
////		rectangle(  40, 4, 500, 500),
////		rectangle(  80, 4, 500, 500),
////		rectangle( 160, 4, 500, 500),
////		rectangle( 320, 4, 500, 500),
////		rectangle( 640, 4, 500, 500),
////		rectangle(1080, 4, 500, 500),
//
//		rectangle(4,   10, 0, 0),
//		rectangle(4,   20, 0, 0),
//		rectangle(4,   40, 0, 0),
//		rectangle(4,   80, 0, 0),
//		rectangle(4,  160, 0, 0),
//		rectangle(4,  320, 0, 0),
//		rectangle(4,  640, 0, 0),
//		rectangle(4, 1080, 0, 0)
//
////		rectangle(4,   10, 500, 500),
////		rectangle(4,   20, 500, 500),
////		rectangle(4,   40, 500, 500),
////		rectangle(4,   80, 500, 500),
////		rectangle(4,  160, 500, 500),
////		rectangle(4,  320, 500, 500),
////		rectangle(4,  640, 500, 500),
////		rectangle(4, 1080, 500, 500)
//));

} /* namespace testing */
} /* namespace tlce*/
} /* namespace lcg */
