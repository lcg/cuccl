#include <gtest/gtest.h>

#include <tcle/meta/fit_threads.h>
#include <tcle/core/ccl.h>

namespace lcg {
namespace tcle {
namespace testing {

class fit_threads_Test :
		public ::testing::Test,
		public fit_threads<label_image::width, label_image::height> {
};

TEST_F(fit_threads_Test, Grid_and_block_dimensions_divide_image_dimensions_evenly) {
	const int bx = label_image::width  % block.x;
	const int by = label_image::height % block.y;
	const int width  = label_image::width;
	const int height = label_image::height;

	ASSERT_EQ(0, bx);
	ASSERT_EQ(0, by);

	ASSERT_EQ(width , grid.x * block.x);
	ASSERT_EQ(height, grid.y * block.y);
}

} /* namespace testing */
} /* namespace tlce*/
} /* namespace lcg */
