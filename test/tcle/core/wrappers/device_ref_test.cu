#include <tcle/core/wrappers.h>
#include <tcle/core/cuda_exception.h>
#include <gtest/gtest.h>

namespace {

__global__
void cmpeq(const int *address, int value, bool *flag) {
	if (address[threadIdx.x] != value)
		*flag = false;
}

__global__
void add(int *base, int increment) {
	base[threadIdx.x] += increment;
}

}

namespace lcg {
namespace tcle {
namespace testing {

template <typename Type>
class device_ref_test : public ::testing::Test {
};

typedef ::testing::Types<int> types;
TYPED_TEST_CASE(device_ref_test, types);

TYPED_TEST(device_ref_test, copy_constructible_from_TypeParamPointer) {
	TypeParam expected = 42;
	TypeParam *device_buffer;
	bool *flag;

	CUDA_FATAL( cudaMallocHost((void**) &flag, sizeof(bool)) );
	CUDA_FATAL( cudaMalloc((void**)&device_buffer, sizeof(TypeParam)) );
	CUDA_FATAL( cudaMemcpy(device_buffer, &expected, sizeof(expected), cudaMemcpyHostToDevice) );

	*flag = true;
	device_ref<TypeParam> variable = device_buffer;
	cmpeq<<<1,1>>>(&variable, expected, flag);
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_TRUE(*flag);

	CUDA_FATAL( cudaFreeHost(flag) );
}

TYPED_TEST(device_ref_test, copy_constructible_from_pinned) {
	const TypeParam expected = 42;
	pinned  <TypeParam> buffer   = expected;

	device_ref<TypeParam> variable = buffer;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_ref_test, copy_constructible_from_device) {
	const TypeParam expected = 42;
	device<TypeParam> buffer = expected;

	device_ref<TypeParam> variable = buffer;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_ref_test, copy_constructible_from_device_ref) {
	const TypeParam expected = 42;
	device<TypeParam> buffer = expected;
	device_ref<TypeParam> other  = buffer;

	device_ref<TypeParam> variable = other;

	hosted<TypeParam> result = buffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

//TYPED_TEST(device_ref_test, copy_constructible_from_pinned) {
//	const TypeParam base = 40, increment = 2;
//	const TypeParam expected = base + increment;
//
//	pinned<TypeParam> buffer = base;
////	device_ref<TypeParam> variable = buffer;
//
////	add<<<1,1>>>(&variable, increment);
//	hosted<TypeParam> result = buffer;
//	CUDA_FATAL( cudaDeviceSynchronize() );
//	ASSERT_EQ(expected, result);
//}
//
//TYPED_TEST(device_ref_test, copy_constructible_from_device) {
//	const TypeParam base = 40, increment = 2;
//	const TypeParam expected = base + increment;
//
//	device<TypeParam> buffer = base;
//	device_ref<TypeParam> variable = buffer;
//
//	add<<<1,1>>>(&variable, increment);
//	hosted<TypeParam> result = buffer;
//	CUDA_FATAL( cudaDeviceSynchronize() );
//	ASSERT_EQ(expected, result);
//}
//
//TYPED_TEST(device_ref_test, copy_constructible_from_device_ref) {
//	const TypeParam base = 40, increment = 2;
//	const TypeParam expected = base + increment;
//
//	device<TypeParam> buffer = base;
//	device_ref<TypeParam> intermediate = buffer;
//	device_ref<TypeParam> variable = intermediate;
//
//	add<<<1,1>>>(&variable, increment);
//	hosted<TypeParam> result = buffer;
//	CUDA_FATAL( cudaDeviceSynchronize() );
//	ASSERT_EQ(expected, result);
//}

TYPED_TEST(device_ref_test, assignable_from_TypeParam) {
	TypeParam expected = 42;
	device<TypeParam> buffer = 33;

	device_ref<TypeParam> variable = buffer;
	variable = hosted<TypeParam>(expected);

	hosted<TypeParam> result = buffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_ref_test, assignable_from_hosted) {
	hosted<TypeParam> expected = 42;
	device<TypeParam> buffer = 33;

	device_ref<TypeParam> variable = buffer;
	variable = expected;

	hosted<TypeParam> result = buffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_ref_test, assignable_from_pinned) {
	pinned<TypeParam> expected = 42;
	device<TypeParam> buffer = 33;

	device_ref<TypeParam> variable = buffer;
	variable = expected;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_ref_test, assignable_from_device) {
	TypeParam unexpected = 33, expected = 42;
	device<TypeParam> buffer = unexpected;
	device<TypeParam> otherbuffer  = expected;

	device_ref<TypeParam> variable = buffer;
	variable = otherbuffer;

	hosted<TypeParam> result = buffer;
	hosted<TypeParam> otherresult = otherbuffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
	ASSERT_EQ(expected, otherresult);
}

TYPED_TEST(device_ref_test, assignable_from_device_ref) {
	TypeParam unexpected = 33, expected = 42;
	device<TypeParam> buffer = unexpected;
	device<TypeParam> otherbuffer = expected;
	device_ref<TypeParam> intermediate = otherbuffer;

	device_ref<TypeParam> variable = buffer;
	variable = intermediate;

	hosted<TypeParam> result = buffer;
	hosted<TypeParam> otherresult = otherbuffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
	ASSERT_EQ(expected, otherresult);
}

} /* namespace testing */
} /* namespace tcle */
} /* namespace lcg */
