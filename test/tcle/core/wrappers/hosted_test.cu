#include <tcle/core/wrappers.h>
#include <gtest/gtest.h>

namespace lcg {
namespace tcle {
namespace testing {

template <class Type>
class hosted_test : public ::testing::Test {};

TYPED_TEST_CASE_P(hosted_test);

TYPED_TEST_P(hosted_test, compatible_with_numeric_TypeParam) {
	const TypeParam answer = 6;
	hosted<TypeParam> a = 5;
	a = answer;
	TypeParam b = a;
	ASSERT_EQ(answer, b);
}

TYPED_TEST_P(hosted_test, default_constructor_calls_that_of_TypeParam) {
	TypeParam answer = TypeParam();
	hosted<TypeParam> variable;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(hosted_test, copy_constructible_from_TypeParam) {
	TypeParam answer = 7;
	hosted<TypeParam> variable = answer;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(hosted_test, copy_constructible_from_hosted) {
	TypeParam answer = 7;
	hosted<TypeParam> other = answer;
	hosted<TypeParam> variable = other;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(hosted_test, copy_constructible_from_pinned) {
	TypeParam answer = 7;
	pinned<TypeParam> other = answer;
	hosted<TypeParam> variable = other;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(hosted_test, copy_constructible_from_device) {
	TypeParam answer = 7;
	device<TypeParam> other = answer;
	hosted  <TypeParam> variable = other;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(hosted_test, copy_constructible_from_device_ref) {
	TypeParam answer = 7;
	device<TypeParam> buffer = answer;
	device_ref<TypeParam> other = buffer;
	hosted  <TypeParam> variable = other;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(hosted_test, assignable_from_TypeParam) {
	TypeParam expected = 7, wrong = 3;
	hosted<TypeParam> variable = wrong;

	variable = expected;

	ASSERT_EQ(expected, variable);
}

TYPED_TEST_P(hosted_test, assignable_from_hosted) {
	const TypeParam expected = 42, wrong = 33;
	hosted<TypeParam> other = expected;
	hosted<TypeParam> variable = wrong;

	variable = other;

	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, variable);
}

TYPED_TEST_P(hosted_test, assignable_from_pinned) {
	const TypeParam expected = 42, wrong = 33;
	pinned<TypeParam> other = expected;
	hosted<TypeParam> variable = wrong;

	variable = other;

	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, variable);
}

TYPED_TEST_P(hosted_test, assignable_from_device) {
	const TypeParam expected = 42, wrong = 33;
	device<TypeParam> other = expected;
	hosted  <TypeParam> variable = wrong;

	variable = other;

	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, variable);
}

TYPED_TEST_P(hosted_test, assignable_from_device_ref) {
	const TypeParam expected = 42, wrong = 33;
	device<TypeParam> buffer = expected;
	device_ref<TypeParam> other = buffer;
	hosted  <TypeParam> variable = wrong;

	variable = other;

	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, variable);
}

REGISTER_TYPED_TEST_CASE_P(hosted_test,
	compatible_with_numeric_TypeParam,
	default_constructor_calls_that_of_TypeParam,
	copy_constructible_from_TypeParam,
	copy_constructible_from_hosted,
	copy_constructible_from_pinned,
	copy_constructible_from_device,
	copy_constructible_from_device_ref,
	assignable_from_TypeParam,
	assignable_from_hosted,
	assignable_from_pinned,
	assignable_from_device,
	assignable_from_device_ref
);

typedef ::testing::Types<int, float> types;
INSTANTIATE_TYPED_TEST_CASE_P(, hosted_test, types);

} /* namespace testing */
} /* namespace tcle */
} /* namespace lcg */
