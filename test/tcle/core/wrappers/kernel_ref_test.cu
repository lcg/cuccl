#include <tcle/core/wrappers.h>
#include <gtest/gtest.h>

namespace {

template <class Type>
__global__
void put_value(lcg::tcle::kernel_ref<Type> buffer, Type value) {
	buffer = value;
}

template <class Type, template <class> class Wrapper>
__global__
void put_value(lcg::tcle::kernel_ref<Type> buffer, Wrapper<Type> value) {
	buffer = value;
}

}

namespace lcg {
namespace tcle {
namespace testing {

class kernel_ref_test : public ::testing::Test {
};

TEST(kernel_ref_test, put_value) {
	const int expected = 42;
	device<int> buffer = 33;

	put_value<<<1, 1>>>(kernel_ref<int>(&buffer), expected);
	hosted<int> result = buffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TEST(kernel_ref_test, copy_constructible_from_device) {
	const int expected = 42;
	device<int> buffer = 33;

	put_value<<<1, 1>>>(kernel_ref<int>(buffer), expected);
	hosted<int> result = buffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TEST(kernel_ref_test, copy_constructible_from_device_ref) {
	const int expected = 42;
	device<int> buffer = 33;
	device_ref<int> other = buffer;

	put_value<<<1, 1>>>(kernel_ref<int>(other), expected);
	hosted<int> result = other;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TEST(kernel_ref_test, copy_constructible_from_pinned) {
	const int expected = 42;
	pinned<int> buffer = 33;

	put_value<<<1, 1>>>(kernel_ref<int>(buffer), expected);
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, buffer);
}

TEST(kernel_ref_test, copy_constructible_from_kernel_ref) {
	const int expected = 42;
	device<int> buffer = 33;
	kernel_ref<int> other = buffer;

	put_value<<<1, 1>>>(kernel_ref<int>(other), expected);
	hosted<int> result = buffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TEST(kernel_ref_test, assignable_from_kernel_ref_on_device) {
	const int expected = 42;
	device<int> buffer = 33;
	device<int> answer = expected;
	kernel_ref<int> other = answer;

	put_value<<<1, 1>>>(kernel_ref<int>(buffer), other);
	hosted<int> result = buffer;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

} /* namespace testing */
} /* namespace tcle */
} /* namespace lcg */
