#include <tcle/core/wrappers.h>
#include <gtest/gtest.h>

namespace lcg {
namespace tcle {
namespace testing {

template <class Type>
class pinned_test : public ::testing::Test {};

TYPED_TEST_CASE_P(pinned_test);

TYPED_TEST_P(pinned_test, compatible_with_numeric_TypeParam) {
	const TypeParam answer = 6;
	pinned<TypeParam> a = 5;
	a = answer;
	TypeParam b = a;
	ASSERT_EQ(answer, b);
}

TYPED_TEST_P(pinned_test, default_constructor_calls_that_of_TypeParam) {
	TypeParam answer;
	pinned<TypeParam> variable;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, copy_constructible_from_TypeParam) {
	TypeParam answer = 7;
	pinned<TypeParam> variable = answer;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, copy_constructible_from_hosted) {
	TypeParam answer = 7;
	hosted<TypeParam> intermediate = answer;
	pinned<TypeParam> variable = intermediate;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, copy_constructible_from_pinned) {
	TypeParam answer = 7;
	pinned<TypeParam> intermediate = answer;
	pinned<TypeParam> variable = intermediate;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, copy_constructible_from_device) {
	TypeParam answer = 7;
	device<TypeParam> intermediate = answer;
	pinned<TypeParam> variable = intermediate;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, copy_constructible_from_device_ref) {
	TypeParam answer = 7;
	device<TypeParam> intermediate1 = answer;
	device_ref<TypeParam> intermediate2 = intermediate1;
	pinned<TypeParam> variable = intermediate2;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, assignable_from_TypeParam) {
	TypeParam answer = 7, wrong = 3;
	pinned<TypeParam> variable = wrong;
	variable = answer;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, assignable_from_hosted) {
	TypeParam answer = 7, wrong = 3;
	pinned<TypeParam> variable = wrong;
	hosted<TypeParam> intermediate = answer;
	variable = intermediate;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, assignable_from_pinned) {
	TypeParam answer = 7, wrong = 3;
	pinned<TypeParam> variable = wrong, intermediate = answer;
	variable = intermediate;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, assignable_from_device) {
	TypeParam answer = 7, wrong = 3;
	pinned<TypeParam> variable = wrong;
	device<TypeParam> intermediate = answer;
	variable = intermediate;
	ASSERT_EQ(answer, variable);
}

TYPED_TEST_P(pinned_test, assignable_from_device_ref) {
	TypeParam answer = 7, wrong = 3;
	pinned<TypeParam> variable = wrong;
	device<TypeParam> intermediate1 = answer;
	device_ref<TypeParam> intermediate2 = intermediate1;
	variable = intermediate2;
	ASSERT_EQ(answer, variable);
}

REGISTER_TYPED_TEST_CASE_P(pinned_test,
	compatible_with_numeric_TypeParam,
	default_constructor_calls_that_of_TypeParam,
	copy_constructible_from_TypeParam,
	copy_constructible_from_hosted,
	copy_constructible_from_pinned,
	copy_constructible_from_device,
	copy_constructible_from_device_ref,
	assignable_from_TypeParam,
	assignable_from_hosted,
	assignable_from_pinned,
	assignable_from_device,
	assignable_from_device_ref
);

typedef ::testing::Types<int, float> types;
INSTANTIATE_TYPED_TEST_CASE_P(, pinned_test, types);

} /* namespace testing */
} /* namespace tcle */
} /* namespace lcg */
