# Class concepts {#concepts}

This section documents class concepts required by various template functions in the CuCCL library.
A _C++ concept_ is a formal requirement on the interface of class so as to restrict which classes are valid type parameters in templates. By defining conformant types, the user is allowed to write extensions that take advantage of existing template functions while customizing their behavior. One such example is the `findRoot` template function found in the `labeling.h` header: suppose that in a custom labeling algorithm, the numeric value of a label encodes extra information and hence, does not strictly corresponds to the offset of the corresponding root pixel in the image; by requiring, through a concept, a `root` function that calculates the real offset, the `findRoot` function is able to correctly calculate label offsets and becomes usable in custom algorithms (eventually, even in new ones). This customization through templates is an effective technique for code reuse, because it prevents implementing a `findRoot` function for every method that encodes labels in a different manner.

## Label wrapping concept

The _label wrapping concept_ is that of type that _wraps_ the `lcg::cuccl::label_t` primitive typedef and defines some operations commonly associated with labels in CCL algorithms, both in host and device code. _Wrapping_ in this context implies that the conformant type should

* Have the exact same size and memory aligment of the wrapped type (`lcg::cuccl::label_t` in this case)
* Have a single data member of the `lcg::cuccl::label_t` type (need not to be accessible)
* Be fully compatible with `lcg::cuccl::label_t` lvalues and rvalues, that is
    * It should be copy-constructible from any `lcg::cuccl::label_t` rvalue
    * It should be usable as a `lcg::cuccl::label_t` lvalue (target of assingment)
    * It should define a cast operator to `lcg::cuccl::label_t`

These requirements are usually satisfied by defining three specific methods:

* A constructor taking a `lcg::cuccl::label_t` rvalue
* A cast operator with signature `lcg::cuccl::label_t operator lcg::cuccl::label_t() const`
* A cast operator with signature `lcg::cuccl::label_t& operator lcg::cuccl::label_t()`

In the CuCCL library, conformant types are implemented by inheriting from the `lcg::cutils::type_wrapper<T>` template class, which produces a type conforming to these wrapping requirements by defining the three necessary methods for type `T`. Additionally, the `cutils::type_wrapper<T>` ensures that these three methods are defined as `__host__ __device__` whenever compiling in `nvcc`.

The extra operations that should be defined by a label wrapping conformant type are listed and explained below

    /**
     * A single constant value that indicates a background label.
     */
    __host__ __device__
	static constexpr lcg::cuccl::label_t background();

    /**
     * Extracts flag bits possibly encoded in the label's numeric value.
     */
	__host__ __device__
	lcg::cuccl::label_t flags() const;

    /**
     * Tests whether the label is a background label.
     */
	__host__ __device__
	bool isBackground() const;

    /**
     * Tests whether the label is a foreground label.
     */
	__host__ __device__
	bool isForeground() const;

    /**
     * Extracts the root pixel's linear index encoded in the label's numeric value.
     */
	__host__ __device__
	lcg::cuccl::label_t root() const;
	
The three conformant types actually defined in the library are `lcg::cuccl::basic_label`, used in all classic algorithms implemented, `lcg::cuccl::tcle_label` and `lcg::cuccl::tclle_label` (used in the methods proposed by Asad et al.).

## Segment wrapping concept

The _segment wrapping concept_ is very similar to the _label wrapping concept_ described previously, but it wraps the `lcg::cuccl::segment_t` primitive type, which is meant to encode per-pixel segments instead of labels. Since the current state of the library assumes binary images (that is, only two possible segments), there is only one conformant type (`lcg::cuccl::binary_segment`) and the set of extra required operations is as simple as

    __host__ __device__
	static constexpr segment_t background();

	__host__ __device__
	bool isBackground() const;

	__host__ __device__
	bool isForeground() const;
	
The current design presumes that foreground pixels are defined by exclusion: all values that do not qualify as background are foreground values.