#!/bin/bash

EARLY_RESOLUTIONS="1024 2048 4096 8192"
LATE_RESOLUTIONS="16384"
ALL_RESOLUTIONS="${EARLY_RESOLUTIONS} ${LATE_RESOLUTIONS}"

IMAGE_SCRIPTS="instancing leveling random_instancing random_merging scaling spiral subdivision"
RANDOM_IMAGE_SCRIPTS="random_instancing random_merging"
LEFT_IMAGE_SCRIPTS="leveling spiral random_instancing instancing"

VIDEO_SCRIPTS="segtrack davis pascal coco"

OTHER_SCRIPTS="memprof scanflag scanmodes seqprof"

HELP_TEXT=$(cat <<-END
Usage: ./$(basename $0) [-h] [-l 1|2|3] [-x ARGS...]\n
\n
Options:\n

  -h ....... Print help text and exit.\n
  -l [1|2|3] Layout type for ridge/spiral plots.\n
  -S ....... Skip sequential profiling, memory profiling, and scan modes/flag profiling.\n
  -V ....... Skip videos profiling.\n
  -x ARGS .. Extra plotting  options passed to ridge/spiral scripts.\n
END
)

IMAGE_SCRIPT_LAYOUT="1"
SKIP_OTHERS=0
SKIP_VIDEOS=0
while getopts l:hSVx: option
do
	case "${option}"
	in
		h) echo -e $HELP_TEXT; exit 0;;
		l) IMAGE_SCRIPT_LAYOUT="${OPTARG}";;
		S) SKIP_OTHERS=1;;
		V) SKIP_VIDEOS=1;;
		x) EXTRA_SCRIPT_OPTIONS=${OPTARG};;
	esac
done

case "${IMAGE_SCRIPT_LAYOUT}"
in
	1) SCRIPT_OPTIONS="--no-title --no-xlabels ${EXTRA_SCRIPT_OPTIONS}";;
	2) SCRIPT_OPTIONS="--no-xlabels ${EXTRA_SCRIPT_OPTIONS}";;
	3) SCRIPT_OPTIONS="--no-xlabels --figure-aspect=0.6 ${EXTRA_SCRIPT_OPTIONS}";;
esac

python2 -m cuccl.profile.plotting.legend --layout ${IMAGE_SCRIPT_LAYOUT} ${SCRIPT_OPTIONS}

if [[ ${SKIP_OTHERS} -eq 0 ]]
then
	for SCRIPT in ${OTHER_SCRIPTS}
	do
		python2 -m cuccl.profile.${SCRIPT} ${SCRIPT_OPTIONS}
	done
fi

for SCRIPT in ${IMAGE_SCRIPTS}
do
	if echo ${RANDOM_IMAGE_SCRIPTS} | tr ' ' '\n' | grep -q "^${SCRIPT}$"
	then
		ALL_OPTIONS="${SCRIPT_OPTIONS} -n50"
	else
		ALL_OPTIONS="${SCRIPT_OPTIONS}"
	fi

	if echo ${LEFT_IMAGE_SCRIPTS} | tr ' ' '\n' | grep -q "${SCRIPT}"
	then
		python2 -m cuccl.profile.patterns.${SCRIPT} -r ${EARLY_RESOLUTIONS} --layout ${IMAGE_SCRIPT_LAYOUT} ${ALL_OPTIONS}
	else
		python2 -m cuccl.profile.patterns.${SCRIPT} -r ${EARLY_RESOLUTIONS} --layout ${IMAGE_SCRIPT_LAYOUT} --no-ylabels ${ALL_OPTIONS}
	fi
done

for SCRIPT in ${IMAGE_SCRIPTS}
do
	if echo ${RANDOM_IMAGE_SCRIPTS} | tr ' ' '\n' | grep -q "^${SCRIPT}$"
	then
		ALL_OPTIONS="${SCRIPT_OPTIONS} -n10"
	else
		ALL_OPTIONS="${SCRIPT_OPTIONS}"
	fi

	if echo ${LEFT_IMAGE_SCRIPTS} | tr ' ' '\n' | grep -q "${SCRIPT}"
	then
		python2 -m cuccl.profile.patterns.${SCRIPT} -r ${LATE_RESOLUTIONS} -p late --layout ${IMAGE_SCRIPT_LAYOUT} ${ALL_OPTIONS}
	else
		python2 -m cuccl.profile.patterns.${SCRIPT} -r ${LATE_RESOLUTIONS} -p late --layout ${IMAGE_SCRIPT_LAYOUT} --no-ylabels ${ALL_OPTIONS}
	fi
done

if [[ ${SKIP_VIDEOS} -eq 0 ]]
then
	for VIDEO in ${VIDEO_SCRIPTS}
	do
		python2 -m cuccl.profile.videos -r 1024 2048 4096 8192 -v ${VIDEO}
		python2 -m cuccl.profile.videos -r 16384 -p late -v ${VIDEO}
	done

	python2 -m cuccl.profile.videos -r 1024 2048 4096 8192 -v ${VIDEO_SCRIPTS}
	python2 -m cuccl.profile.videos -r 16384 -p late -v ${VIDEO_SCRIPTS}
fi
