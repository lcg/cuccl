#!/bin/bash

EARLY_RESOLUTIONS="1024 2048 4096 8192"
LATE_RESOLUTIONS="16384"
ALL_RESOLUTIONS="${EARLY_RESOLUTIONS} ${LATE_RESOLUTIONS}"
VIDEOS="segtrack davis pascal coco"

for script in memprof scanflag scanmodes seqprof
do
	python2 -m cuccl.profile.$script -r ${ALL_RESOLUTIONS} 
done

for script in instancing leveling scaling spiral subdivision
do
	python2 -m cuccl.profile.patterns.$script -r ${EARLY_RESOLUTIONS} 
	python2 -m cuccl.profile.patterns.$script -r ${LATE_RESOLUTIONS} -p late 
done

python2 -m cuccl.profile.patterns.instancing  -r ${EARLY_RESOLUTIONS} 
python2 -m cuccl.profile.patterns.subdivision -r ${EARLY_RESOLUTIONS} 

python2 -m cuccl.profile.patterns.instancing  -r ${LATE_RESOLUTIONS} -p late 
python2 -m cuccl.profile.patterns.subdivision -r ${LATE_RESOLUTIONS} -p late 

for level in 4 3 2 5 1
do
	python2 -m cuccl.profile.patterns.subdivision -r ${EARLY_RESOLUTIONS} -l $level         
	python2 -m cuccl.profile.patterns.subdivision -r ${LATE_RESOLUTIONS}  -l $level -p late 
	for subdivision in 2 1 0
	do
		python2 -m cuccl.profile.patterns.instancing -r ${EARLY_RESOLUTIONS} -l $level --subdivision $subdivision         
		python2 -m cuccl.profile.patterns.instancing -r ${LATE_RESOLUTIONS}  -l $level --subdivision $subdivision -p late 
	done
done

python2 -m cuccl.profile.videos -r ${EARLY_RESOLUTIONS} -v ${VIDEOS} 
python2 -m cuccl.profile.videos -r ${LATE_RESOLUTIONS} -v ${VIDEOS} -p late 

for level in 4 3 2 5 1
do
	for subdivision in 3 2 1 0
	do
		python2 -m cuccl.profile.patterns.random_instancing -n50 -r ${EARLY_RESOLUTIONS} -l $level --subdivision $subdivision 
		python2 -m cuccl.profile.patterns.random_merging -n50    -r ${EARLY_RESOLUTIONS} -l $level --subdivision $subdivision 
	done
done

python2 -m cuccl.profile.patterns.random_instancing -n50 -r ${EARLY_RESOLUTIONS} 
python2 -m cuccl.profile.patterns.random_merging -n50    -r ${EARLY_RESOLUTIONS} 

for level in 4 3 2 5 1
do
	for subdivision in 3 2 1 0
	do
		python2 -m cuccl.profile.patterns.random_instancing -n10 -r ${LATE_RESOLUTIONS} -l $level --subdivision $subdivision -p late 
		python2 -m cuccl.profile.patterns.random_merging -n10    -r ${LATE_RESOLUTIONS} -l $level --subdivision $subdivision -p late 
	done
done

python2 -m cuccl.profile.patterns.random_instancing -n10 -r ${LATE_RESOLUTIONS} -p late 
python2 -m cuccl.profile.patterns.random_merging -n10    -r ${LATE_RESOLUTIONS} -p late 
