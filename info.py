#!/usr/bin/env python2.7

"""Utility script to obtain information about the project and repository state.

Run without arguments or with the --help switch to get usage information.
"""

import argparse
import functools
import os
import re
import subprocess
import sys


def get_branch():
	"""Retrieve the current repository branch."""
	branch = subprocess.check_output(['git', 'show-branch'])
	return re.match(r'^\[(.*)\]\s.*$', branch).group(1)


def get_name():
	"""Retrieve the project name."""
	pattern = r'PROJECT\(\s*(\w+[\w\d]*)'
	regex = re.compile(pattern, re.IGNORECASE)
	match = re.search(regex, cmake_contents)
	if match is None:
		return None
	else:
		return match.group(1)


def get_version(part='short'):
	"""Retrieve the project version.

	:param part: One of 'major', 'minor', 'patch', 'build', 'full', or 'short'
	"""
	pattern = r'PROJECT\(\s*\w+[\w\d]*\s+VERSION\s+(\d+(\.\d+){,3})'
	regex = re.compile(pattern, re.IGNORECASE)
	match = re.search(regex, cmake_contents)
	if match is None:
		return None
	else:
		components = match.group(1).split('.')
		components = components + ['0' for i in range(4-len(components))]
		if part == 'major':
			return components[0]
		elif part == 'minor':
			return components[1]
		elif part == 'patch':
			return components[2]
		elif part == 'build':
			return components[3]
		elif part == 'full':
			return match.group(1)
		elif part == 'short':
			return '.'.join(
				x for i, x in enumerate(components)
				if (int(x) > 0 or i < 2)
			)
		else:
			raise ValueError('Unknown part: "%s"' % part)


def parse_args():
	"""Parse command line arguments."""
	parser = argparse.ArgumentParser(
		description='%s project and repository information tool' % get_name()
	)
	parser.add_argument('command', 
		choices=commands.keys(), 
		help='Choose what project/repository information to display'
	)
	return parser.parse_args()


def main(args):
	"""Print requested information to standard output."""
	print(commands[args.command]())


try:
	root_dir = os.path.dirname(__file__)
	cmake_path = os.path.join(root_dir, 'CMakeLists.txt')
	cmake_file = open(cmake_path)
	cmake_contents = cmake_file.read().replace('\n', ' ')
except IOError as error:
	print('Could not open or read the "%s" file.' % cmake_path)
	print('Run "git status" to check repository state.')
	print('Here\'s the full error stack trace:')
	print(error)
	sys.exit(1)

commands = {
	'branch': get_branch,
	'name': get_name,
	'version': get_version,
	'version_build': functools.partial(get_version, part='build'),
	'version_full': functools.partial(get_version, part='full'),
	'version_major': functools.partial(get_version, part='major'),
	'version_minor': functools.partial(get_version, part='minor'),
	'version_patch': functools.partial(get_version, part='patch'),
	'version_short': functools.partial(get_version, part='short'),
}

if __name__ == '__main__':
	main(parse_args())

