CuCCL
=====

[![license-shield](https://img.shields.io/badge/license-GPL3.0-blue.svg)](https://www.gnu.org/licenses/gpl-3.0-standalone.html)
![version-shield](https://img.shields.io/badge/version-1.0-yellow.svg)

[![pipeline status](https://gitlab.com/lcg/cuccl/badges//pipeline.svg)](https://gitlab.com/lcg/cuccl/commits/)
[![coverage report](https://gitlab.com/lcg/cuccl/badges//coverage.svg)](https://gitlab.com/lcg/cuccl/commits/)

![cuda-shield](https://img.shields.io/badge/CUDA-9-blue.svg)
![c++-shield](https://img.shields.io/badge/C++-11-blue.svg)
![Python-shield](https://img.shields.io/badge/Python-2.7-blue.svg)

---

CuCCL is a C++/CUDA library that implements modified versions of two GPU connected component labeling algorithms for images: Hawick et al. 2010 [^1], and Stava et al. 2011 [^2].
It also contains Python scripts for evaluating the algorithms's performance using synthetic test patterns or (if available) segmented video sequences, as explained in the paper

	On GPU Connected Componentes and Properties: _A Systematic Evaluation of Connected Component Labeling Algorithms and their Extension for Property Extraction_
	Asad, Pedro and Marroquim, Ricardo and Souza, Andréa
	2018, IEEE Transactions on Image Processing (to be published)

This library was developed by students of [LCG](https://lcg.gitlab.io), the Computer Graphics Laboratory of the Federal University of Rio de Janeiro, under the advisory of Prof. Ricardo Marroquim.
If you have any suggestions, or would like to report bugs or have a better understanding of our code and methodology, don't hesitate to contact us at [pasad@ufrj.br](mailto:pasad@ufrj.br) or to open a new issue in the [issues board](https://gitlab.com/lcg/cuccl/issues).


# Dependencies

C++ Dependencies for the library and command-line interface:

* Boost 1.5
* CMake 3.5 (for building)
* CUDA toolkit 9.1
* OpenCV 2.4

Python 2.7 dependencies for profiling and generating plots:

* Matplotlib 2.2
* Numpy 1.14
* Scipy 1.0

If you whish to build the documentation, you'll need Doxygen 1.8.


# Installation

Just type

```bash
mkdir build
cd build  
cmake .. -DRESOLUTION=<resol>
make && make docs && make install
```

replacing `<resol>` with the desired resolution.
We plan on changing the library to support multiple resolutions dynamically, but right now, you have to pick a resolution for building.


## Architectures

The current version of CMake required is 3.5, but CUDA only became a core CMake-supported language starting at version 3.8 ([reference](https://devblogs.nvidia.com/building-cuda-applications-cmake/)).
It means that some functionality, like automatically detecting the architectures your GPU supports may not be available.
Hence, there is a `BUILD_ARCHS` variable in [`CMakeLists.txt`](CMakeLists.txt) that specifies which architectures to build against.
The default list includes almost all currently supported architectures, that is

* `compute_30`, `compute_32`, `compute_35`, `compute_37`
* `compute_50`, `compute_52`
* `compute_60`, `compute_61`
* `compute_70`, `compute_72`

You probably want to manually specify your GPU's architecture to save compilation time.
In order to specify a different list of architectures, pass a comma-separated list of numbers to the `BUILD_ARCHS` variable, as in

```bash
cmake .. -DRESOLUTION=2048 -DBUILD_ARCHS="30;61;72"
```

if you want to produce code for architectures `compute_30`, `compute_61`, and `compute_72`.


## Documentation

If CMake's `BUILD_DOCS` variable is true (the default, meaning you want to build the docs), `make install` will fail.
If this happens, you may either pass the `-DBUILD_DOCS=0` flag to CMake, or type `make docs` before `make install`, as showed in the first example of this session.


## Other issues

In some cases, the default C/C++ compiler may be incompatible with the latest version of CUDA available in the repositories.
In this case, manually point to a compatible compiler using the `CUDA_HOST_COMPILER` CMake variable.


# Usage

Check the [documentation page](https://lcg.gitlab.io/cuccl) for information on the C++ API.
The Python profiling tools are currently undocumented, but we plan to change it soon.

__Note:__ The library is still in alpha version, and a lot of things may change at this point.
However, we expect to release a stable version sometime later during this year.


# References

[^1]: Hawick, Kenneth A., Arno Leist, and Daniel P. Playne. "Parallel graph component labelling with GPUs and CUDA." Parallel Computing 36.12 (2010): 655-678.
[^2]: Št, Ondřej, and Bedřich Beneš. "Connected component labeling in CUDA." GPU computing gems emerald edition. 2011. 569-581.
