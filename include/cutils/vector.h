#ifndef LCG_CUTILS_VECTOR_H_
#define LCG_CUTILS_VECTOR_H_

#include <cutils/cuda_vector.h>
#include <cutils/utils.h>

#include <iostream>

#include <vector_types.h>
#include <vector_functions.h>

namespace lcg {
namespace cutils {

/**
 * \brief Enhances CUDA built-in vector types with additional capabilities.
 *
 * Parameters:
 * 	* S (integral type): an integral type, such as short, int, etc.
 * 	* N (1..4): number of vector components
 *
 * Provides copy constructors for corresponding CUDA vector types and main constructors accepting zero or more coordinates. Specializations are defined for each valid dimension (1..4).
 *
 * Every specialization shall define:
 * 	* \code{base_type}: A \code{typedef} for the corresponding CUDA built-in vector type
 * 	* \code{element_type}: A \code{typedef} for the underlying element type
 */
template <class S, size_t N>
struct vector {
};

template <class S, size_t N>
struct vector_base {
	__host__ __device__
	operator const S* () const {
		return (const S*) this;
	}

	__host__ __device__
	operator S* () {
		return (S*) this;
	}

	template <class T>
	double angle(const vector<T, N> &other) const {
		return acos(this->dot(other) / (this->dot(this) * other->dot(other)));
	}

	template <class T>
	__host__ __device__
	decltype(S() + T()) dot(const vector<T, N> &other) const {
		decltype(S() + T()) result = 0;
		#pragma unroll
		for (int i = 0; i < N; i++)
			result += (*this)[i] * other[i];
		return result;
	}

	__host__ __device__
	double norm() const {
		return sqrt(this->dot(this));
	}
};


template <class S>
struct vector<S, 1> :
		public vector_base<S, 1>,
		public cuda_vector_type<S, 1>::type {

	typedef typename cuda_vector_traits<typename cuda_vector_type<S, 1>::type>::element_type element_type;
	typedef typename cuda_vector_type<S, 1>::type base_type;

	__host__ __device__
	vector(const element_type &x = 0) {
		this->x = x;
	}

	template <class T>
	__host__ __device__
	vector<T, 1> asType() const {
		return vector<T, 1>(this->x);
	}
};

template <class S>
struct vector<S, 2> :
		public vector_base<S, 2>,
		public cuda_vector_type<S, 2>::type {

	typedef typename cuda_vector_traits<typename cuda_vector_type<S, 2>::type>::element_type element_type;
	typedef typename cuda_vector_type<S, 2>::type base_type;

	__host__ __device__
	vector(const element_type &x = 0, const element_type &y = 0) {
		this->x = x;
		this->y = y;
	}

	__host__ __device__
	vector(const typename cuda_vector_type<S, 2>::type &other) {
		this->x = other.x;
		this->y = other.y;
	}

	template <class T>
	__host__ __device__
	vector<T, 2> asType() const {
		return vector<T, 2>(this->x, this->y);
	}

	template <class T>
	__host__ __device__
	vector<decltype(S() * T()), 3> cross(const vector<T, 2> &other) const {
		auto z = this->x * other.y - other.x * this->y;
		return vector<decltype(S() * T()), 3>(0, 0, z);
	}

	template <class T>
	__host__ __device__
	double signedAngle(const vector<T, 2> &other) const {
		double angle = acos(this->dot(other) / (this->dot(*this) * other.dot(other)));
		return sign((this->cross(other)).z) * angle;
	}
};

template <class S>
struct vector<S, 3> :
		public vector_base<S, 3>,
		public cuda_vector_type<S, 3>::type {

	typedef typename cuda_vector_traits<typename cuda_vector_type<S, 3>::type>::element_type element_type;
	typedef typename cuda_vector_type<S, 3>::type base_type;

	__host__ __device__
	vector(const element_type &x = 0, const element_type &y = 0, const element_type &z = 0) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	__host__ __device__
	vector(const typename cuda_vector_type<S, 3>::type &other) {
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
	}

	template <class T>
	__host__ __device__
	vector<T, 3> asType() const {
		return vector<T, 3>(this->x, this->y, this->z);
	}

	template <class T>
	__host__ __device__
	vector<decltype(S() * T()), 3> cross(const vector<T, 2> &other) const {
		auto x = this->y * other.z - other.y * this->z;
		auto y = this->z * other.x - other.z * this->x;
		auto z = this->x * other.y - other.x * this->y;
		return vector<decltype(S() * T()), 3>(x, y, z);
	}
};

template <class S>
struct vector<S, 4> :
		public vector_base<S, 4>,
		public cuda_vector_type<S, 4>::type {

	typedef typename cuda_vector_traits<typename cuda_vector_type<S, 4>::type>::element_type element_type;
	typedef typename cuda_vector_type<S, 4>::type base_type;

	__host__ __device__
	vector(const element_type &x = 0, const element_type &y = 0, const element_type &z = 0, const element_type &w = 0) {
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}

	__host__ __device__
	vector(const typename cuda_vector_type<S, 4>::type &other) {
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = other.w;
	}

	template <class T>
	__host__ __device__
	vector<T, 4> asType() const {
		return vector<T, 4>(this->x, this->y, this->z, this->w);
	}
};

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator+(const vector<S, N> &u, const vector<T, N> &v) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] + v[i];
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator-(const vector<S, N> &u, const vector<T, N> &v) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] - v[i];
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator*(const vector<S, N> &u, const vector<T, N> &v) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] * v[i];
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator*(const vector<S, N> &u, const T &k) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] * k;
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator*(const T &k, const vector<S, N> &u) {
	return u * k;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator/(const vector<S, N> &u, const vector<T, N> &v) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] / v[i];
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator/(const vector<S, N> &u, const T &k) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] / k;
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator/(const T &k, const vector<S, N> &u) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = k / u[i];
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator%(const vector<S, N> &u, const vector<T, N> &v) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] % v[i];
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator%(const vector<S, N> &u, const T &k) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = u[i] % k;
	return w;
}

template <class S, class T, size_t N>
__host__ __device__
vector<decltype(S() + T()), N> operator%(const T &k, const vector<S, N> &u) {
	vector<decltype(S() + T()), N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = k / u[i];
	return w;
}

//TODO: include more binary operators

template <class S, size_t N>
__host__ __device__
vector<S, N> operator+(const vector<S, N> &u) {
	return u;
}

template <class S, size_t N>
__host__ __device__
vector<S, N> operator-(const vector<S, N> &u) {
	vector<S, N> w;
	#pragma unroll
	for (int i = 0; i < N; i++)
		w[i] = -u[i];
	return w;
}

template <class S, size_t N>
inline
std::ostream& operator<<(std::ostream &os, const vector<S, N> &vector) {
	os << "[ " << vector[0];
	#pragma unroll
	for (int i = 1; i < N; i++)
		os << ", " << vector[i];
	return os << " ]";
}

//TODO: review usage and semantics of functions bellow
//TODO: Put some functions like numpy.all and numpy.any
__host__ __device__ inline
unsigned analysis(const dim3 &vec) {
	return vec.x * vec.y * vec.z;
}

__host__ __device__ inline
int off(const int2 &v, const int2 &d) {
	return v.x + v.y * d.x;
}

__host__ __device__ inline
int off(const dim3 &v, const dim3 &d) {
	return v.x + v.y * d.x + v.z * d.x * d.y;
}

typedef vector<char, 1> vec1c;
typedef vector<char, 2> vec2c;
typedef vector<char, 3> vec3c;
typedef vector<char, 4> vec4c;
typedef vector<short, 1> vec1s;
typedef vector<short, 2> vec2s;
typedef vector<short, 3> vec3s;
typedef vector<short, 4> vec4s;
typedef vector<int, 1> vec1i;
typedef vector<int, 2> vec2i;
typedef vector<int, 3> vec3i;
typedef vector<int, 4> vec4i;
typedef vector<long, 1> vec1l;
typedef vector<long, 2> vec2l;
typedef vector<long, 3> vec3l;
typedef vector<long, 4> vec4l;
typedef vector<long long, 1> vec1ll;
typedef vector<long long, 2> vec2ll;
typedef vector<long long, 3> vec3ll;
typedef vector<long long, 4> vec4ll;
typedef vector<unsigned char, 1> vec1uc;
typedef vector<unsigned char, 2> vec2uc;
typedef vector<unsigned char, 3> vec3uc;
typedef vector<unsigned char, 4> vec4uc;
typedef vector<unsigned short, 1> vec1us;
typedef vector<unsigned short, 2> vec2us;
typedef vector<unsigned short, 3> vec3us;
typedef vector<unsigned short, 4> vec4us;
typedef vector<unsigned, 1> vec1u;
typedef vector<unsigned, 2> vec2u;
typedef vector<unsigned, 3> vec3u;
typedef vector<unsigned, 4> vec4u;
typedef vector<unsigned long, 1> vec1ul;
typedef vector<unsigned long, 2> vec2ul;
typedef vector<unsigned long, 3> vec3ul;
typedef vector<unsigned long, 4> vec4ul;
typedef vector<unsigned long long, 1> vec1ull;
typedef vector<unsigned long long, 2> vec2ull;
typedef vector<unsigned long long, 3> vec3ull;
typedef vector<unsigned long long, 4> vec4ull;
typedef vector<float, 1> vec1f;
typedef vector<float, 2> vec2f;
typedef vector<float, 3> vec3f;
typedef vector<float, 4> vec4f;
typedef vector<double, 1> vec1d;
typedef vector<double, 2> vec2d;
typedef vector<double, 3> vec3d;
typedef vector<double, 4> vec4d;

} /* namespace cutils*/
} /* namespace lcg */

#endif /* LCG_CUTILS_VECTOR_H_ */
