#ifdef __CUDACC__
#ifndef LCG_CUTILS_CUDA_INDEX_H_
#define LCG_CUTILS_CUDA_INDEX_H_

#include <cutils/vector.h>

namespace lcg {
namespace cutils {

//TODO: Review this class' interface. Probably the best thing to do is make it a subclass of vec3i (to allow signed arithmetics) and add conversion operators to vec*i.
struct cuda_index {

	// Instance methods - which call static methods!
	__device__ inline
	operator int() const {
		return index2d1();
	}

	__device__ inline
	vec2i i2d() const {
		return index2d2();
	}

	// Static methods - some of them redundant!
	__device__
	static inline int index1d1() {
		return index2d1();
	}

	__device__
	static inline int index2d1() {
		return (blockIdx.y * blockDim.y + threadIdx.y) * gridDim.x * blockDim.x +
				blockIdx.x * blockDim.x + threadIdx.x;
	}

	__device__
	static inline int index2d1(const dim3 &v) {
		return v.y * gridDim.x * blockDim.x + v.x;
	}

	__device__
	static inline int index2d1(const vec2i &v) {
		return v.y * gridDim.x * blockDim.x + v.x;
	}

	__device__
	static inline vec2i index2d2() {
		return vector<int, 2>(
				blockIdx.x * blockDim.x + threadIdx.x,
				blockIdx.y * blockDim.y + threadIdx.y);
	}

	__device__
	static inline int index1d() {
		return index2d1();
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_CUDA_INDEX_H_ */
#endif /* __CUDACC__ */
