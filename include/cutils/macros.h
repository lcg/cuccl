#ifndef LCG_CUTILS_MACROS_H_
#define LCG_CUTILS_MACROS_H_

#ifdef __CUDA_ARCH__
#define __HOST__DEVICE__IF_CUDA __host__ __device__
#else
#define __HOST__DEVICE__IF_CUDA
#endif

#endif /* LCG_CUTILS_MACROS_H_ */
