#ifndef LCG_CUTILS_CUDA_VECTOR_H_
#define LCG_CUTILS_CUDA_VECTOR_H_

#include <vector_types.h>

namespace lcg {
namespace cutils {

/**
 * Builds a CUDA vector type from a base Type and a number of Dimensions. It
 * has specializations for all CUDA vector types.
 *
 * \see lcg::cutils::cuda_vector_traits
 */
template <class Type, int Dims>
struct cuda_vector_type {
};

/**
 * Traits class for CUDA built-in vector types (i.e. \c int2, \c short3). All
 * specializations shall define:
 * 	* \c element_type: A typedef for the underlying data type
 *	* \c dims: A \c static \c const \c int member with the number of components
 *
 * \see lcg::cutils::cuda_vector_type
 */
template <class Type>
struct cuda_vector_traits {
	static const int dims;
};

} /* namespace cutils */
} /* namespace lcg */

//=============================================================================
// Below, you'll find specializations for the templates defined above.
//=============================================================================

namespace lcg {
namespace cutils {

template <>
struct cuda_vector_type <char, 1> {
	typedef char1 type;
};

template <>
struct cuda_vector_type <char, 2> {
	typedef char2 type;
};

template <>
struct cuda_vector_type <char, 3> {
	typedef char3 type;
};

template <>
struct cuda_vector_type <char, 4> {
	typedef char4 type;
};

template <>
struct cuda_vector_type <unsigned char, 1> {
	typedef uchar1 type;
};

template <>
struct cuda_vector_type <unsigned char, 2> {
	typedef uchar2 type;
};

template <>
struct cuda_vector_type <unsigned char, 3> {
	typedef uchar3 type;
};

template <>
struct cuda_vector_type <unsigned char, 4> {
	typedef uchar4 type;
};

template <>
struct cuda_vector_type <short, 1> {
	typedef short1 type;
};

template <>
struct cuda_vector_type <short, 2> {
	typedef short2 type;
};

template <>
struct cuda_vector_type <short, 3> {
	typedef short3 type;
};

template <>
struct cuda_vector_type <short, 4> {
	typedef short4 type;
};

template <>
struct cuda_vector_type <unsigned short, 1> {
	typedef ushort1 type;
};

template <>
struct cuda_vector_type <unsigned short, 2> {
	typedef ushort2 type;
};

template <>
struct cuda_vector_type <unsigned short, 3> {
	typedef ushort3 type;
};

template <>
struct cuda_vector_type <unsigned short, 4> {
	typedef ushort4 type;
};

template <>
struct cuda_vector_type <int, 1> {
	typedef int1 type;
};

template <>
struct cuda_vector_type <int, 2> {
	typedef int2 type;
};

template <>
struct cuda_vector_type <int, 3> {
	typedef int3 type;
};

template <>
struct cuda_vector_type <int, 4> {
	typedef int4 type;
};

template <>
struct cuda_vector_type <unsigned int, 1> {
	typedef uint1 type;
};

template <>
struct cuda_vector_type <unsigned int, 2> {
	typedef uint2 type;
};

template <>
struct cuda_vector_type <unsigned int, 3> {
	typedef uint3 type;
};

template <>
struct cuda_vector_type <unsigned int, 4> {
	typedef uint4 type;
};

template <>
struct cuda_vector_type <long int, 1> {
	typedef long1 type;
};

template <>
struct cuda_vector_type <long int, 2> {
	typedef long2 type;
};

template <>
struct cuda_vector_type <long int, 3> {
	typedef long3 type;
};

template <>
struct cuda_vector_type <long int, 4> {
	typedef long4 type;
};

template <>
struct cuda_vector_type <unsigned long int, 1> {
	typedef ulong1 type;
};

template <>
struct cuda_vector_type <unsigned long int, 2> {
	typedef ulong2 type;
};

template <>
struct cuda_vector_type <unsigned long int, 3> {
	typedef ulong3 type;
};

template <>
struct cuda_vector_type <unsigned long int, 4> {
	typedef ulong4 type;
};

template <>
struct cuda_vector_type <long long int, 1> {
	typedef longlong1 type;
};

template <>
struct cuda_vector_type <long long int, 2> {
	typedef longlong2 type;
};

template <>
struct cuda_vector_type <long long int, 3> {
	typedef longlong3 type;
};


template <>
struct cuda_vector_type <long long int, 4> {
	typedef longlong4 type;
};

template <>
struct cuda_vector_type <unsigned long long int, 1> {
	typedef ulonglong1 type;
};

template <>
struct cuda_vector_type <unsigned long long int, 2> {
	typedef ulonglong2 type;
};

template <>
struct cuda_vector_type <unsigned long long int, 3> {
	typedef ulonglong3 type;
};

template <>
struct cuda_vector_type <unsigned long long int, 4> {
	typedef ulonglong4 type;
};

template <>
struct cuda_vector_type <float, 1> {
	typedef float1 type;
};

template <>
struct cuda_vector_type <float, 2> {
	typedef float2 type;
};

template <>
struct cuda_vector_type <float, 3> {
	typedef float3 type;
};

template <>
struct cuda_vector_type <float, 4> {
	typedef float4 type;
};

template <>
struct cuda_vector_type <double, 1> {
	typedef double1 type;
};

template <>
struct cuda_vector_type <double, 2> {
	typedef double2 type;
};

template <>
struct cuda_vector_type <double, 3> {
	typedef double3 type;
};

template <>
struct cuda_vector_type <double, 4> {
	typedef double4 type;
};

template <>
struct cuda_vector_traits<char1> {
	typedef char element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<char2> {
	typedef char element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<char3> {
	typedef char element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<char4> {
	typedef char element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<uchar1> {
	typedef unsigned char element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<uchar2> {
	typedef unsigned char element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<uchar3> {
	typedef unsigned char element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<uchar4> {
	typedef unsigned char element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<short1> {
	typedef short element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<short2> {
	typedef short element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<short3> {
	typedef short element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<short4> {
	typedef short element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<ushort1> {
	typedef unsigned short element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<ushort2> {
	typedef unsigned short element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<ushort3> {
	typedef unsigned short element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<ushort4> {
	typedef unsigned short element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<int1> {
	typedef int element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<int2> {
	typedef int element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<int3> {
	typedef int element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<int4> {
	typedef int element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<uint1> {
	typedef unsigned int element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<uint2> {
	typedef unsigned int element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<uint3> {
	typedef unsigned int element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<uint4> {
	typedef unsigned int element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<long1> {
	typedef long element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<long2> {
	typedef long element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<long3> {
	typedef long element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<long4> {
	typedef long element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<ulong1> {
	typedef unsigned long element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<ulong2> {
	typedef unsigned long element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<ulong3> {
	typedef unsigned long element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<ulong4> {
	typedef unsigned long element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<longlong1> {
	typedef long long element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<longlong2> {
	typedef long long element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<ulonglong1> {
	typedef unsigned long long element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<ulonglong2> {
	typedef unsigned long long element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<ulonglong3> {
	typedef unsigned long long element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<ulonglong4> {
	typedef unsigned long long element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<float1> {
	typedef float element_type;
	static constexpr int dims = 1;
};

template <>
struct cuda_vector_traits<float2> {
	typedef float element_type;
	static constexpr int dims = 2;
};

template <>
struct cuda_vector_traits<float3> {
	typedef float element_type;
	static constexpr int dims = 3;
};

template <>
struct cuda_vector_traits<float4> {
	typedef float element_type;
	static constexpr int dims = 4;
};

template <>
struct cuda_vector_traits<double1> {
	typedef double element_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<double2> {
	typedef double element_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<double3> {
	typedef double element_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<double4> {
	typedef double element_type;
	static const int dims = 4;
};

}
}

#endif /* LCG_CUTILS_CUDA_VECTOR_H_ */
