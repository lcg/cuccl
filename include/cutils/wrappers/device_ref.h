#ifndef LCG_CUTILS_WRAPPERS_DEVICE_REF_H_
#define LCG_CUTILS_WRAPPERS_DEVICE_REF_H_

#include <cutils/shared_device_ptr.h>
#include <stdexcept>

namespace lcg {
namespace cutils {

template<class Type>
class hosted;

template<class Type>
class hosted_ref;

template<class Type>
class pinned;

template<class Type>
class device;

template<class Type>
class device_ref;

template <class Type>
class kernel_ref;

template<class Type>
class device_ref {
	friend class kernel_ref<Type>;

	shared_device_ptr<Type> gpupointer;

public:
	device_ref(Type &other) :
			gpupointer(MEMORY_DEVICE, &other) {
	}

	device_ref(Type *other) :
			gpupointer(MEMORY_DEVICE, other) {
	}

	device_ref(pinned<Type> &other) :
			gpupointer(other.buffer) {
	}

	device_ref(device<Type> &other) :
			gpupointer(other.gpupointer) {
	}

	device_ref(const device_ref &other) :
			gpupointer(other.gpupointer) {
	}

	device_ref& operator= (const Type &other) {
		CUTILS_CHECK_CALL( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device_ref& operator= (const hosted<Type> &other) {
		CUTILS_CHECK_CALL( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device_ref& operator= (const hosted_ref<Type> &other) {
		CUTILS_CHECK_CALL( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device_ref& operator= (const pinned<Type> &other) {
		CUTILS_CHECK_CALL( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device_ref& operator= (const device<Type> &other) {
		CUTILS_CHECK_CALL( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyDeviceToDevice) );
		return *this;
	}

	device_ref& operator= (const device_ref &other) {
		CUTILS_CHECK_CALL( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyDeviceToDevice) );
		return *this;
	}

	void memset(int value = 0, size_t bytes = sizeof(Type)) {
		CUTILS_CHECK_CALL( cudaMemset(gpupointer, value, bytes) );
	}

	operator Type&() {
		return *gpupointer;
	}

	operator const Type&() const {
		return *gpupointer;
	}

	Type& operator*() {
		return *gpupointer;
	}

	const Type& operator*() const {
		return *gpupointer;
	}

	Type* operator->() {
		return gpupointer;
	}

	const Type* operator->() const {
		return gpupointer;
	}

	Type* operator&() {
		return gpupointer;
	}

	const Type* operator&() const {
		return gpupointer;
	}
};

template <class Type>
class device_ref<Type*> {
	friend class hosted  <Type*>;
	friend class pinned<Type*>;
	friend class device<Type*>;
	friend class kernel_ref<Type*>;

	size_t length;
	shared_device_ptr<Type> gpupointer;
public:
	device_ref(const device<Type*> &other) :
			length(other.length), gpupointer(other.gpupointer) {
	}

	__host__ __device__
	operator Type*() {
		return gpupointer;
	}

	__host__ __device__
	operator const Type*() const {
		return gpupointer;
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_WRAPPERS_DEVICE_REF_H_ */
