#ifndef LCG_CUTILS_WRAPPERS_H_
#define LCG_CUTILS_WRAPPERS_H_

#include <cutils/wrappers/device_ref.h>
#include <cutils/wrappers/device.h>
#include <cutils/wrappers/hosted_ref.h>
#include <cutils/wrappers/hosted.h>
#include <cutils/wrappers/kernel_ref.h>
#include <cutils/wrappers/pinned.h>
#include <cutils/wrappers/type_wrapper.h>

namespace lcg {
namespace cutils {

//TODO: Missing array version
template <class Type>
hosted_ref<Type> in_host(Type &data) {
	return hosted_ref<Type>(data);
}

//TODO: Missing array version
template <class Type>
device_ref<Type> in_device(Type &data) {
	return hosted_ref<Type>(data);
}

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_WRAPPERS_H_ */
