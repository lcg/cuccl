/*
 * cached_allocator.h
 *
 *  Created on: 22/06/2017
 *      Author: pedro
 */

#ifndef CACHED_ALLOCATOR_H_
#define CACHED_ALLOCATOR_H_

#include <thrust/system/cuda/vector.h>
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/host_vector.h>
#include <thrust/generate.h>
#include <thrust/pair.h>
#include <iostream>
#include <map>

namespace thrust {
namespace extra {

// Example by Nathan Bell and Jared Hoberock
// (modified by Mihail Ivakhnenko)
//
// This example demonstrates how to intercept calls to get_temporary_buffer
// and return_temporary_buffer to control how Thrust allocates temporary storage
// during algorithms such as thrust::reduce. The idea will be to create a simple
// cache of allocations to search when temporary storage is requested. If a hit
// is found in the cache, we quickly return the cached allocation instead of
// resorting to the more expensive thrust::cuda::malloc.
//
// Note: this implementation cached_allocator is not thread-safe. If multiple
// (host) threads use the same cached_allocator then they should gain exclusive
// access to the allocator before accessing its methods.

// cached_allocator: a simple allocator for caching allocation requests
class cached_allocator
{
public:
    // just allocate bytes
    typedef char value_type;

    cached_allocator() {
    }

    ~cached_allocator() {
        free_all();
    }

    char* allocate(std::ptrdiff_t num_bytes) {

        char* result = 0;

        free_blocks_type::iterator free_block = free_blocks.find(num_bytes);

        if (free_block != free_blocks.end()) {
            result = free_block->second;
            free_blocks.erase(free_block);
        }
        else {
			result = thrust::cuda::malloc<char>(num_bytes).get();
        }

        allocated_blocks.insert(std::make_pair(result, num_bytes));

        return result;
    }

    void deallocate(char* ptr, size_t n) {

        // erase the allocated block from the allocated blocks map
        allocated_blocks_type::iterator iter = allocated_blocks.find(ptr);
        std::ptrdiff_t num_bytes = iter->second;
        allocated_blocks.erase(iter);

        // insert the block into the free blocks map
        free_blocks.insert(std::make_pair(num_bytes, ptr));
    }

private:
    typedef std::multimap<std::ptrdiff_t, char*> free_blocks_type;
    typedef std::map<char*, std::ptrdiff_t> allocated_blocks_type;

    free_blocks_type free_blocks;
    allocated_blocks_type allocated_blocks;

    void free_all() {

        for (free_blocks_type::iterator i = free_blocks.begin(); i != free_blocks.end(); i++)
            thrust::cuda::free(thrust::cuda::pointer<char>(i->second));

        for (allocated_blocks_type::iterator i = allocated_blocks.begin(); i != allocated_blocks.end(); i++)
            thrust::cuda::free(thrust::cuda::pointer<char>(i->first));
    }
};

} /* namespace thrust::extra */
} /* namespace thrust */

#endif /* CACHED_ALLOCATOR_H_ */
