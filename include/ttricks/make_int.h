#ifndef LCG_TTRICKS_MAKE_INT_H_
#define LCG_TTRICKS_MAKE_INT_H_

#include <stdint.h>

namespace lcg {
namespace ttricks {

template <int Bits>
struct make_int {};

template<>
struct make_int<1> {
	typedef int8_t type;
};

template<>
struct make_int<2> {
	typedef int8_t type;
};

template<>
struct make_int<3> {
	typedef int8_t type;
};

template<>
struct make_int<4> {
	typedef int8_t type;
};

template<>
struct make_int<5> {
	typedef int8_t type;
};

template<>
struct make_int<6> {
	typedef int8_t type;
};

template<>
struct make_int<7> {
	typedef int8_t type;
};

template<>
struct make_int<8> {
	typedef int8_t type;
};

template<>
struct make_int<9> {
	typedef int16_t type;
};

template<>
struct make_int<10> {
	typedef int16_t type;
};

template<>
struct make_int<11> {
	typedef int16_t type;
};

template<>
struct make_int<12> {
	typedef int16_t type;
};

template<>
struct make_int<13> {
	typedef int16_t type;
};

template<>
struct make_int<14> {
	typedef int16_t type;
};

template<>
struct make_int<15> {
	typedef int16_t type;
};

template<>
struct make_int<16> {
	typedef int16_t type;
};

template<>
struct make_int<17> {
	typedef int32_t type;
};

template<>
struct make_int<18> {
	typedef int32_t type;
};

template<>
struct make_int<19> {
	typedef int32_t type;
};

template<>
struct make_int<20> {
	typedef int32_t type;
};

template<>
struct make_int<21> {
	typedef int32_t type;
};

template<>
struct make_int<22> {
	typedef int32_t type;
};

template<>
struct make_int<23> {
	typedef int32_t type;
};

template<>
struct make_int<24> {
	typedef int32_t type;
};

template<>
struct make_int<25> {
	typedef int32_t type;
};

template<>
struct make_int<26> {
	typedef int32_t type;
};

template<>
struct make_int<27> {
	typedef int32_t type;
};

template<>
struct make_int<28> {
	typedef int32_t type;
};

template<>
struct make_int<29> {
	typedef int32_t type;
};

template<>
struct make_int<30> {
	typedef int32_t type;
};

template<>
struct make_int<31> {
	typedef int32_t type;
};

template<>
struct make_int<32> {
	typedef int32_t type;
};

template<>
struct make_int<33> {
	typedef int64_t type;
};

template<>
struct make_int<34> {
	typedef int64_t type;
};

template<>
struct make_int<35> {
	typedef int64_t type;
};

template<>
struct make_int<36> {
	typedef int64_t type;
};

template<>
struct make_int<37> {
	typedef int64_t type;
};

template<>
struct make_int<38> {
	typedef int64_t type;
};

template<>
struct make_int<39> {
	typedef int64_t type;
};

template<>
struct make_int<40> {
	typedef int64_t type;
};

template<>
struct make_int<41> {
	typedef int64_t type;
};

template<>
struct make_int<42> {
	typedef int64_t type;
};

template<>
struct make_int<43> {
	typedef int64_t type;
};

template<>
struct make_int<44> {
	typedef int64_t type;
};

template<>
struct make_int<45> {
	typedef int64_t type;
};

template<>
struct make_int<46> {
	typedef int64_t type;
};

template<>
struct make_int<47> {
	typedef int64_t type;
};

template<>
struct make_int<48> {
	typedef int64_t type;
};

template<>
struct make_int<49> {
	typedef int64_t type;
};

template<>
struct make_int<50> {
	typedef int64_t type;
};

template<>
struct make_int<51> {
	typedef int64_t type;
};

template<>
struct make_int<52> {
	typedef int64_t type;
};

template<>
struct make_int<53> {
	typedef int64_t type;
};

template<>
struct make_int<54> {
	typedef int64_t type;
};

template<>
struct make_int<55> {
	typedef int64_t type;
};

template<>
struct make_int<56> {
	typedef int64_t type;
};

template<>
struct make_int<57> {
	typedef int64_t type;
};

template<>
struct make_int<58> {
	typedef int64_t type;
};

template<>
struct make_int<59> {
	typedef int64_t type;
};

template<>
struct make_int<60> {
	typedef int64_t type;
};

template<>
struct make_int<61> {
	typedef int64_t type;
};

template<>
struct make_int<62> {
	typedef int64_t type;
};

template<>
struct make_int<63> {
	typedef int64_t type;
};

template<>
struct make_int<64> {
	typedef int64_t type;
};

} /* namespace ttricks */
} /* namespace lcg */

#endif /* LCG_TTRICKS_MAKE_INT_H_ */
