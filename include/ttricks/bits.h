#ifndef LCG_TTRICKS_BITS_H_
#define LCG_TTRICKS_BITS_H_

#include "integer_traits.h"

namespace lcg {
namespace ttricks {

/**
 * Auxiliar template to implement bits<>. It takes the evaluated Number,
 * the pow of 2 that corresponds to the currently tested significant
 * n and a redundant Mask that should equal Number & pow. In the
 * general case, Mask is different from pow and a lesser pow of 2
 * is tested for the same Number. In the specialized cases, when Mask
 * equals number, meaning that the sought pow of 2 has been found.
 */
template<unsigned long long Number, unsigned long long Mask, unsigned long long Power>
struct bits_match {
	static const unsigned long long n = bits_match<Number, Number & (Power >> 1), (Power >> 1)>::n;
};

/**
 * Extracts the number of bits of an unsigned integer constant.
 * It uses the bits_match auxiliar template to try matching from the
 * greatest significant n in C++, which is 2^63, down to 0. When the
 * most significant n is not found at given position, the template
 * recursion tries the immediately lesser pow of 2.
 */
template<unsigned long long Number>
struct bits {
	static const unsigned long long n = bits_match<Number, Number & (1ull << 63), (1ull << 63)>::n;
};

/**
 * Auxiliar template to implement gp2<>. It takes the evaluated Number,
 * the pow of 2 that corresponds to the currently tested significant
 * n and a redundant Mask that should equal Number & pow. In the
 * general case, Mask is different from pow and a greater pow of 2
 * is tested for the same Number. In the specialized cases, when Mask
 * equals number, meaning that the sought pow of 2 has been found.
 */
template<unsigned long long Number, unsigned long long Mask, unsigned long long Power>
struct gp2_match {
	static const unsigned long long bit = gp2_match<Number, Number & (Power << 1), (Power << 1)>::bit;
	static const unsigned long long pow = gp2_match<Number, Number & (Power << 1), (Power << 1)>::pow;
};

/**
 * Extracts the greatest pow of 2 that divides an unsigned integer constant.
 * It uses the gp2_match auxiliar template to try matching from 1
 * up the most significant pow of 2 in C++, which is 2^63. When the
 * greatest factor is not found at given position, the template
 * recursion tries the immediately greater pow of 2.
 */
template<unsigned long long Number>
struct gp2 {
	static const unsigned long long bit = gp2_match<Number, Number & 1ull, 1ull>::bit;
	static const unsigned long long pow = gp2_match<Number, Number & 1ull, 1ull>::pow;
};

template<unsigned long long Number>
struct bits_match<Number, 0, 0> {
	static const unsigned long long n = 0;
};

template<unsigned long long Number>
struct bits_match<Number, 1ull, 1ull> {
	static const unsigned long long n = 1;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 1), (1ull << 1)> {
	static const unsigned long long n = 2;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 2), (1ull << 2)> {
	static const unsigned long long n = 3;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 3), (1ull << 3)> {
	static const unsigned long long n = 4;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 4), (1ull << 4)> {
	static const unsigned long long n = 5;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 5), (1ull << 5)> {
	static const unsigned long long n = 6;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 6), (1ull << 6)> {
	static const unsigned long long n = 7;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 7), (1ull << 7)> {
	static const unsigned long long n = 8;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 8), (1ull << 8)> {
	static const unsigned long long n = 9;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 9), (1ull << 9)> {
	static const unsigned long long n = 10;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 10), (1ull << 10)> {
	static const unsigned long long n = 11;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 11), (1ull << 11)> {
	static const unsigned long long n = 12;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 12), (1ull << 12)> {
	static const unsigned long long n = 13;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 13), (1ull << 13)> {
	static const unsigned long long n = 14;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 14), (1ull << 14)> {
	static const unsigned long long n = 15;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 15), (1ull << 15)> {
	static const unsigned long long n = 16;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 16), (1ull << 16)> {
	static const unsigned long long n = 17;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 17), (1ull << 17)> {
	static const unsigned long long n = 18;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 18), (1ull << 18)> {
	static const unsigned long long n = 19;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 19), (1ull << 19)> {
	static const unsigned long long n = 20;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 20), (1ull << 20)> {
	static const unsigned long long n = 21;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 21), (1ull << 21)> {
	static const unsigned long long n = 22;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 22), (1ull << 22)> {
	static const unsigned long long n = 23;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 23), (1ull << 23)> {
	static const unsigned long long n = 24;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 24), (1ull << 24)> {
	static const unsigned long long n = 25;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 25), (1ull << 25)> {
	static const unsigned long long n = 26;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 26), (1ull << 26)> {
	static const unsigned long long n = 27;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 27), (1ull << 27)> {
	static const unsigned long long n = 28;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 28), (1ull << 28)> {
	static const unsigned long long n = 29;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 29), (1ull << 29)> {
	static const unsigned long long n = 30;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 30), (1ull << 30)> {
	static const unsigned long long n = 31;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 31), (1ull << 31)> {
	static const unsigned long long n = 32;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 32), (1ull << 32)> {
	static const unsigned long long n = 33;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 33), (1ull << 33)> {
	static const unsigned long long n = 34;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 34), (1ull << 34)> {
	static const unsigned long long n = 35;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 35), (1ull << 35)> {
	static const unsigned long long n = 36;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 36), (1ull << 36)> {
	static const unsigned long long n = 37;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 37), (1ull << 37)> {
	static const unsigned long long n = 38;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 38), (1ull << 38)> {
	static const unsigned long long n = 39;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 39), (1ull << 39)> {
	static const unsigned long long n = 40;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 40), (1ull << 40)> {
	static const unsigned long long n = 41;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 41), (1ull << 41)> {
	static const unsigned long long n = 42;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 42), (1ull << 42)> {
	static const unsigned long long n = 43;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 43), (1ull << 43)> {
	static const unsigned long long n = 44;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 44), (1ull << 44)> {
	static const unsigned long long n = 45;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 45), (1ull << 45)> {
	static const unsigned long long n = 46;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 46), (1ull << 46)> {
	static const unsigned long long n = 47;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 47), (1ull << 47)> {
	static const unsigned long long n = 48;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 48), (1ull << 48)> {
	static const unsigned long long n = 49;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 49), (1ull << 49)> {
	static const unsigned long long n = 50;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 50), (1ull << 50)> {
	static const unsigned long long n = 51;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 51), (1ull << 51)> {
	static const unsigned long long n = 52;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 52), (1ull << 52)> {
	static const unsigned long long n = 53;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 53), (1ull << 53)> {
	static const unsigned long long n = 54;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 54), (1ull << 54)> {
	static const unsigned long long n = 55;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 55), (1ull << 55)> {
	static const unsigned long long n = 56;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 56), (1ull << 56)> {
	static const unsigned long long n = 57;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 57), (1ull << 57)> {
	static const unsigned long long n = 58;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 58), (1ull << 58)> {
	static const unsigned long long n = 59;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 59), (1ull << 59)> {
	static const unsigned long long n = 60;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 60), (1ull << 60)> {
	static const unsigned long long n = 61;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 61), (1ull << 61)> {
	static const unsigned long long n = 62;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 62), (1ull << 62)> {
	static const unsigned long long n = 63;
};

template<unsigned long long Number>
struct bits_match<Number, (1ull << 63), (1ull << 63)> {
	static const unsigned long long n = 64;
};

template<unsigned long long Number>
struct gp2_match<Number, 1ull, 1ull> {
	static const unsigned long long bit = 0;
	static const unsigned long long pow = 1ull;
};

template<unsigned long long Number>
struct gp2_match<Number, (1ull << 1), (1ull << 1)> {
	static const unsigned long long bit = 1;
	static const unsigned long long pow = (1ull << 1);
};

template<unsigned long long Number>
struct gp2_match<Number, (1ull << 2), (1ull << 2)> {
	static const unsigned long long bit = 2;
	static const unsigned long long pow = (1ull << 2);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 3), (1ull << 3)> {
	static const unsigned long long bit = 3;
	static const unsigned long long pow = (1ull << 3);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 4), (1ull << 4)> {
	static const unsigned long long bit = 4;
	static const unsigned long long pow = (1ull << 4);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 5), (1ull << 5)> {
	static const unsigned long long bit = 5;
	static const unsigned long long pow = (1ull << 5);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 6), (1ull << 6)> {
	static const unsigned long long bit = 6;
	static const unsigned long long pow = (1ull << 6);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 7), (1ull << 7)> {
	static const unsigned long long bit = 7;
	static const unsigned long long pow = (1ull << 7);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 8), (1ull << 8)> {
	static const unsigned long long bit = 8;
	static const unsigned long long pow = (1ull << 8);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 9), (1ull << 9)> {
	static const unsigned long long bit = 9;
	static const unsigned long long pow = (1ull << 9);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 10), (1ull << 10)> {
	static const unsigned long long bit = 10;
	static const unsigned long long pow = (1ull << 10);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 11), (1ull << 11)> {
	static const unsigned long long bit = 11;
	static const unsigned long long pow = (1ull << 11);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 12), (1ull << 12)> {
	static const unsigned long long bit = 12;
	static const unsigned long long pow = (1ull << 12);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 13), (1ull << 13)> {
	static const unsigned long long bit = 13;
	static const unsigned long long pow = (1ull << 13);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 14), (1ull << 14)> {
	static const unsigned long long bit = 14;
	static const unsigned long long pow = (1ull << 14);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 15), (1ull << 15)> {
	static const unsigned long long bit = 15;
	static const unsigned long long pow = (1ull << 15);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 16), (1ull << 16)> {
	static const unsigned long long bit = 16;
	static const unsigned long long pow = (1ull << 16);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 17), (1ull << 17)> {
	static const unsigned long long bit = 17;
	static const unsigned long long pow = (1ull << 17);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 18), (1ull << 18)> {
	static const unsigned long long bit = 18;
	static const unsigned long long pow = (1ull << 18);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 19), (1ull << 19)> {
	static const unsigned long long bit = 19;
	static const unsigned long long pow = (1ull << 19);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 20), (1ull << 20)> {
	static const unsigned long long bit = 20;
	static const unsigned long long pow = (1ull << 20);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 21), (1ull << 21)> {
	static const unsigned long long bit = 21;
	static const unsigned long long pow = (1ull << 21);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 22), (1ull << 22)> {
	static const unsigned long long bit = 22;
	static const unsigned long long pow = (1ull << 22);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 23), (1ull << 23)> {
	static const unsigned long long bit = 23;
	static const unsigned long long pow = (1ull << 23);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 24), (1ull << 24)> {
	static const unsigned long long bit = 24;
	static const unsigned long long pow = (1ull << 24);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 25), (1ull << 25)> {
	static const unsigned long long bit = 25;
	static const unsigned long long pow = (1ull << 25);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 26), (1ull << 26)> {
	static const unsigned long long bit = 26;
	static const unsigned long long pow = (1ull << 26);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 27), (1ull << 27)> {
	static const unsigned long long bit = 27;
	static const unsigned long long pow = (1ull << 27);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 28), (1ull << 28)> {
	static const unsigned long long bit = 28;
	static const unsigned long long pow = (1ull << 28);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 29), (1ull << 29)> {
	static const unsigned long long bit = 29;
	static const unsigned long long pow = (1ull << 29);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 30), (1ull << 30)> {
	static const unsigned long long bit = 30;
	static const unsigned long long pow = (1ull << 30);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 31), (1ull << 31)> {
	static const unsigned long long bit = 31;
	static const unsigned long long pow = (1ull << 31);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 32), (1ull << 32)> {
	static const unsigned long long bit = 32;
	static const unsigned long long pow = (1ull << 32);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 33), (1ull << 33)> {
	static const unsigned long long bit = 33;
	static const unsigned long long pow = (1ull << 33);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 34), (1ull << 34)> {
	static const unsigned long long bit = 34;
	static const unsigned long long pow = (1ull << 34);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 35), (1ull << 35)> {
	static const unsigned long long bit = 35;
	static const unsigned long long pow = (1ull << 35);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 36), (1ull << 36)> {
	static const unsigned long long bit = 36;
	static const unsigned long long pow = (1ull << 36);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 37), (1ull << 37)> {
	static const unsigned long long bit = 37;
	static const unsigned long long pow = (1ull << 37);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 38), (1ull << 38)> {
	static const unsigned long long bit = 38;
	static const unsigned long long pow = (1ull << 38);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 39), (1ull << 39)> {
	static const unsigned long long bit = 39;
	static const unsigned long long pow = (1ull << 39);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 40), (1ull << 40)> {
	static const unsigned long long bit = 40;
	static const unsigned long long pow = (1ull << 40);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 41), (1ull << 41)> {
	static const unsigned long long bit = 41;
	static const unsigned long long pow = (1ull << 41);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 42), (1ull << 42)> {
	static const unsigned long long bit = 42;
	static const unsigned long long pow = (1ull << 42);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 43), (1ull << 43)> {
	static const unsigned long long bit = 43;
	static const unsigned long long pow = (1ull << 43);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 44), (1ull << 44)> {
	static const unsigned long long bit = 44;
	static const unsigned long long pow = (1ull << 44);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 45), (1ull << 45)> {
	static const unsigned long long bit = 45;
	static const unsigned long long pow = (1ull << 45);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 46), (1ull << 46)> {
	static const unsigned long long bit = 46;
	static const unsigned long long pow = (1ull << 46);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 47), (1ull << 47)> {
	static const unsigned long long bit = 47;
	static const unsigned long long pow = (1ull << 47);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 48), (1ull << 48)> {
	static const unsigned long long bit = 48;
	static const unsigned long long pow = (1ull << 48);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 49), (1ull << 49)> {
	static const unsigned long long bit = 49;
	static const unsigned long long pow = (1ull << 49);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 50), (1ull << 50)> {
	static const unsigned long long bit = 50;
	static const unsigned long long pow = (1ull << 50);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 51), (1ull << 51)> {
	static const unsigned long long bit = 51;
	static const unsigned long long pow = (1ull << 51);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 52), (1ull << 52)> {
	static const unsigned long long bit = 52;
	static const unsigned long long pow = (1ull << 52);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 53), (1ull << 53)> {
	static const unsigned long long bit = 53;
	static const unsigned long long pow = (1ull << 53);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 54), (1ull << 54)> {
	static const unsigned long long bit = 54;
	static const unsigned long long pow = (1ull << 54);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 55), (1ull << 55)> {
	static const unsigned long long bit = 55;
	static const unsigned long long pow = (1ull << 55);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 56), (1ull << 56)> {
	static const unsigned long long bit = 56;
	static const unsigned long long pow = (1ull << 56);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 57), (1ull << 57)> {
	static const unsigned long long bit = 57;
	static const unsigned long long pow = (1ull << 57);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 58), (1ull << 58)> {
	static const unsigned long long bit = 58;
	static const unsigned long long pow = (1ull << 58);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 59), (1ull << 59)> {
	static const unsigned long long bit = 59;
	static const unsigned long long pow = (1ull << 59);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 60), (1ull << 60)> {
	static const unsigned long long bit = 60;
	static const unsigned long long pow = (1ull << 60);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 61), (1ull << 61)> {
	static const unsigned long long bit = 61;
	static const unsigned long long pow = (1ull << 61);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 62), (1ull << 62)> {
	static const unsigned long long bit = 62;
	static const unsigned long long pow = (1ull << 62);
};
template<unsigned long long Number>
struct gp2_match<Number, (1ull << 63), (1ull << 63)> {
	static const unsigned long long bit = 63;
	static const unsigned long long pow = (1ull << 63);
};

} /* namespace ttricks */
} /* namespace lcg */

#endif /* LCG_TTRICKS_BITS_H_ */
