#ifndef LCG_CUCCL_IMAGE_H_
#define LCG_CUCCL_IMAGE_H_

#include <iostream>
#include <limits>
#include <stdint.h>

#include <cutils/macros.h>
#include <cutils/vector.h>

namespace lcg {
namespace cuccl {

template<class PixelType, int Width, int Height = 1>
class array_2d {
public:
	static const int width  = Width;
	static const int height = Height;
	static const int size   = width * height * sizeof(PixelType);
	static const int pitch  = width * sizeof(PixelType);
	static const dim3 dims; //TODO: Remove this when images are no longer restricted in size

	typedef PixelType type;

	PixelType data[height * width];

	__HOST__DEVICE__IF_CUDA inline
	PixelType& operator()(const cutils::vec2i &point) {
		return data[point.y * width + point.x];
	}

	__HOST__DEVICE__IF_CUDA inline
	const PixelType& operator()(const cutils::vec2i &point) const {
		return data[point.y * width + point.x];
	}

	__HOST__DEVICE__IF_CUDA inline
	PixelType& operator()(int x, int y = 0) {
		return data[y * width + x];
	}

	__HOST__DEVICE__IF_CUDA inline
	const PixelType& operator()(int x, int y = 0) const {
		return data[y * width + x];
	}

	__HOST__DEVICE__IF_CUDA inline
	void fill(const PixelType &value) {
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
				(*this)(x, y) = value;
	}

	__HOST__DEVICE__IF_CUDA inline
	array_2d& operator=(const PixelType &value) {
		fill(value);
		return *this;
	}
};

template<class T, int W, int H>
const int array_2d<T, W, H>::width;

template<class T, int W, int H>
const int array_2d<T, W, H>::height;

template<class T, int W, int H>
const dim3 array_2d<T, W, H>::dims = dim3(W, H, 1);

template<class T, int W, int H>
const int array_2d<T, W, H>::size;

template<class T, int W, int H>
const int array_2d<T, W, H>::pitch;

template<class PixelType, int Width, int Height = 1>
class image: public array_2d<PixelType, Width, Height> {
public:
	__host__ __device__
	static bool inside(const cutils::vec2i &p) {
		return p.x >= 0 and p.y >= 0 and p.x < Width and p.y < Height;
	}
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_IMAGE_H_ */
