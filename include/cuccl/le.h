#ifndef LCG_CUCCL_LE_H_
#define LCG_CUCCL_LE_H_

#include <cuccl/ccl_algorithm.h>
#include <cuccl/types.h>

namespace lcg {
namespace cuccl {

class ccl_data;

class LabelEquivalence : public ccl_algorithm {
public:
	struct Options {
		bool computePropertiesLater;
		bool pruneNeighborsInFirstScan;
		bool sequentialRelabel;
		int  uncheckedIterations;
		bool useLocalFlagInScan;
		bool useNwNeighborsInFirstScan;

		Options();
	};

	LabelEquivalence(const dim3 &size, Options options);

	virtual
	~LabelEquivalence();

	virtual void
	ccl(const segment_t *binary, ccl_data &output);

private:
	Options options;
	dim3 size;
	label_t *seqlabels;
	cutils::pinned<bool> changed;
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_LE_H_ */
