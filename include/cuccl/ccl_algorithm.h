#ifndef LCG_CUCCL_CCL_ALGORITHM_H_
#define LCG_CUCCL_CCL_ALGORITHM_H_

#include <cuccl/types.h>

namespace lcg {
namespace cuccl {

class ccl_data;

class ccl_algorithm {
public:
	virtual ~ccl_algorithm();
	virtual void ccl(const segment_t *binary, ccl_data &output) = 0;
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_CCL_ALGORITHM_H_ */
