#ifndef LCG_CUCCL_INTERNAL_RELABEL_H_
#define LCG_CUCCL_INTERNAL_RELABEL_H_

#include <cuccl/types.h>

namespace lcg {
namespace cuccl {

class ccl_data;

namespace internal {

void deinitRelabel();

void initRelabel(dim3 size);

void
relabel(const dim3 &size, ccl_data &output, label_t *rootsBuffer, label_t *ccCount=nullptr);

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_RELABEL_H_ */
