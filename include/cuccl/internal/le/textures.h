//TODO: Once functions that depend on textures are effectively parameterized by texture objects, this header should be unnecessary
#ifndef LCG_CUCCL_INTERNAL_LE_TEXTURES_H_
#define LCG_CUCCL_INTERNAL_LE_TEXTURES_H_

#include <cuccl/types.h>
#include <cutils/vector.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace le {

extern texture<segment_t, cudaTextureType2D> binaryTex;
extern texture<  label_t, cudaTextureType2D>  labelTex;

void
bindBinaryImageToTexture(const segment_t *buffer, const dim3 &size);

void
bindLabelImageToTexture(const label_t *buffer, const dim3 &size);

} /* namespace lcg::cuccl::internal::le */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg:: */

#endif /* LCG_CUCCL_INTERNAL_LE_TEXTURES_H_ */
