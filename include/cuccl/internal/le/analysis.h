#ifndef LCG_CUCCL_INTERNAL_LE_ANALYSIS_H_
#define LCG_CUCCL_INTERNAL_LE_ANALYSIS_H_

#include <cuccl/ccl_data.h>
#include <cuccl/types.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace le {

void
analysis(const dim3 &size, ccl_data output);

} /* namespace lcg::cuccl::internal::le */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_LE_ANALYSIS_H_ */
