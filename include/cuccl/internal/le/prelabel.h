#ifndef LCG_CUCCL_INTERNAL_LE_PRELABEL_H_
#define LCG_CUCCL_INTERNAL_LE_PRELABEL_H_

#include <cuccl/types.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace le {

void
prelabel(const dim3 &size, const segment_t *binary, ccl_data output);

} /* namespace lcg::cuccl::internal::le */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_LE_PRELABEL_H_ */
