#ifndef LCG_CUCCL_INTERNAL_SMALLEST_H_
#define LCG_CUCCL_INTERNAL_SMALLEST_H_

#include <cuccl/types.h>
#include <cuccl/internal/le/textures.h>

namespace lcg {
namespace cuccl {
namespace internal {

template <class Neighbors>
struct smallest_neighbor {
	//TODO: Create a concept for this kind of object
	//TODO: Parameterize by texture object and remove dependency on le_textures.h
	__device__ inline
	label_t operator()(const cutils::vec2i &p) {
		using cutils::vec2i;

		label_t smallest = std::numeric_limits<label_t>::max();

		Neighbors::forEach(p, [&](const cutils::vec2i &dir, const cutils::vec2i &q) {
			const label_t label = tex2D(le::labelTex, q.x, q.y);

			if (label < smallest)
				smallest = label;
		});

		return smallest;
	}
};

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_SMALLEST_H_ */
