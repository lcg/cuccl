#ifndef LCG_CUCCL_INTERNAL_TRANSFER_PROPS_H_
#define LCG_CUCCL_INTERNAL_TRANSFER_PROPS_H_

namespace lcg {
namespace cuccl {

class ccl_data;

namespace internal {

void
transfer_props(const dim3 &size, ccl_data &output);

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_TRANSFER_PROPS_H_ */
