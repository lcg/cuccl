#ifndef LCG_CUCCL_INTERNAL_LLE_SCAN_H_
#define LCG_CUCCL_INTERNAL_LLE_SCAN_H_

#include <cuccl/types.h>
#include <cuccl/internal/le/textures.h>
#include <cuccl/internal/neighbors_8.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace lle {

void localPhase(const dim3 &size, const segment_t *binary, ccl_data output, const dim3 &block=dim3());

} /* namespace lcg::cuccl::internal::lle */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_LLE_SCAN_H_ */
