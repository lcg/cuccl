#ifndef LCG_CUCCL_INTERNAL_NEIGHBORS_8_SCAN_H_
#define LCG_CUCCL_INTERNAL_NEIGHBORS_8_SCAN_H_

namespace lcg {
namespace cuccl {
namespace internal {

struct neighbors_8_scan {
	template <class Function>
	__device__ inline
	static void forEach(const cutils::vec2i p, Function func) {
		using cutils::vec2i;
		forEachCheck(p, [&](const vec2i &dir, const vec2i q) -> bool {
			func(dir, q);
			return true;
		});
	}

	template <class Function>
	__device__ inline
	static void forEachCheck(const cutils::vec2i p, Function func) {
		const cutils::vec2i neighb[] = {
			{ -1, -1 }, { 0, -1 }, { 1, -1 },
			{ -1,  0 }
		};
		for (int i = 0; i < length(neighb); i++)
			if (not func(neighb[i], p + neighb[i]))
				break;
	}
};

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_NEIGHBORS_8_SCAN_H_ */
