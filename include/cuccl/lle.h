#ifndef LCG_CUCCL_LLE_H_
#define LCG_CUCCL_LLE_H_

#include <cuccl/ccl_algorithm.h>
#include <cuccl/types.h>

namespace lcg {
namespace cuccl {

class ccl_data;

class LocalLabelEquivalence : public ccl_algorithm {
public:
	struct Options {
		bool computePropertiesLater;
		bool sequentialRelabel;
		int  uncheckedIterations;
		bool useLocalFlagInScan;
		bool useNwNeighborsInFirstScan;

		Options();
	};

	LocalLabelEquivalence(const dim3 &size, Options options);

	virtual
	~LocalLabelEquivalence();

	virtual void
	ccl(const segment_t *binary, ccl_data &output);

private:
	Options options;
	dim3 size;
	label_t *seqlabels;
	cutils::pinned<bool> changed;
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_LLE_H_ */
