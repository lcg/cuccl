#ifndef LCG_CUCCL_BINARY_SEGMENT_H_
#define LCG_CUCCL_BINARY_SEGMENT_H_

#include <cstdint>
#include <cuccl/types.h>
#include <cutils/wrappers/type_wrapper.h>

namespace lcg {
namespace cuccl {

/**
 * Wrapper class for \c segment_t conforming with the <em>segment wrapping
 * concept</em> and used in many CCL algorithms.
 *
 * \see The \ref concepts section for more information on the <em>segment
 * wrapping concept</em>.
 */
class binary_segment : public cutils::type_wrapper<segment_t> {
public:
	using cutils::type_wrapper<segment_t>::type_wrapper;

	__HOST__DEVICE__IF_CUDA inline
	static constexpr segment_t background() {
		return std::numeric_limits<segment_t>::max();
	}

	__HOST__DEVICE__IF_CUDA inline
	bool isBackground() const {
		return (*this) == background();
	}

	__HOST__DEVICE__IF_CUDA inline
	bool isForeground() const {
		return not isBackground();
	}
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_BINARY_SEGMENT_H_ */
