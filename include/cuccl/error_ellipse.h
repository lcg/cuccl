#ifndef LCG_CUCCL_ERROR_ELLIPSE_H_
#define LCG_CUCCL_ERROR_ELLIPSE_H_

#include <cuccl/types.h>
#include <cuccl/properties.h>
#include <cutils/vector.h>

namespace lcg {
namespace cuccl {

struct error_ellipse {
	float covmatrix[2][2];
	float  eigenval[2];
	cutils::vec2f eigenvec[2];

	__HOST__DEVICE__IF_CUDA
	error_ellipse(const variance_t &variances, const centroid_t &centroid, float area);

	__HOST__DEVICE__IF_CUDA
	error_ellipse(const properties &props);

	__HOST__DEVICE__IF_CUDA
	float angle() const;

	__HOST__DEVICE__IF_CUDA
	float area() const;
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_ERROR_ELLIPSE_H_ */
