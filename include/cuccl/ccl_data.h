#ifndef LCG_CUCCL_CCL_DATA_H_
#define LCG_CUCCL_CCL_DATA_H_

#include <cuccl/types.h>
#include <cuccl/properties.h>

namespace lcg {
namespace cuccl {

//TODO: Adapt to use size-agnostic buffers and thrust::vectors
//TODO: Not releasing allocated memory
//TODO: Create a connected_component class and use iterators to visit instances
//TODO: Review inline methods
struct ccl_data {
	dim3 size;
	label_t    ccSlots;
	label_t    *labels;
	area_t     *area;
	centroid_t *centroid;
	variance_t *variance;

	static ccl_data allocDevice(const dim3 &size, bool withProps=true, int factor=1);
	static ccl_data allocHost(const dim3 &size, bool withProps=true, int factor=1);

	void copyTo(ccl_data &other, cudaMemcpyKind transferKind);

	ccl_data withoutProps();

	__HOST__DEVICE__IF_CUDA inline
	properties getProps(int index) const {
		properties props;
		props.area     = area    [index];
		props.centroid = centroid[index];
		props.variance = variance[index];
		return props;
	}

	__HOST__DEVICE__IF_CUDA inline
	bool hasProps() const {
		return area != nullptr;
	}

	__HOST__DEVICE__IF_CUDA inline
	void setProps(int index, const properties &props) {
		area    [index] = props.area;
		centroid[index] = props.centroid;
		variance[index] = props.variance;
	}

	#ifdef __CUDACC__
		template <class LabelType>
		__device__ inline
		void addProps(LabelType to, const properties &props) {
			atomicAdd(&area    [to.address()]  , props.area      );
			atomicAdd(&centroid[to.address()].x, props.centroid.x);
			atomicAdd(&centroid[to.address()].y, props.centroid.y);
			atomicAdd(&variance[to.address()].x, props.variance.x);
			atomicAdd(&variance[to.address()].y, props.variance.y);
			atomicAdd(&variance[to.address()].z, props.variance.z);
		}

		template <class LabelType>
		__device__ inline
		void unifyProps(LabelType from, LabelType to) {
			const area_t a = area[LabelType(from).address()];

			if (a != 0) {
				const centroid_t c = centroid[from.address()];
				const variance_t v = variance[from.address()];

				atomicAdd(&area    [to.address()]  , a  );
				atomicAdd(&centroid[to.address()].x, c.x);
				atomicAdd(&centroid[to.address()].y, c.y);
				atomicAdd(&variance[to.address()].x, v.x);
				atomicAdd(&variance[to.address()].y, v.y);
				atomicAdd(&variance[to.address()].z, v.z);

				setProps(from.address(), properties::null());
			}
		}
	#endif /* __CUDACC__ */
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_CCL_DATA_H_ */
