#ifndef LCG_CUCCL_PROPERTIES_H_
#define LCG_CUCCL_PROPERTIES_H_

#include <cuccl/types.h>
#include <cutils/vector.h>

namespace lcg {
namespace cuccl {

/**
 * Groups region properties (area, centroid and variance) in a single struct
 * with arithmetic operations.
 */
struct properties {
	area_t area;
	centroid_t centroid;
	variance_t variance;

	__HOST__DEVICE__IF_CUDA inline
	properties operator-() const {
		return {
			-this->area    ,
			-this->centroid,
			-this->variance
		};
	}

	__HOST__DEVICE__IF_CUDA inline
	properties operator+(const properties &other) const {
		return {
			this->area     + other.area    ,
			this->centroid + other.centroid,
			this->variance + other.variance
		};
	}

	__HOST__DEVICE__IF_CUDA inline
	properties operator-(const properties &other) const {
		return {
			this->area     - other.area    ,
			this->centroid - other.centroid,
			this->variance - other.variance
		};
	}

	/**
	 * Null region properties (all zeroes).
	 */
	__HOST__DEVICE__IF_CUDA inline
	static properties null() {
		return { 0, centroid_t(), variance_t() };
	}

	/**
	 * Properties of a unit pixel-region at the specified point.
	 */
	__HOST__DEVICE__IF_CUDA inline
	static properties of(const cutils::vec2i &point) {
		return {
			1,
			centroid_t(point.x, point.y),
			variance_t(point.x * point.x, point.y * point.y, point.x * point.y)
		};
	}
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_PROPERTIES_H_ */
