#include <cached_allocator.h>
#include <cstdio>
#include <cuccl/types.h>
#include <sstream>
#include <string>
#include <thrust/device_vector.h>
#include <thrust/scan.h>
#include <thrust/system/cuda/detail/par.h>

using namespace lcg::cuccl;

struct {
	int width;
	int height;
	int rounds;
} options = { 1024, 1024, 100 };

uint8_t *cpu_buffer;
uint8_t *gpu_buffer;

int main(int argc, char *argv[]) {

	thrust::extra::cached_allocator alloc;

	if (argc < 4) {
		fprintf(stderr, "Correct usage: %s <width> <height> <repetitions>\n", argv[0]);
		exit(1);
	} else {
		std::stringstream(argv[1]) >> options.width;
		std::stringstream(argv[2]) >> options.height;
		std::stringstream(argv[3]) >> options.rounds;
	}

	thrust::device_vector<label_t> gpu_buffer(options.width * options.height);

	for (int i = 0; i < options.rounds; i++) {
		thrust::inclusive_scan(thrust::cuda::par(alloc), gpu_buffer.begin(), gpu_buffer.end(), gpu_buffer.begin());
	}
}
