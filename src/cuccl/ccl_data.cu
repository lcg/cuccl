#include <cuccl/ccl_data.h>

namespace lcg {
namespace cuccl {

ccl_data ccl_data::allocDevice(const dim3 &size, bool withProps, int factor) {
	ccl_data frame;
	frame.size = size;
	CUTILS_CHECK_CALL( cudaMalloc((void**) &frame.labels, size.x * size.y * sizeof(label_t)) );
	if (withProps) {
		frame.ccSlots = size.x * size.y / factor;
		CUTILS_CHECK_CALL( cudaMalloc((void**) &frame.area    , frame.ccSlots * sizeof(    area_t)) );
		CUTILS_CHECK_CALL( cudaMalloc((void**) &frame.centroid, frame.ccSlots * sizeof(centroid_t)) );
		CUTILS_CHECK_CALL( cudaMalloc((void**) &frame.variance, frame.ccSlots * sizeof(variance_t)) );
	} else {
		frame.ccSlots = 0;
		frame.area     = nullptr;
		frame.centroid = nullptr;
		frame.variance = nullptr;
	}
	return frame;
}

ccl_data ccl_data::allocHost(const dim3 &size, bool withProps, int factor) {
	ccl_data frame;
	frame.size = size;
	frame.labels = new label_t[size.x * size.y];
	if (withProps) {
		frame.ccSlots = size.x * size.y / factor;
		frame.area     = new     area_t[frame.ccSlots];
		frame.centroid = new centroid_t[frame.ccSlots];
		frame.variance = new variance_t[frame.ccSlots];
	} else {
		frame.ccSlots = 0;
		frame.area     = nullptr;
		frame.centroid = nullptr;
		frame.variance = nullptr;
	}
	return frame;
}

void ccl_data::copyTo(ccl_data &other, cudaMemcpyKind transferKind) {
	// TODO: Review this command: it should be sync'd because of sequential relabels, but is this the right place?
	CUTILS_CHECK_CALL( cudaDeviceSynchronize() );
	CUTILS_CHECK_CALL( cudaMemcpy(other.labels, labels, size.x * size.y * sizeof(label_t), transferKind) );
	if (area != nullptr and other.area != nullptr) {
		other.ccSlots = ccSlots;
		CUTILS_CHECK_CALL( cudaMemcpy(other.area    , area    , ccSlots * sizeof(    area_t), transferKind) );
		CUTILS_CHECK_CALL( cudaMemcpy(other.centroid, centroid, ccSlots * sizeof(centroid_t), transferKind) );
		CUTILS_CHECK_CALL( cudaMemcpy(other.variance, variance, ccSlots * sizeof(variance_t), transferKind) );
	}
}

ccl_data ccl_data::withoutProps() {
	return { size, 0, labels, nullptr };
}

} /* namespace lcg::cuccl */
} /* namespace lcg */
