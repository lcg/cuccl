#include <cached_allocator.h>
#include <cuccl/basic_label.h>
#include <cuccl/ccl_data.h>
#include <cuccl/internal/image_indexing.h>
#include <cuccl/internal/relabel.h>
#include <cuda_profiler_api.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/scan.h>
#include <thrust/system/cuda/execution_policy.h>

namespace lcg {
namespace cuccl {
namespace internal {

thrust::extra::cached_allocator *alloc = nullptr;

__global__
void
markRootsKernel(const label_t *labels, label_t *rootsBuffer) {
	const int I = image_indexing::globalOffset();

	basic_label label = labels[I];

	if (label.address() == I)
		rootsBuffer[I] = 1;
	else
		rootsBuffer[I] = 0;
}

__global__
void
relabelKernel(ccl_data frame, const label_t *rootsBuffer, label_t *ccCount) {
	const int I = image_indexing::globalOffset();

	basic_label label = frame.labels[I];

	if (label.isForeground()) {
		const label_t newLabel = rootsBuffer[label.address()] - 1;

		frame.labels[I] = newLabel;

		if (frame.hasProps() and label.address() == I)
			frame.setProps(newLabel, frame.getProps(I));
	}
}

void
initRelabel(dim3 size) {
	if (alloc == nullptr) {
		alloc = new thrust::extra::cached_allocator();

		//TODO: Use context manager objects (RAII idiom) to supervise turning on and off of the profiler
		cudaProfilerStop();

		thrust::device_vector<label_t> dummyVector(size.x * size.y);
		thrust::inclusive_scan(thrust::cuda::par(*alloc), dummyVector.begin(), dummyVector.end(), dummyVector.begin());

		cudaProfilerStart();
	}
}

void
deinitRelabel() {
	if (alloc != nullptr)
		delete alloc;
}

void
relabel(const dim3 &size, ccl_data &output, label_t *rootsBuffer, label_t *ccCount) {
	const size_t block = 16 * 8;
	const size_t grid = (size.x * size.y) / block;

	initRelabel(size);

	markRootsKernel<<<grid, block>>>(output.labels, rootsBuffer); CUTILS_CHECK_LAUNCH

	thrust::device_ptr<label_t> rootsBufferBegin(rootsBuffer);
	thrust::device_ptr<label_t> rootsBufferEnd = rootsBufferBegin + size.x * size.y;
	thrust::inclusive_scan(thrust::cuda::par(*alloc), rootsBufferBegin, rootsBufferEnd, rootsBufferBegin);

	relabelKernel<<<grid, block>>>(output, rootsBuffer, ccCount); CUTILS_CHECK_LAUNCH

	if (ccCount != nullptr)
		thrust::copy_n(rootsBufferEnd - 1, 1, ccCount);
}

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */
