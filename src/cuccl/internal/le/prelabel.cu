#include <cuccl/basic_label.h>
#include <cuccl/binary_segment.h>
#include <cuccl/ccl_data.h>
#include <cuccl/properties.h>
#include <cuccl/internal/image_indexing.h>
#include <cuccl/internal/le/prelabel.h>
#include <cuccl/internal/le/textures.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace le {

__global__
void prelabelKernel(ccl_data data) {
	const cutils::vec2i P = image_indexing::globalPoint();
	const int   I = image_indexing::globalOffset(P);

	basic_label label;
	properties props;

	if (tex2D(binaryTex, P.x, P.y) != binary_segment::background()) {
		label = I;
		if (data.hasProps())
			props = properties::of(P);
	} else {
		label = basic_label::background();
		if (data.hasProps())
			props = properties::null();
	}

	data.labels[I] = label;
	if (data.hasProps())
		data.setProps(I, props);
}

void prelabel(const dim3 &size, const segment_t *binary, ccl_data output) {
	const dim3 block(8, 8);
	const dim3 grid(size.x / block.x, size.y / block.y);

	bindBinaryImageToTexture(binary, size);
	prelabelKernel<<<grid, block>>>(output); CUTILS_CHECK_LAUNCH
}

} /* namespace lcg::cuccl::internal::le */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */
