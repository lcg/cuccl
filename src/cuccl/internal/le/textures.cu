#include <cuccl/types.h>
#include <cuccl/internal/le/textures.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace le {

texture<segment_t, cudaTextureType2D> binaryTex;
texture<  label_t, cudaTextureType2D>  labelTex;

bool texturesInitialized = false;

inline
void initTextures() {
	if (not texturesInitialized) {
		binaryTex.addressMode[0] = cudaAddressModeClamp;
		binaryTex.addressMode[1] = cudaAddressModeClamp;
		binaryTex.filterMode = cudaFilterModePoint;
		binaryTex.normalized = false;

		labelTex.addressMode[0] = cudaAddressModeClamp;
		labelTex.addressMode[1] = cudaAddressModeClamp;
		labelTex.filterMode = cudaFilterModePoint;
		labelTex.normalized = false;

		texturesInitialized = true;
	}
}

//TODO: Make it a single template function parameterized with a texture object
void bindBinaryImageToTexture(const segment_t *buffer, const dim3 &size) {
	initTextures();
	cudaBindTexture2D(nullptr, binaryTex, buffer, size.x, size.y, size.x * sizeof(segment_t));
}

void bindLabelImageToTexture(const label_t *buffer, const dim3 &size) {
	initTextures();
	cudaBindTexture2D(nullptr, labelTex, buffer, size.x, size.y, size.x * sizeof(label_t));
}

} /* namespace lcg::cuccl::internal::le */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */
