#include <cuccl/basic_label.h>
#include <cuccl/ccl_data.h>
#include <cuccl/internal/image_indexing.h>
#include <cuccl/internal/transfer_props.h>
#include <cuccl/types.h>
#include <cutils/vector.h>

namespace lcg {
namespace cuccl {
namespace internal {

__global__
void
transferPropsKernel(ccl_data frame) {
	const cutils::vec2i P = image_indexing::globalPoint();
	const int I = image_indexing::globalOffset();

	const basic_label label = frame.labels[I];

	if (label.isForeground())
		frame.addProps<basic_label>(label, properties::of(P));
}

void
transfer_props(const dim3 &size, ccl_data &output) {
	dim3 block(16, 16);
	dim3 grid(size.x / block.x, size.y / block.y);
	transferPropsKernel<<<grid, block>>>(output); CUTILS_CHECK_LAUNCH
}

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */
