#include <cuccl/error_ellipse.h>

namespace lcg {
namespace cuccl {

__host__ __device__
error_ellipse::error_ellipse(const variance_t &variances, const centroid_t &centroid, float area) {
	covmatrix[0][0] = variances.x / area - cutils::sqr(centroid.x / area);
	covmatrix[0][1] = variances.z / area - centroid.x / area * centroid.y / area;
	covmatrix[1][0] = variances.z / area - centroid.x / area * centroid.y / area;
	covmatrix[1][1] = variances.y / area - cutils::sqr(centroid.y / area);

	const float &a = covmatrix[0][0], &b = covmatrix[0][1], &c = covmatrix[1][0], &d = covmatrix[1][1];
	const float trace = a + d, determ = a * d - b * c;

	eigenval[0] = trace / 2 + sqrt(cutils::sqr(trace) / 4 - determ);
	eigenval[1] = trace / 2 - sqrt(cutils::sqr(trace) / 4 - determ);

	if (b) {
		eigenvec[0] = cutils::vec2f(b, eigenval[0] - a);
		eigenvec[1] = cutils::vec2f(b, eigenval[1] - a);

		eigenvec[0] = eigenvec[0] / sqrt(cutils::sqr(eigenvec[0].x) + cutils::sqr(eigenvec[0].y));
		eigenvec[1] = eigenvec[1] / sqrt(cutils::sqr(eigenvec[1].x) + cutils::sqr(eigenvec[1].y));
	} else if (c) {
		eigenvec[0] = cutils::vec2f(eigenval[0] - a, c);
		eigenvec[1] = cutils::vec2f(eigenval[1] - a, c);

		eigenvec[0] = eigenvec[0] / sqrt(cutils::sqr(eigenvec[0].x) + cutils::sqr(eigenvec[0].y));
		eigenvec[1] = eigenvec[1] / sqrt(cutils::sqr(eigenvec[1].x) + cutils::sqr(eigenvec[1].y));
	} else {
		eigenvec[0] = cutils::vec2f(1, 0);
		eigenvec[1] = cutils::vec2f(0, 1);
	}
}

__host__ __device__
error_ellipse::error_ellipse(const properties &props) : error_ellipse(props.variance, props.centroid, props.area) {
}

__host__ __device__
float error_ellipse::angle() const {
	return cutils::vec2i(1, 0).signedAngle(eigenvec[0]);
}

__host__ __device__
float error_ellipse::area() const {
	return 4 * M_PI * sqrt(eigenval[0] * eigenval[1]);
}

} /* namespace lcg::cuccl */
} /* namespace lcg */
