#include <cuccl/ccl_data.h>
#include <cuccl/internal/any_smaller.h>
#include <cuccl/internal/neighbors_8.h>
#include <cuccl/internal/neighbors_8_scan.h>
#include <cuccl/internal/relabel.h>
#include <cuccl/internal/smallest.h>
#include <cuccl/internal/transfer_props.h>
#include <cuccl/internal/le/analysis.h>
#include <cuccl/internal/le/prelabel.h>
#include <cuccl/internal/le/scan.h>
#include <cuccl/le.h>
#include <cutils/vector.h>

using namespace lcg::cutils;

namespace lcg {
namespace cuccl {

LabelEquivalence::Options::Options() :
		computePropertiesLater   (false),
		pruneNeighborsInFirstScan(false),
		sequentialRelabel        (true ),
		uncheckedIterations      (0    ),
		useLocalFlagInScan       (true ),
		useNwNeighborsInFirstScan(true ) {
}

LabelEquivalence::LabelEquivalence(const dim3 &size, Options options) :
		options(options), size(size), seqlabels(nullptr) {
	if (options.sequentialRelabel)
		CUTILS_CHECK_CALL( cudaMalloc((void**) &seqlabels, size.x * size.y * sizeof(label_t)) );
}

LabelEquivalence::~LabelEquivalence() {
	if (seqlabels != nullptr)
	CUTILS_CHECK_CALL( cudaFree(seqlabels) );
}

void LabelEquivalence::ccl(const segment_t *binary, ccl_data &output) {
	ccl_data frame;
	if (output.hasProps() and options.computePropertiesLater)
		frame = output.withoutProps();
	else
		frame = output;

	// Prelabel
	internal::le::prelabel(size, binary, frame);

	// First scan
	if (options.pruneNeighborsInFirstScan)
		if (options.useNwNeighborsInFirstScan)
			internal::le::scan(size, frame.labels, nullptr, internal::any_smaller_neighbor<internal::neighbors_8_scan>(), options.useLocalFlagInScan);
		else
			internal::le::scan(size, frame.labels, nullptr, internal::any_smaller_neighbor<internal::neighbors_8     >(), options.useLocalFlagInScan);
	else
		if (options.useNwNeighborsInFirstScan)
			internal::le::scan(size, frame.labels, nullptr, internal::smallest_neighbor   <internal::neighbors_8_scan>(), options.useLocalFlagInScan);
		else
			internal::le::scan(size, frame.labels, nullptr, internal::smallest_neighbor   <internal::neighbors_8     >(), options.useLocalFlagInScan);

	// Unchecked iterations
	for (int i = 0; i < options.uncheckedIterations - 1; i++) {
		internal::le::analysis(size, frame);
		internal::le::scan(size, frame.labels, nullptr, internal::smallest_neighbor<internal::neighbors_8_scan>(), options.useLocalFlagInScan);
	}

	// Checked iterations
	do {
		internal::le::analysis(size, frame);

		*changed = false;
		internal::le::scan(size, frame.labels, &changed, internal::smallest_neighbor<internal::neighbors_8_scan>(), options.useLocalFlagInScan);
		CUTILS_CHECK_CALL(cudaDeviceSynchronize());
	} while (changed);

	// Sequential relabel
	if (seqlabels != nullptr)
		internal::relabel(size, frame, seqlabels, &output.ccSlots);

	// Post-computation of properties
	if (output.hasProps() and options.computePropertiesLater) {
		CUTILS_CHECK_CALL( cudaDeviceSynchronize() );
		CUTILS_CHECK_CALL( cudaMemset(output.area    , 0, output.ccSlots * sizeof(    area_t)) );
		CUTILS_CHECK_CALL( cudaMemset(output.centroid, 0, output.ccSlots * sizeof(centroid_t)) );
		CUTILS_CHECK_CALL( cudaMemset(output.variance, 0, output.ccSlots * sizeof(variance_t)) );
		internal::transfer_props(size, output);
	}
}

} /* namespace lcg::cuccl */
} /* namespace lcg */
