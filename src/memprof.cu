#include <cstdio>
#include <cuccl/types.h>
#include <sstream>
#include <string>

using namespace lcg::cuccl;

struct {
	int bytes;
	int rounds;
	bool pinned;
} options = { 1, 100, false};

uint8_t *cpu_buffer;
uint8_t *gpu_buffer;

int main(int argc, char *argv[]) {

	if (argc < 3) {
		fprintf(stderr, "Correct usage: %s <objects> <repetitions> [-p]\n", argv[0]);
		exit(1);
	} else {
		std::stringstream(argv[1]) >> options.bytes;
		std::stringstream(argv[2]) >> options.rounds;
		options.pinned = (argc > 3 and std::string(argv[3]) == "-p");
	}

	if (options.pinned)
		cudaMallocHost((void**) &cpu_buffer, options.bytes);
	else
		cpu_buffer = new uint8_t[options.bytes];

	cudaMalloc((void**) &gpu_buffer, options.bytes);

	for (int i = 0; i < options.rounds; i++) {
		cudaMemcpy(cpu_buffer, gpu_buffer, options.bytes, cudaMemcpyDeviceToHost);
	}

	if (options.pinned)
		cudaFreeHost(cpu_buffer);
	else
		delete[] cpu_buffer;

	cudaFree(gpu_buffer);
}
