#ifndef LCG_CUCCL_COMMON_CV_IO_H_
#define LCG_CUCCL_COMMON_CV_IO_H_

#include <string>

#include "cv_traits.h"
#include <cutils/wrappers/hosted.h>
#include <cuccl/image.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace lcg {
namespace cuccl {

/**
 * \brief Utility functions image IO and OpenCV interoperability.
 */
class cv_io {
public:

	/**
	 * Converts the given img to an OpenCV matrix. Pixel data is shared, and pixels may not be modified.
	 * @param img
	 * @return
	 */
	template <class T, int W, int H>
	static const cv::Mat* imgAsMatrix(const array_2d<T, W, H> *img) {
		return new cv::Mat(
				array_2d<T, W, H>::height,
				array_2d<T, W, H>::width,
				cv_traits<T>::cvtype,
				img->data);
	}

	/**
	 * Converts the given img to an OpenCV matrix. Pixel data is shared.
	 * @param img
	 * @return
	 */
	template <class T, int W, int H>
	static cv::Mat* imgAsMatrix(array_2d<T, W, H> *img) {
		return new cv::Mat(
				array_2d<T, W, H>::height,
				array_2d<T, W, H>::width,
				cv_traits<T>::cvtype,
				img->data);
	}

	/**
	 * Converts the given img to an OpenCV matrix. Pixel data is copied.
	 * @param img
	 * @return
	 */
	template <class T, int W, int H>
	static cv::Mat* imgToMatrix(const array_2d<T, W, H> *img) {
		cv::Mat *matrix = new cv::Mat(
				array_2d<T, W, H>::height,
				array_2d<T, W, H>::width,
				cv_traits<T>::cvtype);
		memcpy(matrix->data, img->data, array_2d<T, W, H>::size);
		return matrix;
	}

	template <class ImageType>
	static ImageType* matrixToImg(const cv::Mat *matrix) {
		ImageType *img = new ImageType;

		cv::Mat tempmat1, tempmat2;
		matrix->convertTo(tempmat1, cv::DataType<typename ImageType::type>::type);
		cv::resize(tempmat1, tempmat2, cv::Size(ImageType::width, ImageType::height), 0, 0, cv::INTER_NEAREST);

		memcpy(img->data, tempmat2.data, ImageType::size);

		return img;
	}

	/**
	 * \brief Loads a grayscale img from a file as an image of the template type.
	 * @param filename Valid URL to img file.
	 * @return
	 */
	template <class ImageType>
	static ImageType* load(const std::string &filename) {
		cv::Mat tempmat1 = cv::imread(filename, 0);

		//TODO: Replace by custom exception type
		if (tempmat1.data == nullptr)
			throw std::string("Could not load image file ") + filename;

		cv::Mat tempmat2;
		tempmat1.convertTo(tempmat2, cv::DataType<typename ImageType::type>::type);
		cv::resize(tempmat2, tempmat1, cv::Size(ImageType::width, ImageType::height), 0, 0, cv::INTER_NEAREST);

		ImageType *img = new ImageType;
		memcpy(img->data, tempmat1.data, ImageType::size);

		return img;
	}
};

} /* namespace cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_COMMON_CV_IO_H_ */
