#ifndef DUMPER_H_
#define DUMPER_H_

#include <cstdint>

namespace lcg {
namespace cuccl {

class ccl_data;

}
}


namespace boost {
namespace filesystem {

class path;

}
}


class Dumper {
public:
	virtual ~Dumper();
	virtual void dump(size_t frame, lcg::cuccl::ccl_data &data) = 0;
};

#endif /* DUMPER_H_ */
