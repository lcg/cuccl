#ifndef LCG_CUCCL_COMMON_CV_TRAITS_H_
#define LCG_CUCCL_COMMON_CV_TRAITS_H_

#include <opencv2/core/core.hpp>

namespace lcg {
namespace cuccl {

template<class Type>
struct cv_traits {
};

template<class Type, int cv_type>
struct cv_traits_base {
	static const int cvtype = cv_type;
};

template<>
struct cv_traits<uint8_t> :
		std::numeric_limits<uint8_t>,
		cv_traits_base<uint8_t, CV_8UC1> {
};

template<>
struct cv_traits<uint16_t> :
		std::numeric_limits<uint16_t>,
		cv_traits_base<uint16_t, CV_16UC1> {
};

template<>
struct cv_traits<uint32_t> :
		std::numeric_limits<uint32_t>,
		cv_traits_base<uint32_t, CV_8UC4> {
};

} /* namespace cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_COMMON_CV_TRAITS_H_ */
