#ifndef DRIVER_H_
#define DRIVER_H_

#include "input.h"
#include "output.h"

#include <boost/filesystem.hpp>
#include <cuccl/ccl_data.h>
#include <cuccl/types.h>

namespace lcg {
namespace cuccl {

class ccl_algorithm;

} /* namespace lcg::cuccl */
} /* namespace */


class Dumper;


class Driver {
public:
	enum Algorithm {
		LE,  // Label Equivalence
		LLE, // Local Label Equivalence
		TM,  // Tile Merging
	};

	enum OperationMode {
		PROFILE,
		PROFILE_NO_MEM,
		VISUALIZE
	};

	enum PropertiesMode {
		YES, NO, LATE
	};

	struct Options {
		Algorithm algorithm;
		OperationMode mode;
		PropertiesMode propsMode;

		size_t someframes;
		size_t frameRepetitions;

		boost::filesystem::path inputPath;

		dim3 outputSize;

		bool blackForeground;
		bool recolor;
		bool wait;
		bool quiet;

		struct {
			bool pruneNeighborsInFirstScan;
			bool sequentialRelabel;
			int  uncheckedIterations;
			bool useLocalFlagInScan;
			bool useNwNeighborsInFirstScan;

			struct {
				int tileSize;
				int blockFactor;
			} tm;

			int lateFactor;
		} algopts;

		struct {
			boost::filesystem::path csv;
		} dump;
	};

	Driver(const Options &options);

	int run();

private:
	Options options;

	size_t totalFrames;

	lcg::cuccl::ccl_data gpuccl_data, cpuccl_data;
	lcg::cutils::hosted<lcg::cuccl::segment_image> cpu_binary;
	lcg::cutils::device<lcg::cuccl::segment_image> gpu_binary;

	// Dynamically allocated abstract objects
	Source *video;
	Terminal *screen;
	Dumper *dumper;
	lcg::cuccl::ccl_algorithm *labeler;
};

#endif /* DRIVER_H_ */
