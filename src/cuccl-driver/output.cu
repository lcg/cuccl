#include "cv_traits.h"
#include "output.h"
#include <cuccl/ccl_data.h>
#include <cuccl/error_ellipse.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace lcg::cuccl;
using namespace lcg::cutils;

void drawProps(ccl_data &cpuccl_data, cv::Mat &colorMatNoAlpha);

struct ScreenTerminal : Terminal {

	ScreenTerminal(const dim3 &frameSize, const dim3 &outputSize, float fps, bool drawEllipses, bool wait);
	~ScreenTerminal();
	virtual void draw(ccl_data &cpuccl_data);
	virtual bool quitRequested() const;

private:
	cv::Size _frameSize, _outputSize;
	float _fps;
	bool _drawEllipses, _wait, _shouldQuit;
};

Terminal* Terminal::getScreen(const dim3 &frameSize, const dim3 &outputSize, float fps, bool drawEllipses, bool wait) {
	return new ScreenTerminal(frameSize, outputSize, fps, drawEllipses, wait);
}

Terminal::~Terminal() {
	cv::destroyWindow("Result");
}

ScreenTerminal::ScreenTerminal(const dim3 &frameSize, const dim3 &outputSize, float fps, bool drawEllipses, bool wait) {
	_frameSize = cv::Size(frameSize.x, frameSize.y);
	_outputSize = cv::Size(outputSize.x, outputSize.y);
	_fps = fps;
	_drawEllipses = drawEllipses;
	_wait = wait;
	_shouldQuit = false;
}

ScreenTerminal::~ScreenTerminal() {}

void ScreenTerminal::draw(ccl_data &cpuccl_data) {
	cv::Mat colorMat(
		_frameSize,
		cv_traits<label_t>::cvtype,
		cpuccl_data.labels
	);

	cv::Mat colorMatNoAlpha;
	cv::cvtColor(colorMat, colorMatNoAlpha, CV_BGRA2BGR);

	if (_drawEllipses)
		drawProps(cpuccl_data, colorMatNoAlpha);

	cv::resize(colorMatNoAlpha, colorMatNoAlpha, _outputSize);
	cv::imshow("Result", colorMatNoAlpha);

	char opt;

	if (_wait) {
		do {
			opt = cv::waitKey() & 255;
		} while(opt != ' ' and opt != 'q');
	} else {
		opt = cv::waitKey(1000 / _fps) & 255;
	}

	_shouldQuit = (opt == 'q');
}

bool ScreenTerminal::quitRequested() const {
	return _shouldQuit;
}

void drawProps(ccl_data &cpuccl_data, cv::Mat &colorMatNoAlpha) {
	//TODO: Do these guys deserve to be parameterized?
	const cv::Scalar RED(0,0,255), GREEN(0,255,0), BLUE(255,0,0), CYAN(255,255,0), MAGENTA(255,0,255), YELLOW(0,255,255), WHITE(255,255,255), BLACK(0,0,0), GRAY(128,128,128);
	const int lineThickness = 2;

	CUTILS_CHECK_CALL( cudaDeviceSynchronize() );

	//TODO: Create cc_iterator classes to replace manual iteration of slots
	for (label_t i = 0; i < cpuccl_data.ccSlots; i++) {
		if (cpuccl_data.area[i] > 0) {
			float area = cpuccl_data.area[i];
			vec2f centroid = {
					cpuccl_data.centroid[i].x / area,
					cpuccl_data.centroid[i].y / area
			};
			vec3f variance = {
				cpuccl_data.variance[i].x / area - sqr(centroid.x),
				cpuccl_data.variance[i].y / area - sqr(centroid.y),
				cpuccl_data.variance[i].z / area - centroid.x * centroid.y
			};

			error_ellipse covmat(cpuccl_data.variance[i], cpuccl_data.centroid[i], cpuccl_data.area[i]);

			const float error_ellipse_radii[2] = {
					2 * sqrt(abs(covmat.eigenval[0])),
					2 * sqrt(abs(covmat.eigenval[1]))
			};

			const float ellipse_aim = 180 * covmat.angle() / M_PI;
			const float factor =
				(error_ellipse_radii[0] != 0 and error_ellipse_radii[1] != 0)   ?
				area / (M_PI * error_ellipse_radii[0] * error_ellipse_radii[1]) :
				0.0f;

			const float area_ellipse_radii[2] = {
					sqrt(factor) * error_ellipse_radii[0],
					sqrt(factor) * error_ellipse_radii[1]
			};

			try {
				cv::Scalar color = CYAN;
				cv::Point  centroid(
					cpuccl_data.centroid[i].x / area,
					cpuccl_data.centroid[i].y / area);

				// Major PCA axis
				cv::line(colorMatNoAlpha, centroid,
					cv::Point(
						centroid.x + sqrt(abs(covmat.eigenval[0])) * covmat.eigenvec[0].x,
						centroid.y + sqrt(abs(covmat.eigenval[0])) * covmat.eigenvec[0].y),
					color, lineThickness);

				// Minor PCA axis
				cv::line(colorMatNoAlpha, centroid,
					cv::Point(
						centroid.x + sqrt(abs(covmat.eigenval[1])) * covmat.eigenvec[1].x,
						centroid.y + sqrt(abs(covmat.eigenval[1])) * covmat.eigenvec[1].y),
					color, lineThickness);


				// Error ellipse
				if (error_ellipse_radii[0] > 1 and error_ellipse_radii[1] > 1)
					cv::ellipse(colorMatNoAlpha, centroid,
						cv::Size(error_ellipse_radii[0], error_ellipse_radii[1]),
						ellipse_aim, 0, 360, color, lineThickness);

				// True area ellipse aligned with error ellipse
				if (area_ellipse_radii[0] > 1 and area_ellipse_radii[1] > 1)
					cv::ellipse(colorMatNoAlpha, centroid,
						cv::Size(area_ellipse_radii[0], area_ellipse_radii[1]),
						ellipse_aim, 0, 360, 0.5 * color, lineThickness);
			} catch (cv::Exception &ex) {
			}
		}
	}
}
