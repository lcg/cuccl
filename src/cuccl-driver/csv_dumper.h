#ifndef CSV_DUMPER_H_
#define CSV_DUMPER_H_

#include "dumper.h"
#include <fstream>

namespace lcg {
namespace cuccl {

class ccl_data;

}
}

namespace boost {
namespace filesystem {

class path;

}
}

class CSVDumper: public Dumper {
public:
	CSVDumper(const boost::filesystem::path &path);
	virtual ~CSVDumper();
	void dump(size_t frame, lcg::cuccl::ccl_data &data);

private:
	std::ofstream outputStream;
};

#endif /* CSV_DUMPER_H_ */
