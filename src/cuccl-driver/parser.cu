#include "driver.h"
#include "parser.h"

#include <cstdint>

#include <boost/program_options.hpp>
#include <cuccl/ccl.h>
#include <cuccl/version.h>

namespace fs = boost::filesystem;
namespace po = boost::program_options;

using namespace lcg::cuccl;
using namespace lcg::cutils;

//TODO: parse options using boost::program_options
Driver::Options parseArgs(int argc, char** argv) {
	namespace po = boost::program_options;
	using std::cerr;
	using std::cout;
	using std::endl;

	const std::string programTitle = "CuCCL driver/profiling application";

	Driver::Options programArgs;

	po::options_description programOptions(programTitle);

	programOptions.add_options()
		("algorithm,a",
			po::value<std::string>()
				->required()->notifier([&](const std::string &algorithmArg) {
					std::map<std::string, Driver::Algorithm> algorithmMap = {
						{ "le" , Driver::Algorithm::LE  },
						{ "lle", Driver::Algorithm::LLE },
						{ "tm" , Driver::Algorithm::TM  },
					};

					if (algorithmMap.find(algorithmArg) == algorithmMap.end()) {
						cerr << "Unrecognized algorithm: " << algorithmArg << endl;
						exit(EXIT_FAILURE);
					} else {
						programArgs.algorithm = algorithmMap[algorithmArg];
					}
				}),
			"Specify which algorithm will be used. Accepted values: \"le\","
			"\"lle\" and \"tm\".")

		("black-foreground,b",
			po::value<bool>(&programArgs.blackForeground)
				->implicit_value(true)-> default_value(false)->zero_tokens(),
			"Assumes input image/video has black foreground.")

		("dump.csv",
			po::value<fs::path>(&programArgs.dump.csv)
				->default_value("")->implicit_value("cuccl.csv"),
			"[path, empty for default] Output CSV file containing per frame"
			"informations (like CC count, CC properties, etc.). If not used,"
			"no output is written.")

		("frame-repetitions,f",
			po::value<size_t>(&programArgs.frameRepetitions)
				->default_value(1),
			"[integer] Specify how many times each frame should be processed "
			"(does not affect visualization)."
		)

		("help,h",
			"Show this help text and exit.")

		("input,i",
			po::value<fs::path>(&programArgs.inputPath)
				->required(),
			"[path] Specify input image/video file.")

		("late-factor,k",
			po::value<int>(&programArgs.algopts.lateFactor)
				->required()->default_value(64),
			"[integer] Memory reduction factor for property arrays (only relevant"
			" when -p=late).")

		("limit,l",
			po::value<size_t>(&programArgs.someframes)
				->default_value(std::numeric_limits<size_t>::max()),
			"[integer] Stop at this frame when processing videos.")

		("mode,m",
			po::value<std::string>()
				->required()->notifier([&](const std::string &modeArg) {
					std::map<std::string, Driver::OperationMode> modeMap = {
						{ "vis"        , Driver::OperationMode::VISUALIZE      },
						{ "prof"       , Driver::OperationMode::PROFILE        },
						{ "prof-no-mem", Driver::OperationMode::PROFILE_NO_MEM },
					};

					if (modeMap.find(modeArg) == modeMap.end()) {
						cerr << "Unrecognized operation mode: " << modeArg << endl;
						exit(EXIT_FAILURE);
					} else {
						programArgs.mode = modeMap[modeArg];
					}
				}),
			"[prof, prof-no_mem, vis] Selects operation mode: \"vis\"(visulize), "
			"\"prof\" (profile) and \"prof-no-mem\" (profile without accounting"
			"for memory time).")

		("properties,p",
			po::value<std::string>()
				->required()->notifier([&](const std::string &modeArg) {
					std::map<std::string, Driver::PropertiesMode> modeMap = {
						{ "yes" , Driver::PropertiesMode::YES  },
						{ "no"  , Driver::PropertiesMode::NO   },
						{ "late", Driver::PropertiesMode::LATE },
					};

					if (modeMap.find(modeArg) == modeMap.end()) {
						cerr << "Unrecognized properties mode: " << modeArg << endl;
						exit(EXIT_FAILURE);
					} else {
						programArgs.propsMode = modeMap[modeArg];
					}
				}),
			"[yes, no, late] Whether or not to compute and display (if "
			"--mode=\"vis\" is used) component properties, or to compute them "
			"after relabeling.")

		("quiet,q",
			po::value<bool>(&programArgs.quiet)
				->implicit_value(true)->default_value(false)->zero_tokens(),
			"Suppress all terminal output.")

		("recolor,r",
			po::value<bool>(&programArgs.recolor)
				->implicit_value(true)-> default_value(false)->zero_tokens(),
			"Paint connected components with more evenly spaced colors.")

		("screen.x,x",
			po::value<unsigned>(&programArgs.outputSize.x)
				->default_value(LCG_CUCCL_MAX_WIDTH),
			"[integer] Output screen width. Only used if --mode=\"vis\".")

		("screen.y,y",
			po::value<unsigned>(&programArgs.outputSize.y)
				->default_value(LCG_CUCCL_MAX_HEIGHT),
			"[integer] Output screen height. Only used if --mode=\"vis\".")

		("version",
			"Display version and compilation information and exit.")

		("wait,w",
			po::value<bool>(&programArgs.wait)
				->implicit_value(true)-> default_value(false)->zero_tokens(),
			"Awaits for the user to press a key at every frame before "
			"proceeding to the next one (ignored if -v not supplied).")

		("pruneNeighborsInFirstScan",
			po::value<bool>(&programArgs.algopts.pruneNeighborsInFirstScan)
				->default_value(false),
			"[true/false] Stop neighbor search during first scan once a proper"
			"neighbor in raster order is found. Affects only --algorithm=\"le\"."
			)

		("sequentialRelabel,s",
			po::value<bool>(&programArgs.algopts.sequentialRelabel)
				->default_value(false)->implicit_value(true)->zero_tokens(),
			"Relabel all connected components in a frame with consecutive"
			"labels. Affects all algorithms."
			)

		("uncheckedIterations",
			po::value<int>(&programArgs.algopts.uncheckedIterations)
				->default_value(0),
			"[integer] Perform a fixed number of iterations before"
			"checking if additional iterations are really needed. Affects "
			"--algorithm=\"le\", --algorithm=\"lle\" and --algorithm=\"tm\"."
			)

		("useLocalFlagInScan",
			po::value<bool>(&programArgs.algopts.useLocalFlagInScan)
				->default_value(true),
			"[true/false] Use a local shared memory flag for annotating changes"
			"during label equivalence scans. Affects --algorithm=\"le\", "
			"--algorithm=\"lle\" and --algorithm=\"tm\"."
			)

		("useNwNeighborsInFirstcan",
			po::value<bool>(&programArgs.algopts.useNwNeighborsInFirstScan)
				->default_value(true),
			"[true/false] Search for minimal labels only in neighbors with a"
			"lesser raster value during the first scan. Affects "
			"--algorithm=\"le\" and --algorithm=\"lle\"."
			)

		("tm.blockFactor",
			po::value<int>(&programArgs.algopts.tm.blockFactor)
				->default_value(4),
			"[integer] Number of tiles merged in x and y directions by merge "
			"phase when --algorithm=\"tm\".")

		("tm.tileSize",
			po::value<int>(&programArgs.algopts.tm.tileSize)
				->default_value(16),
			"[integer] Size of tiles (in pixels) used in local phase when"
			"--algorithm=\"tm\".")
		;

	po::variables_map argumentMap;
	po::store(po::parse_command_line(argc, argv, programOptions), argumentMap);

	if (argumentMap.count("help")) {
		cout << programOptions << endl;
		exit(EXIT_SUCCESS);
	}

	if (argumentMap.count("version")) {
		cout
			<< programTitle << endl
			<< "v" << VERSION_STRING << endl
			<< "Compiled for "
			<< LCG_CUCCL_MAX_WIDTH  << " x "
			<< LCG_CUCCL_MAX_HEIGHT << " resolution" << endl;
		exit(EXIT_SUCCESS);
	}

	po::notify(argumentMap);

	return programArgs;
}
