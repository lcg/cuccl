#include "csv_dumper.h"
#include "driver.h"
#include "dumper.h"
#include "parser.h"

#include <cuccl/basic_label.h>
#include <cuccl/ccl.h>
#include <cuccl/ccl_data.h>
#include <cuccl/ccl_algorithm.h>
#include <cuccl/version.h>
#include <cuccl/internal/image_indexing.h>
#include <cuccl/internal/relabel.h>
#include <cuda_profiler_api.h>
#include <set>

namespace fs = boost::filesystem;

using namespace lcg::cuccl;
using namespace lcg::cutils;

__global__
void computeBegin() {}

__global__
void computeEnd() {}

__global__
void frameBegin() {}

__global__
void frameEnd() {}

__global__
void scrambleColorsKernel(label_t *img);

__global__
void swapBlackWhiteKernel(label_t *img);

//TODO: Implement more methods: All of Hawick's methods; Jung's version of Label Equivalence; Kalentev's Row-Col Unification

Driver::Driver(const Driver::Options &options):
		options(options), video(nullptr), screen(nullptr), dumper(nullptr), labeler(nullptr) {

	const std::set<fs::path> imgExt = { ".jpg", ".png" };
	const std::set<fs::path> vidExt = { ".avi", ".mp4" };
	const std::set<fs::path> lstExt = { ".txt" };

	if (imgExt.find(options.inputPath.extension()) != imgExt.end()) {
		video = Source::openImage(options.inputPath, options.blackForeground, options.frameRepetitions);
	} else if (vidExt.find(options.inputPath.extension()) != vidExt.end()) {
		video = Source::openVideo(options.inputPath, options.blackForeground);
	} else if (lstExt.find(options.inputPath.extension()) != lstExt.end()) {
		video = Source::openImageList(options.inputPath, options.blackForeground, options.frameRepetitions);
	} else {
		std::cerr << "Unrecognized input extension: " << options.inputPath.extension();
		exit(EXIT_FAILURE);
	}

	if (options.quiet) {
		std::cout.rdbuf(nullptr);
		stdout = fopen("/dev/null", "w");
	}

	screen = Terminal::getScreen(
		video->frameSize(),
		options.outputSize,
		video->fps(),
		options.propsMode != PropertiesMode::NO,
		options.wait
	);

	cpuccl_data = ccl_data::allocHost  (video->frameSize(), options.propsMode != PropertiesMode::NO, (options.propsMode == PropertiesMode::LATE) ? options.algopts.lateFactor : 1);
	gpuccl_data = ccl_data::allocDevice(video->frameSize(), options.propsMode != PropertiesMode::NO, (options.propsMode == PropertiesMode::LATE) ? options.algopts.lateFactor : 1);
	totalFrames = min(video->frames(), options.someframes);

	LabelEquivalence::Options le_options;
	LocalLabelEquivalence::Options lle_options;
	TileMerging::Options tm_options;

	switch (options.algorithm) {
	case Driver::Algorithm::LE:
		le_options.computePropertiesLater    = (options.propsMode == PropertiesMode::LATE);
		le_options.pruneNeighborsInFirstScan = options.algopts.pruneNeighborsInFirstScan;
		le_options.sequentialRelabel         = options.algopts.sequentialRelabel        ;
		le_options.uncheckedIterations       = options.algopts.uncheckedIterations      ;
		le_options.useLocalFlagInScan        = options.algopts.useLocalFlagInScan       ;
		le_options.useNwNeighborsInFirstScan = options.algopts.useNwNeighborsInFirstScan;

		labeler = new LabelEquivalence(label_image::dims, le_options);
		break;

	case Driver::Algorithm::LLE:
		lle_options.computePropertiesLater    = (options.propsMode == PropertiesMode::LATE);
		lle_options.sequentialRelabel         = options.algopts.sequentialRelabel        ;
		lle_options.uncheckedIterations       = options.algopts.uncheckedIterations      ;
		lle_options.useLocalFlagInScan        = options.algopts.useLocalFlagInScan       ;
		lle_options.useNwNeighborsInFirstScan = options.algopts.useNwNeighborsInFirstScan;

		labeler = new LocalLabelEquivalence(label_image::dims, lle_options);
		break;

	case Driver::Algorithm::TM:
		tm_options.computePropertiesLater = (options.propsMode == PropertiesMode::LATE);
		tm_options.sequentialRelabel      = options.algopts.sequentialRelabel        ;
		tm_options.blockFactor            = options.algopts.tm.blockFactor;
		tm_options.tileSize               = options.algopts.tm.tileSize;

		labeler = new TileMerging(label_image::dims, tm_options);
		break;
	}

	if (options.dump.csv != "") {
		dumper = new CSVDumper(options.dump.csv);
	}
}

//TODO: Use producer/consumer buffers to reduce waiting time for frames
int Driver::run() {
	internal::initRelabel(video->frameSize());

	for (int i = 0; video->read(cpu_binary) and i < totalFrames; i++) {
		gpu_binary = cpu_binary;

		frameBegin<<<1, 1>>>(); CUTILS_CHECK_LAUNCH
		computeBegin<<<1, 1>>>(); CUTILS_CHECK_LAUNCH
		labeler->ccl(gpu_binary->data, gpuccl_data);
		computeEnd<<<1, 1>>>(); CUTILS_CHECK_LAUNCH

		fprintf(stdout, "Progress: %d / %lu frames -- %.2f%%\r",
			i + 1,
			totalFrames,
			100.0f * (i + 1) / totalFrames
		);
		fflush(stdout);

		if (options.mode == OperationMode::VISUALIZE) {
			if (options.recolor)
				scrambleColorsKernel<<<video->framePixels() / 256, 256>>>(gpuccl_data.labels); CUTILS_CHECK_LAUNCH
			swapBlackWhiteKernel<<<video->framePixels() / 256, 256>>>(gpuccl_data.labels); CUTILS_CHECK_LAUNCH
		}

		if (options.mode == OperationMode::PROFILE_NO_MEM) {
			CUTILS_CHECK_CALL( cudaDeviceSynchronize() );
			cpuccl_data.ccSlots = gpuccl_data.ccSlots;
		} else {
			gpuccl_data.copyTo(cpuccl_data, cudaMemcpyDeviceToHost);
		}

		frameEnd<<<1, 1>>>(); CUTILS_CHECK_LAUNCH

		if (options.mode == OperationMode::VISUALIZE) {
			screen->draw(cpuccl_data);
			if (screen->quitRequested())
				video->close();
		}

		if (dumper != nullptr)
			dumper->dump(i, cpuccl_data);
	}

	internal::deinitRelabel();

	CUTILS_CHECK_CALL( cudaProfilerStop() );

	fprintf(stdout, "\n");

	delete labeler;
	delete video;
	delete screen;
	delete dumper;

	return EXIT_SUCCESS;
}

__global__
void scrambleColorsKernel(label_t *img) {
	const int I = cuda_index::index1d1();

	const label_t label = img[I];

	if (label != basic_label::background()) {
		const label_t r = (label & 0x00000F00) << 12;
		const label_t g = (label & 0x000000F0) << 8;
		const label_t b = (label & 0x0000000F) << 4;
		img[I] = r | g | b;
	}
}

__global__
void swapBlackWhiteKernel(label_t *img) {
	const int I = internal::image_indexing::globalOffset();

	if (img[I] == basic_label::background())
		img[I] = 0xFF000000;
}
