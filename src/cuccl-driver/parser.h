#ifndef PARSER_H_
#define PARSER_H_

#include "driver.h"

Driver::Options parseArgs(int argc, char **argv);

#endif /* PARSER_H_ */
