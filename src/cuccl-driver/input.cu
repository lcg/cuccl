#include "input.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <fstream>
#include <vector>

using namespace lcg::cuccl;
using namespace lcg::cutils;
namespace fs = boost::filesystem;

struct ImageSource : Source {
	ImageSource(const fs::path &path, bool blackForeground, size_t repeat);
	virtual ~ImageSource();
	virtual void close();
	virtual float fps() const;
	virtual size_t frames() const;
	virtual size_t framePixels() const;
	virtual dim3 frameSize() const;
	virtual bool read(hosted<segment_image> &buffer);

private:
	cv::Mat _frame;
	size_t _repeat;
};

struct ImageListSource : Source {
	ImageListSource(const fs::path &path, bool blackForeground, size_t repeat);
	virtual ~ImageListSource();
	virtual void close();
	virtual float fps() const;
	virtual size_t frames() const;
	virtual size_t framePixels() const;
	virtual dim3 frameSize() const;
	virtual bool read(hosted<segment_image> &buffer);

private:
	std::vector<fs::path> image_paths;
	size_t image_index, repeat_index, repetitions;
	bool blackForeground;
	dim3 frame_size;
};

struct VideoSource : Source {
	VideoSource(const fs::path &path, bool blackForeground);
	virtual ~VideoSource();
	virtual void close();
	virtual float fps() const;
	virtual size_t frames() const;
	virtual size_t framePixels() const;
	virtual dim3 frameSize() const;
	virtual bool read(hosted<segment_image> &buffer);

private:
	cv::VideoCapture source;
	float _fps;
	size_t _frames;
	cv::Size _frameSize;
	bool _blackForeground;
};

Source* Source::openImage(const fs::path &path, bool blackForeground, size_t repeat) {
	return new ImageSource(path, blackForeground, repeat);
}

Source* Source::openImageList(const fs::path &path, bool blackForeground, size_t repeat) {
	return new ImageListSource(path, blackForeground, repeat);
}

Source* Source::openVideo(const fs::path &path, bool blackForeground) {
	return new VideoSource(path, blackForeground);
}

Source::~Source() {}

ImageSource::ImageSource(const fs::path &path, bool blackForeground, size_t repeat) {
	cv::resize(cv::imread(path.native(), 0), _frame, cv::Size(LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT), 0, 0, cv::INTER_NEAREST);
//	_frame = cv::imread(path.native(), 0);
	_repeat = repeat;

	if (not blackForeground)
		_frame = ~_frame;

	cv::threshold(_frame, _frame, 128, 255, CV_THRESH_BINARY);
}

ImageSource::~ImageSource() {
}

void ImageSource::close() {
	_repeat = 0;
}

float ImageSource::fps() const {
	return 30.0f;
}

size_t ImageSource::frames() const {
	return _repeat;
}

size_t ImageSource::framePixels() const {
	return _frame.total();
}

dim3 ImageSource::frameSize() const {
	return dim3(_frame.size[1], _frame.size[0], 1);
}

bool ImageSource::read(hosted<segment_image> &buffer) {
	cv::Mat finalImage(_frame.size[0], _frame.size[1], CV_8UC1, (uint8_t*) buffer->data);

	if (_repeat-- > 0) {
		_frame.copyTo(finalImage);
		return true;
	} else {
		return false;
	}
}

ImageListSource::ImageListSource(const fs::path &path, bool blackForeground, size_t repeat):
		image_index(0), repeat_index(0), repetitions(repeat), blackForeground(blackForeground) {

	std::ifstream list_file(path.string());
	std::string line;

	while (list_file.good()) {
		std::getline(list_file, line);
		fs::path image_path = line;
		if (line.size()) {
			image_paths.push_back(image_path);
		}
	}

	cv::Mat first_frame = cv::imread(image_paths[0].native(), 0);
	frame_size = dim3(first_frame.size[0], first_frame.size[1], 1);
}

ImageListSource::~ImageListSource() {
}

void ImageListSource::close() {
	image_index = image_paths.size();
}

float ImageListSource::fps() const {
	return 30.0f;
}

size_t ImageListSource::frames() const {
	return image_paths.size() * repetitions;
}

size_t ImageListSource::framePixels() const {
	return frame_size.x * frame_size.y;
}

dim3 ImageListSource::frameSize() const {
	return frame_size;
}

bool ImageListSource::read(hosted<segment_image> &buffer) {
	if (image_index >= image_paths.size()) {
		return false;
	} else {
		cv::Mat frame =	cv::imread(image_paths[image_index].native(), 0);

		if (not blackForeground)
			frame = ~frame;
		cv::threshold(frame, frame, 128, 255, CV_THRESH_BINARY);

		cv::Mat finalImage(frame.size[0], frame.size[1], CV_8UC1, (uint8_t*) buffer->data);
		frame.copyTo(finalImage);

		if (++repeat_index >= repetitions) {
			repeat_index = 0;
			image_index++;
		}

		return true;
	}
}

VideoSource::VideoSource(const fs::path &path, bool blackForeground) {
	source.open(path.native());
	_fps = source.get(CV_CAP_PROP_FPS);
	_frames = source.get(CV_CAP_PROP_FRAME_COUNT);
	_frameSize = cv::Size(LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT);
//	_frameSize = cv::Size(
//		source.get(CV_CAP_PROP_FRAME_WIDTH ),
//		source.get(CV_CAP_PROP_FRAME_HEIGHT)
//	);
	_blackForeground = blackForeground;
}

VideoSource::~VideoSource() {
	source.release();
}

void VideoSource::close() {
	source.release();
}

float VideoSource::fps() const {
	if (source.isOpened()) {
		return std::isnan(_fps) ? 30.0f : _fps;
	} else {
		throw std::logic_error("Cannot determine FPS of a closed video");
	}
}

size_t VideoSource::frames() const {
	if (source.isOpened()) {
		return _frames;
	} else {
		throw std::logic_error("Cannot determine frame count of a closed video");
	}
}

size_t VideoSource::framePixels() const {
	return _frameSize.width * _frameSize.height;
}

dim3 VideoSource::frameSize() const {
	if (source.isOpened()) {
		return dim3(_frameSize.width, _frameSize.height);
	} else {
		throw std::logic_error("Cannot determine frame size of a closed video");
	}
}

bool VideoSource::read(hosted<segment_image> &buffer) {
	cv::Mat frame;
	cv::Mat finalImage(_frameSize, CV_8UC1, buffer->data);
	bool result = source.isOpened() and source.read(frame);

	if (result) {
		cv::resize   (frame, frame, _frameSize, 0, 0, cv::INTER_NEAREST);
		cv::cvtColor (frame, frame, CV_BGR2GRAY);
		if (not _blackForeground)
			frame = ~frame;
		cv::threshold(frame, finalImage, 128, 255, CV_THRESH_BINARY);
	}

	return result;
}
