import matplotlib
matplotlib.rc('text', usetex=True)
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

import cuccl.profile.plotting as cpp
import numpy as np

from cuccl.profile.model import DataLoader
from scipy.integrate import trapz as integral
from scipy.signal import gaussian
from itertools import product


def plot_smooth_distribution(data, n=100, gauss_frac=0.05, axes=None, plot_opts={}):
    if axes is None:
        axes = plt.gca()

    min_t = np.min(data)
    max_t = np.max(data)

    bins = np.linspace(min_t, max_t, n + 1)
    step = (max_t - min_t) / n

    kernel_sigma = gauss_frac * (max_t - min_t)
    kernel = gaussian(min(n+1, int(np.round(6 * kernel_sigma / step))), kernel_sigma)

    y, x = np.histogram(data, bins=bins)
    x = 0.5 * (x[:-1] + x[1:])
    y = np.convolve(y, kernel, mode='same')
    y = y / integral(y, x)

    plots = axes.plot(x, y, **plot_opts)

    x_mean = np.mean(data)
    x_std = np.std(data)

    axes.axvline(x_mean, linestyle='--', color=plots[0].get_color())
    axes.axvline(x_mean - 3 * x_std, 0.05, 0.1, linestyle='--', color=plots[0].get_color())
    axes.axvline(x_mean + 3 * x_std, 0.05, 0.1, linestyle='--', color=plots[0].get_color())


def plot_named_queries(points, field, queries, axes=None, plot_opts={}, smooth_opts={}):
    for label, query in queries.items():
        data = points.query(query).eval(field)
        opts = {'label': label}
        opts.update(plot_opts)
        plot_smooth_distribution(data, axes=axes, plot_opts=opts, **smooth_opts)


algorithms = 'le tm'.split()
properties = 'no yes late'.split()
resolution = 8192

points = {
    pair: DataLoader(*pair).load_all_points()
    for pair in product(algorithms, properties)
}

queries={
    #     'COCO': 'param == "coco" and fg < 0.3',
    #     'Davis': 'param == "davis" and fg < 0.3',
    #     'Pascal': 'param == "pascal" and fg < 0.3',
    #     'SegTrack': 'param == "segtrack" and fg < 0.3',
    'All videos': 'type == "videos" and fg < 0.3',

    #         'Instancing': 'type == "instancing"',
    #         'Leveling': 'type == "leveling"',
    #         'RandomInstancing': 'type == "random_instancing"',
    #         'RandomMerging': 'type == "random_merging"',
    #         'Scaling': 'type == "scaling"',
    #         'Spiral': 'type == "spiral"',
    #         'Subdivision': 'type == "subdivision"',

    #         'Ridge patterns': 'type != "spiral" and type != "videos"',
    #         'Instancing patterns': 'type == "instancing" or type == "random_instancing"',
    #         'Random patterns': 'type == "random_instancing" or type == "random_merging"',
    #         'Non-random patterns': 'type != "random_instancing" and type != "random_merging" and type != "videos"',
    #         'Non-random Ridge patterns': 'type != "random_instancing" and type != "random_merging" and type != "spiral" and type != "videos"',
    #         'Patterns': 'type != "videos"',

    'Custom Patterns': ' or '.join(
        'type == %r' % pattern for pattern in [
            'instancing',
            'leveling',
            'random_instancing',
            'random_merging',
            'scaling',
            'spiral',
            'subdivision',
        ]
    ) #+ ' and cccount <= 64'
}

figure, axes = cpp.subplots(
    rows=len(properties), cols=len(algorithms),
    sharex=True, sharey='row',
    mode='single', aspect=0.8, scale=1.0
)

for i, props in enumerate(properties):
    for j, alg in enumerate(algorithms):
        axes[i, j].clear()
        plot_named_queries(
            points[alg, props].query('pixels == %d**2' % resolution),
            field='C / 1000',
            queries=queries,
            axes=axes[i, j],
            smooth_opts={
                'gauss_frac': 0.05
            }
        )
        axes[i, j].grid()
        axes[0, j].set_title(alg.upper())
        axes[-1, j].set_xlabel('Processing time (us)')

    if props == 'no':
        axes[i, 0].set_ylabel('Ordinary\nProbability density')
    elif props == 'yes':
        axes[i, 0].set_ylabel('Early\nProbability density')
    elif props == 'late':
        axes[i, 0].set_ylabel('Late\nProbability density')

axes[0, 0].legend()
axes[0, 0].set_xlim(0, 150)
