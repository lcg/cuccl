# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Python 2.7 wrapper for the CuCCL (Connected Components Labeling in CUDA) library.
"""

import os


class Version(object):
    """Represents the current library version.
    """

    @classmethod
    def get(cls):
        """Get default version object by parsing the version file.

        The version file `VERSION` is located at library's root installation directory.
        """
        version_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'VERSION')
        with open(version_path) as version_file:
            version_components = version_file.read().strip().split('.')
            build = int((len(version_components) == 4) and version_components[3])
            patch = int((len(version_components) >= 3) and version_components[2])
            minor = int((len(version_components) >= 2) and version_components[1])
            major = int((len(version_components) >= 1) and version_components[0])
        return cls(major, minor, patch, build)

    def __init__(self, major, minor, patch, build):
        self.major = major
        self.minor = minor
        self.patch = patch
        self.build = build

    def __iter__(self):
        return iter([self.major, self.minor, self.patch, self.build])

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, ','.join(str(part) for part in self))

    @property
    def long(self):
        """Long form of version string, i.e. a version string with four '.'-separated fields.
        """
        return '.'.join(str(part) for part in self)

    @property
    def short(self):
        """Short form of version string, i.e. a version string with up to four '.'-separated fields (zero omitted).
        """
        return '.'.join(str(part) for part in self if part > 0)


version = Version.get()
