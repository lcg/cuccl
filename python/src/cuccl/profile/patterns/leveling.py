# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Profiles the CuCCL library using Ridge Pattern leveling.
"""

import cuccl.profile
import cuccl.profile.patterns

import numpy as np
import sys


class LevelingElement(cuccl.profile.patterns.ImageProfileElement):
    @property
    def filepattern(self):
        return LevelingProfile.filepattern.format(
            resolution=self.resolution,
            algorithm=self.algorithm,
            properties=self.properties,
        )

    @property
    def paramrange(self):
        return np.arange(1, int(np.log(self.resolution) / np.log(2)))

    @property
    def shapefunc(self):
        return cuccl.profile.patterns.ridge


class LevelingProfile(cuccl.profile.patterns.ImageProfile):
    element_class = LevelingElement
    filepattern = '-'.join([
        'leveling',
        'resol:{resolution}',
        'alg:{algorithm}',
        'props:{properties}-s',
        'level:{{:d}}',
    ])
    name='ridge pattern leveling'
    parameter_name='ridge level'
    short_name = 'leveling'


if __name__ == '__main__':
    args = LevelingProfile.options_class.get()

    profile = LevelingProfile(args)

    profile.run()

    if args.just_profile:
        sys.exit(0)

    profile.gen_plots()
    profile.print_stats()
