# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Profiles the CuCCL library using Ridge Pattern instancing, with a variable merging probability.
"""

import cuccl.profile
import cuccl.profile.patterns
import cuccl.profile.patterns.instancing
import sys
import numpy as np


class MergingElement(cuccl.profile.patterns.RandomImageProfileElement):
    @property
    def filepattern(self):
        return MergingProfile.filepattern.format(
            resolution=self.resolution,
            algorithm=self.algorithm,
            properties=self.properties,
            level=self.level,
            subdivision=self.subdivision,
        )

    @property
    def level(self):
        return self.session.level

    @property
    def paramrange(self):
        return np.linspace(0, 1.0, 21)

    @property
    def shapefunc(self):
        def inner(resolution, merge_probability):
            return cuccl.profile.patterns.splitridge(
                size=resolution,
                subdivisions=self.subdivision,
                level=self.level,
                merge_prob=merge_probability
            )
        return inner

    @property
    def subdivision(self):
        # TODO: Refactor this code into a Ridge-pattern related class to avoid code repetition
        if self.session.subdivision is None:
            return int(np.log(self.resolution) / np.log(2)) - 8
        else:
            return self.session.subdivision


class MergingProfile(cuccl.profile.patterns.ImageProfile):
    element_class = MergingElement
    filepattern = '-'.join([
        'random_merging',
        'resol:{resolution}',
        'alg:{algorithm}',
        'props:{properties}-s',
        'level:{level}',
        'subdiv:{subdivision}',
        'prob:{{:.2f}}'
    ])
    name='random ridge pattern merging'
    options_class = cuccl.profile.patterns.instancing.InstancingProfilingOptions
    parameter_name = 'merging probability'
    short_name = 'random_merging'


if __name__ == '__main__':
    args = MergingProfile.options_class.get()

    profile = MergingProfile(args)

    profile.run()

    if args.just_profile:
        sys.exit(0)

    profile.gen_plots()
    profile.print_stats()

