# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Profiles the CuCCL library using Ridge Pattern instancing.
"""

import cuccl.profile
import cuccl.profile.patterns

import itertools as it
import numpy as np
import sys


class InstancingProfilingOptions(cuccl.profile.patterns.ImageProfilingOptions):
    @classmethod
    def parser(cls):
        parser = cuccl.profile.patterns.ImageProfilingOptions.parser()

        # Options for some pattern types
        parser.add_argument('-l', '--level', type=int, default=3)
        parser.add_argument('--subdivision', type=int, default=None)

        return parser


class InstancingElement(cuccl.profile.patterns.ImageProfileElement):
    @property
    def filepattern(self):
        return InstancingProfile.filepattern.format(
            resolution=self.resolution,
            algorithm=self.algorithm,
            properties=self.properties,
            level=self.level,
            subdivision=self.subdivision,
        )

    @property
    def level(self):
        return self.session.level

    @property
    def paramrange(self):
        step = 4 ** self.subdivision / 16 or 1
        #TODO: Lower limit should be 0 instead of step, but profiler is not generating missing inst:0 files.
        return np.arange(0, 4 ** self.subdivision + 1, step)

    @property
    def random_shapefunc(self):
        return False

    @property
    def shapefunc(self):
        return cuccl.profile.patterns.multiridge_n(self.level, self.subdivision)

    @property
    def subdivision(self):
        if self.session.subdivision is None:
            return int(np.log(self.resolution) / np.log(2)) - 8
        else:
            return self.session.subdivision


class InstancingProfile(cuccl.profile.patterns.ImageProfile):
    element_class = InstancingElement
    filepattern = '-'.join([
        'instancing',
        'resol:{resolution}',
        'alg:{algorithm}',
        'props:{properties}-s',
        'level:{level}',
        'subdiv:{subdivision}',
        'inst:{{}}'
    ])
    name = 'ridge pattern instancing'
    parameter_name = 'number of instances'
    options_class = InstancingProfilingOptions
    short_name = 'instancing'


if __name__ == '__main__':
    args = InstancingProfile.options_class.get()

    profile = InstancingProfile(args)

    profile.run()

    if args.just_profile:
        sys.exit(0)

    profile.gen_plots()
    profile.print_stats()
