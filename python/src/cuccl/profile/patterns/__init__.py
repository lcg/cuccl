# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.
import matplotlib; matplotlib.use('Agg')

import abc
import itertools as it
import numpy as np
import os
import PIL.Image
import Queue
import re
import subprocess as sp
import skimage.io
import skimage.measure
import skimage.transform
import tempfile
import threading
import tqdm

from matplotlib import pyplot as plt
from numpy import random as npr

import cuccl.profile
import cuccl.profile.plotting
from cuccl.profile import openCSV, ProfilingOptions, ProfileElement, Profile

PIL.Image.MAX_IMAGE_PIXELS = None


class ImageProfileElement(ProfileElement):
    @abc.abstractproperty
    def shapefunc(self):
        pass

    @property
    def stats(self):
        if self._stats is None:
            # Null statistics
            stats_shape = (len(self.paramrange), self.session.repeat)
            self._stats = np.zeros(stats_shape, dtype=self.stats_dtype)

            self._stats['homog'] = 1.0
            self._stats['pixels'] = self.resolution ** 2
            self._stats['param'] = np.vstack([self.paramrange for round_index in range(self.session.repeat)]).T
            self._stats['type'] = self.session.short_name

            param_proggress = tqdm.tqdm(self.paramrange)

            # Generate image, profile and compute totals for each parameter in the specified range
            for param_index, param in enumerate(param_proggress):
                param_proggress.set_description('Reading profile for param={}'.format(param))

                cache_path = self.cachePath(param)

                if os.path.exists(cache_path):
                    with open(cache_path, 'rb') as cache_file:
                        self._stats[param_index] = np.load(cache_file)
                        continue

                frame_index = 0
                frame_begin = 0
                last_kernel_name = ''
                last_kernel_start = 0

                line_proggress = tqdm.tqdm(openCSV(self.profPath(param)), desc='Reading lines'.format(cache_path))

                for kernel_index, kernel_launch in enumerate(line_proggress):
                    if kernel_launch['name'] is None:
                        continue

                    kernel_name = kernel_launch['name']
                    kernel_time = float(kernel_launch['duration'])
                    kernel_start = float(kernel_launch['start'])

                    # Start of frame: record time, and reset compute and memcpy accumulators
                    if re.search(r'frameBegin', kernel_name):
                        # TODO: Bug: frameBegin is issued by driver after first HtoD operation
                        frame_begin = last_kernel_start

                    # End of frame: calculate overhead and increment frame/parameter indices
                    if re.search(r'frameEnd', kernel_name):
                        compute_time = self._stats['C'][param_index, frame_index]
                        dtoh_time = self._stats['D'][param_index, frame_index]
                        htod_time = self._stats['U'][param_index, frame_index]
                        frame_time = kernel_start + kernel_time - frame_begin

                        self._stats['O'][param_index, frame_index] = frame_time - (compute_time + htod_time + dtoh_time)

                        frame_index += 1

                    # Account for memcpy DtoH operations
                    if re.search(r'memcpy DtoH', kernel_name):
                        self._stats['D'][param_index, frame_index] += kernel_time

                    # Account for memcpy HtoD operations
                    if re.search(r'memcpy HtoD', kernel_name):
                        self._stats['U'][param_index, frame_index] += kernel_time

                    # Account for memset operations
                    if re.search(r'memset', last_kernel_name):
                        memset_time = kernel_start - last_kernel_start
                        self._stats['M'][param_index, frame_index] += memset_time
                        self._stats['C'][param_index, frame_index] += memset_time

                    # Account for Analysis/PathCompression operation and increment iteration counter
                    if re.search(r'::(analysisKernel|UpdateBorders)', kernel_name):
                        if self._stats['i'][param_index, frame_index] == 0:
                            self._stats['f'][param_index, frame_index] += kernel_time
                        else:
                            self._stats['F'][param_index, frame_index] += kernel_time
                        self._stats['i'][param_index, frame_index] += 1
                        self._stats['C'][param_index, frame_index] += kernel_time

                    # Account for Prelabel/LocalMerge operation
                    if re.search('::prelabelKernel', kernel_name):
                        self._stats['I'][param_index, frame_index] += kernel_time
                        self._stats['C'][param_index, frame_index] += kernel_time

                    # Account for Scan/GlobalMerge operation
                    if re.search(r'::(scanKernel|MergeTiles)', kernel_name):
                        if self._stats['i'][param_index, frame_index] == 0:
                            self._stats['j'][param_index, frame_index] += kernel_time
                        else:
                            self._stats['J'][param_index, frame_index] += kernel_time
                        self._stats['C'][param_index, frame_index] += kernel_time

                    # Account for SequentialRelabeling operation
                    if re.search(r'thrust::|::(markRoots|relabel)Kernel', kernel_name):
                        self._stats['R'][param_index, frame_index] += kernel_time
                        self._stats['C'][param_index, frame_index] += kernel_time

                    # Account for PropertiesSum operation
                    if re.search(r'::transferPropsKernel', kernel_name):
                        self._stats['P'][param_index, frame_index] += kernel_time
                        self._stats['C'][param_index, frame_index] += kernel_time

                    last_kernel_name = kernel_name
                    last_kernel_start = kernel_start

                img_path = self.imgPath(param)
                image = skimage.io.imread(img_path, True)
                image_labels = skimage.measure.label(image, connectivity=2)
                image_properties = skimage.measure.regionprops(image_labels)
                sizes = [region.area for region in image_properties]
                self._stats['fg'][param_index, :] = 1.0 * np.sum(sizes) / self.resolution ** 2
                self._stats['cccount'][param_index, :] = len(sizes)
                if sizes:
                    self._stats['avg_ccsize'][param_index, :] = np.mean(sizes)
                    self._stats['max_ccsize'][param_index, :] = np.max(sizes)
                else:
                    self._stats['avg_ccsize'][param_index, :] = 0
                    self._stats['max_ccsize'][param_index, :] = 0

                with open(cache_path, 'wb') as cache_file:
                    np.save(cache_file, self._stats[param_index])

        return self._stats

    def imgPath(self, param):
        return '{}/{}.png'.format(self.session.IMGDIR, self.filepattern.format(param))

    def run(self):
        """Lower-level profiling function, accepting various synthetic pattern specifications.

        This routine profiles a family of synthetic test patterns parametrized by a single variable. It requires the user
        to specify a univariate function that spans the pattern family, and a range of parameters on which the function is
        evaluated to generate specific instances of the family. Profiling sub-products are stored in pre-defined directories
        using a file name pattern also supplied by the user. This routine may recompile the CuCCL driver if its resolution
        does not match the required profiling resolution. Progress is reported by writing to standard output.
        """
        self.session.checkDirectories()

        # Run profiler and generate csv output
        if self.session.withmemory:
            driver_mode = 'prof'
        else:
            driver_mode = 'prof-no-mem'

        param_proggress = tqdm.tqdm(self.paramrange)

        # Generate image, profile and compute totals for each parameter in the specified range
        for i, param in enumerate(param_proggress):
            param_proggress.set_description('Profiling param={}'.format(param))

            image_path = self.imgPath(param)
            profile_path = self.profPath(param)

            if os.path.exists(profile_path) and not self.session.overwrite:
                continue

            self.session.checkBuildResolution(self.resolution)

            # Generate profiled image
            image = self.shapefunc(self.resolution, param)
            skimage.io.imsave(image_path, image)

            driver_command = ' '.join([
                '{binary_dir}/cuccl-driver',
                '-i {img_path}',
                '-a {algorithm}',
                '-p {properties}',
                '-s',
                '-f {repeat}',
                '-m {driver_mode}',
                '-q',
            ]).format(
                binary_dir=self.session.BINDIR,
                img_path=image_path,
                algorithm=self.algorithm,
                properties=self.properties,
                repeat=self.session.repeat,
                driver_mode=driver_mode,
            )

            profiler_command = ' '.join([
                '{profiler_bin}',
                '--csv',
                '-u us',
                '--print-gpu-trace',
                '--log-file {log_file}',
                '{driver_command}'
            ]).format(
                profiler_bin=self.session.PROFILER,
                log_file=profile_path,
                driver_command=driver_command,
            )

            sp.call(profiler_command.split(), stdout=self.session.stdout, stderr=self.session.stderr)

            self.session.stdout.write('\tProfiling: %5.1f%% completed\r' % (100.0 * (i + 1) / (len(self.paramrange))))
            self.session.stdout.flush()


class RandomImageProfileElement(ImageProfileElement):
    @property
    def stats(self):
        if self._stats is None:
            cache_path = self.cachePath()
            if os.path.exists(cache_path):
                with open(cache_path, 'rb') as cache_file:
                    self._stats = np.load(cache_file)
            else:
                # Null statistics
                stats_shape = (len(self.paramrange), self.session.repeat)
                self._stats = np.zeros(stats_shape, dtype=self.stats_dtype)

                self._stats['homog'] = 1.0
                self._stats['pixels'] = self.resolution ** 2
                self._stats['param'] = np.vstack([self.paramrange for round_index in range(self.session.repeat)]).T
                self._stats['type'] = self.session.short_name

                for param_index, param in enumerate(self.paramrange):
                    for round_index in range(self.session.repeat):
                        img_path = self.imgPath(param, round_index)
                        image = skimage.io.imread(img_path, True)
                        image_labels = skimage.measure.label(image, connectivity=2)
                        image_properties = skimage.measure.regionprops(image_labels)
                        sizes = [region.area for region in image_properties]
                        self._stats['fg'][param_index, round_index] = np.sum(sizes) / self.resolution ** 2.0
                        self._stats['cccount'][param_index, round_index] = len(sizes)
                        if sizes:
                            self._stats['avg_ccsize'][param_index, round_index] = np.mean(sizes)
                            self._stats['max_ccsize'][param_index, round_index] = np.max(sizes)
                        else:
                            self._stats['avg_ccsize'][param_index, round_index] = 0
                            self._stats['max_ccsize'][param_index, round_index] = 0

                param_index = 0
                round_index = 0
                frame_begin = 0
                last_kernel_name = ''
                last_kernel_start = 0

                for kernel_index, kernel_launch in enumerate(openCSV(self.profPath())):
                    if kernel_launch['name'] is None:
                        continue

                    kernel_name = kernel_launch['name']
                    kernel_time = float(kernel_launch['duration'])
                    kernel_start = float(kernel_launch['start'])

                    # Start of frame: record time, and reset compute and memcpy accumulators
                    if re.search(r'frameBegin', kernel_name):
                        # TODO: Bug: frameBegin is issued by driver after first HtoD operation
                        frame_begin = last_kernel_start

                    # End of frame: calculate overhead and increment frame/parameter indices
                    if re.search(r'frameEnd', kernel_name):
                        compute_time = self._stats['C'][param_index, round_index]
                        dtoh_time = self._stats['D'][param_index, round_index]
                        htod_time = self._stats['U'][param_index, round_index]
                        frame_time = kernel_start + kernel_time - frame_begin

                        self._stats['O'][param_index, round_index] = frame_time - (compute_time + htod_time + dtoh_time)

                        if round_index + 1 == self.session.repeat:
                            param_index += 1
                            round_index = 0
                        else:
                            round_index += 1

                    # Account for memcpy DtoH operations
                    if re.search(r'memcpy DtoH', kernel_name):
                        self._stats['D'][param_index, round_index] += kernel_time

                    # Account for memcpy HtoD operations
                    if re.search(r'memcpy HtoD', kernel_name):
                        self._stats['U'][param_index, round_index] += kernel_time

                    # Account for memset operations
                    if re.search(r'memset', last_kernel_name):
                        memset_time = kernel_start - last_kernel_start
                        self._stats['M'][param_index, round_index] += memset_time
                        self._stats['C'][param_index, round_index] += memset_time

                    # Account for Analysis/PathCompression operation and increment iteration counter
                    if re.search(r'::(analysisKernel|UpdateBorders)', kernel_name):
                        if self._stats['i'][param_index, round_index] == 0:
                            self._stats['f'][param_index, round_index] += kernel_time
                        else:
                            self._stats['F'][param_index, round_index] += kernel_time
                        self._stats['i'][param_index, round_index] += 1
                        self._stats['C'][param_index, round_index] += kernel_time

                    # Account for Prelabel/LocalMerge operation
                    if re.search('::prelabelKernel', kernel_name):
                        self._stats['I'][param_index, round_index] += kernel_time
                        self._stats['C'][param_index, round_index] += kernel_time

                    # Account for Scan/GlobalMerge operation
                    if re.search(r'::(scanKernel|MergeTiles)', kernel_name):
                        if self._stats['i'][param_index, round_index] == 0:
                            self._stats['j'][param_index, round_index] += kernel_time
                        else:
                            self._stats['J'][param_index, round_index] += kernel_time
                        self._stats['C'][param_index, round_index] += kernel_time

                    # Account for SequentialRelabeling operation
                    if re.search(r'thrust::|::(markRoots|relabel)Kernel', kernel_name):
                        self._stats['R'][param_index, round_index] += kernel_time
                        self._stats['C'][param_index, round_index] += kernel_time

                    # Account for PropertiesSum operation
                    if re.search(r'::transferPropsKernel', kernel_name):
                        self._stats['P'][param_index, round_index] += kernel_time
                        self._stats['C'][param_index, round_index] += kernel_time

                    last_kernel_name = kernel_name
                    last_kernel_start = kernel_start

                with open(cache_path, 'wb') as cache_file:
                    np.save(cache_file, self._stats)

        return self._stats

    def cachePath(self):
        return '{}/{}.npy'.format(self.session.CACHEDIR, '-'.join(self.filepattern.format(1).split('-')[:-1]))

    def dumpPath(self):
        return '{}/{}-dump.csv'.format(self.session.DUMPDIR, '-'.join(self.filepattern.format(1).split('-')[:-1]))

    def imgPath(self, param, round):
        return '{}/{}-round:{}.png'.format(self.session.IMGDIR, self.filepattern.format(param), round)

    def profPath(self):
        return '{}/{}.csv'.format(self.session.PROFDIR, '-'.join(self.filepattern.format(1).split('-')[:-1]))

    def run(self):
        """Lower-level profiling function, accepting various synthetic pattern specifications.

        This routine profiles a family of synthetic test patterns parametrized by a single variable. It requires the user
        to specify a univariate function that spans the pattern family, and a range of parameters on which the function is
        evaluated to generate specific instances of the family. Profiling sub-products are stored in pre-defined directories
        using a file name pattern also supplied by the user. This routine may recompile the CuCCL driver if its resolution
        does not match the required profiling resolution. Progress is reported by writing to standard output.
        """
        self.session.checkDirectories()

        # Run profiler and generate csv output
        if self.session.withmemory:
            driver_mode = 'prof'
        else:
            driver_mode = 'prof-no-mem'

        # Generate images and image list file
        self.setup()
        image_path_list = []

        for i, param in enumerate(self.paramrange):
            if not os.path.exists(profile_path) or self.session.overwrite:
                profile_path = self.profPath()
                for k in range(self.session.repeat):
                    image_path_list.append(self.imgPath(param, k))

        if image_path_list:
            image_list_file = tempfile.NamedTemporaryFile(mode='w', prefix='cuccl', suffix='.txt', delete=True)
            image_list_file.write('\n'.join(image_path_list))
            image_list_file.flush()

            self.session.checkBuildResolution(self.resolution)

            driver_command = ' '.join([
                '{binary_dir}/cuccl-driver',
                '-i {img_path} -f 1',
                '-a {algorithm}',
                '-p {properties}',
                '-s',
                '-m {driver_mode}',
            ]).format(
                binary_dir=self.session.BINDIR,
                img_path=image_list_file.name,
                algorithm=self.algorithm,
                properties=self.properties,
                driver_mode=driver_mode,
            )

            profiler_command = ' '.join([
                '{profiler_bin}',
                '--csv',
                '-u us',
                '--print-gpu-trace',
                '--log-file {log_file}',
                '{driver_command}'
            ]).format(
                profiler_bin=self.session.PROFILER,
                log_file=profile_path,
                driver_command=driver_command,
            )

            # self.session.stdout.write('\tProfiling:\r')
            sp.call(profiler_command.split(), stdout=self.session.stdout, stderr=self.session.stderr)
            # self.session.stdout.write('\tProfiling: 100% completed\r')
            # self.session.stdout.flush()

    def setup(self):
        """Prepare profile scenario by generating random patterns.
        """
        self.session.checkDirectories()

        num_threads = 7

        work_proggress = tqdm.tqdm(
            total=len(self.paramrange) * self.session.repeat,
            desc='Generating images; '
        )
        work_mutex = threading.Semaphore()
        work_queue = Queue.Queue()

        for param, k in it.product(self.paramrange, range(self.session.repeat)):
            work_queue.put((param, k))

        def worker():
            while True:
                try:
                    param, k = work_queue.get()
                    image_path = self.imgPath(param, k)

                    if not os.path.exists(image_path):
                        # Generate profiled image and add to the list of files
                        image = self.shapefunc(self.resolution, param)
                        skimage.io.imsave(image_path, image)

                    with work_mutex:
                        work_proggress.update(1)

                except Queue.Empty:
                    return

        threads = []
        for i in range(num_threads):
            threads.append(threading.Thread(target=worker))
        for t in threads:
            t.start()
        for t in threads:
            t.join()

        work_proggress.close()


class ImageProfilingOptions(ProfilingOptions):
    @classmethod
    def parser(cls):
        parser = ProfilingOptions.parser()

        # Options for patterns
        parser.add_argument('-n', '--repeat', type=int, default=100, help='Number of frame repetitions')

        # Options for plotting-related sub-commands (patterns-only)
        parser.add_argument('--layout', type=str, default='1', choices=['1', '2', '3'], help='Type of plot layout')
        parser.add_argument('--overhead-threshold', type=float, default=0.0,
                            help='Group kernel contributions for which the mean kernel contribution is smaller than this percentage of the y-limit into the overhead category.')

        return parser


class ImageProfile(Profile):
    options_class = ImageProfilingOptions

    def gen_plots(self, xfactor=1):
        if self.layout == '1':
            self._gen_plots_1(xfactor)
        elif self.layout == '2':
            self._gen_plots_2(xfactor)
        elif self.layout == '3':
            self._gen_plots_3(xfactor)

    def _gen_plots_1(self, xfactor=1):
        """Generate plots.
        """
        for res in self.resolutions:
            cols = len(self.algorithms) * len(self.properties)

            figure, axes = cuccl.profile.plotting.subplots(1, cols, mode='double', scale=self.options.figure_scale,
                                                           aspect=self.options.figure_aspect, sharex=True, sharey=True)
            axes = axes[0]

            for j, (props, alg) in enumerate(it.product(self.properties, self.algorithms)):
                key = (res, alg, props)

                xvalues = self[key].paramrange * xfactor

                y_0, y_1 = 0, 0

                for phase in 'RIjJfFMPO':
                    delta_y = np.mean(self[key].aggr(phase), axis=1) / 1000
                    y_0, y_1 = y_1, y_1 + delta_y

                    axes[j].fill_between(
                        xvalues, y_1, y_0,
                        color=self.options.colors[phase],
                        linewidth=0
                    )

                axes[j].plot(
                    xvalues,
                    np.mean(self[res, alg, 'late'].aggr('RIjJfF'), axis=1) / 1000,
                    'k--'
                )

                axes[j].set_ylim(*self.options.ylim[res])
                axes[j].grid()

                if self.options.show_title:
                    if props == 'yes':
                        axes[j].set_title('Early %s' % alg.upper(), fontsize=10)
                    elif props == 'no':
                        axes[j].set_title('%s' % alg.upper(), fontsize=10)
                    elif props == 'late':
                        axes[j].set_title('Late %s' % alg.upper(), fontsize=10)

                if self.options.show_xlabels:
                    axes[j].set_xlabel(self.parameter_name)

            if self.options.show_ylabels:
                axes[0].set_ylabel('Performance (ms)')

            cuccl.profile.plotting.savefig(
                figure,
                '{}/{}-s-resol:{}'.format(
                    self.PLOTDIR,
                    self.short_name,
                    res
                )
            )
            plt.close(figure)

    def _gen_plots_2(self, xfactor=1):
        """Generate plots (alternative layout 1).
        """
        for res in self.resolutions:
            cols = len(self.algorithms)
            rows = len(self.properties)

            figure, axes = cuccl.profile.plotting.subplots(rows, cols, mode='single', scale=self.options.figure_scale,
                                                           aspect=self.options.figure_aspect,
                                                           sharex=True, sharey=True)
            for i, props in enumerate(self.properties):
                for j, alg in enumerate(self.algorithms):
                    key = (res, alg, props)
                    noprops_key = (res, alg, 'late')

                    xvalues = self[key].paramrange * xfactor

                    y_0, y_1 = 0, 0

                    for phase in 'RIjJfFM':
                        delta_y = np.mean(self[key].aggr(phase), axis=1) / 1000
                        y_0, y_1 = y_1, y_1 + delta_y

                        axes[i][j].fill_between(
                            xvalues, y_1, y_0,
                            color=self.options.colors[phase],
                            linewidth=0)

                    axes[i][j].plot(
                        xvalues,
                        np.mean(self[noprops_key].aggr('RIjJfF'), axis=1) / 1000,
                        'k--'
                    )

                    if props == 'late':
                        axes[i][j].fill_between(
                            xvalues,
                            np.mean(self[key].aggr('RIjJfFMP'), axis=1) / 1000,
                            np.mean(self[key].aggr('RIjJfFM'), axis=1) / 1000,
                            color=self.options.colors['P'],
                        )
                        axes[i][j].fill_between(
                            xvalues,
                            np.mean(self[key].aggr('RIjJfFMPO'), axis=1) / 1000,
                            np.mean(self[key].aggr('RIjJfFMP'), axis=1) / 1000,
                            color=self.options.colors['O'],
                        )
                    else:
                        axes[i][j].fill_between(
                            xvalues,
                            np.mean(self[key].aggr('RIjJfFMO'), axis=1) / 1000,
                            np.mean(self[key].aggr('RIjJfFM'), axis=1) / 1000,
                            color=self.options.colors['O'],
                        )

                    axes[i][j].set_ylim(*self.options.ylim[res])
                    axes[i][j].grid()

                    if self.options.show_title:
                        if alg == 'le':
                            axes[0][j].set_title('LE', fontsize=10)
                        elif alg == 'tm':
                            axes[0][j].set_title('TM', fontsize=10)

                    if self.options.show_xlabels:
                        axes[-1][j].set_xlabel(self.parameter_name)

                if self.options.show_ylabels:
                    if props == 'late':
                        axes[i][0].set_ylabel('Late properties\nPerformance (ms)')
                    elif props == 'yes':
                        axes[i][0].set_ylabel('Early properties\nPerformance (ms)')

            cuccl.profile.plotting.savefig(
                figure,
                '{}/{}-s-resol:{}'.format(
                    self.PLOTDIR,
                    self.short_name,
                    res
                )
            )
            plt.close(figure)

    def _gen_plots_3(self, xfactor=1):
        """Generate plots (alternative layout 2).
        """
        for res in self.resolutions:
            cols = len(self.algorithms)
            rows = len(self.properties) + 1

            figure, axes = cuccl.profile.plotting.subplots(rows, cols, mode='single', scale=self.options.figure_scale,
                                                           aspect=self.options.figure_aspect, sharex=True, sharey=True)
            for i, props in enumerate(['noprops'] + self.properties):
                for j, alg in enumerate(self.algorithms):
                    if props == 'noprops':
                        key = (res, alg, 'late')
                    else:
                        key = (res, alg, props)

                    noprops_key = (res, alg, 'late')
                    xvalues = self[key].paramrange * xfactor

                    if props == 'noprops':
                        y_0, y_1 = 0, 0
                        overhead = 0

                        for phase in 'RIjJfFO':
                            delta_y = np.mean(self[key].aggr(phase), axis=1) / 1000
                            y_0, y_1 = y_1, y_1 + delta_y
                            if phase == 'O':
                                y_1 = y_1 + overhead
                                axes[i][j].fill_between(xvalues, y_1, y_0, color=self.options.colors[phase],
                                                        linewidth=0)
                            elif np.mean(y_1 - y_0) > self.options.get_overhead_threshold(res):
                                axes[i][j].fill_between(xvalues, y_1, y_0, color=self.options.colors[phase],
                                                        linewidth=0)
                            else:
                                overhead = overhead + y_1 - y_0
                                y_1 = y_0

                                # if 'yes' in self.properties:
                                #     y_early = np.mean(cuccl.self.image.aggr(self.stats[res, alg, 'yes'], 'RIjJfFMPO'), axis=1) / 1000
                                #     axes[i][j].plot(xvalues, y_early, color=cuccl.self.colors['early'], linestyle='--', linewidth=0.5)
                                #
                                # if 'late' in self.properties:
                                #     y_late = np.mean(cuccl.self.image.aggr(self.stats[res, alg, 'late'], 'RIjJfFMPO'), axis=1) / 1000
                                #     axes[i][j].plot(xvalues, y_late, color=cuccl.self.colors['late'], linestyle='--', linewidth=0.5)
                    else:
                        y_0, y_1 = 0, 0
                        overhead = 0

                        for phase in 'RIjJfFO':
                            y_a = np.mean(self[key].aggr(phase), axis=1)
                            y_b = np.mean(self[noprops_key].aggr(phase), axis=1)

                            delta_y = np.clip(y_a - y_b, 0, float('inf')) / 1000

                            y_0, y_1 = y_1, y_1 + delta_y
                            if phase == 'O':
                                y_1 = y_1 + overhead
                                axes[i][j].fill_between(xvalues, y_1, y_0, color=self.options.colors[phase], linewidth=0)
                            if np.mean(y_1 - y_0) > self.options.get_overhead_threshold(res):
                                axes[i][j].fill_between(xvalues, y_1, y_0, color=self.options.colors[phase], linewidth=0)
                            else:
                                overhead = overhead + y_1 - y_0
                                y_1 = y_0

                        for phase in 'MP':
                            delta_y = np.mean(self[key].aggr(phase), axis=1) / 1000
                            y_0, y_1 = y_1, y_1 + delta_y
                            axes[i][j].fill_between(xvalues, y_1, y_0, color=self.options.colors[phase], linewidth=0)

                    axes[i][j].set_ylim(*self.options.ylim[res])
                    axes[i][j].grid()

                    if self.options.show_title:
                        if alg == 'le':
                            axes[0][j].set_title('LE', fontsize=10)
                        elif alg == 'tm':
                            axes[0][j].set_title('TM', fontsize=10)

                    if self.options.show_xlabels:
                        axes[-1][j].set_xlabel(self.parameter_name)

                if self.options.show_ylabels:
                    if props == 'noprops':
                        axes[i][0].set_ylabel('No properties\nPerformance (ms)')
                    elif props == 'late':
                        axes[i][0].set_ylabel('Late properties\nOverhead (ms)')
                    elif props == 'yes':
                        axes[i][0].set_ylabel('Early properties\nOverhead (ms)')

            cuccl.profile.plotting.savefig(
                figure,
                '{}/{}-s-resol:{}'.format(
                    self.PLOTDIR,
                    self.short_name,
                    res
                )
            )
            plt.close(figure)

    def print_stats(self):
        """Print algorithms performance summary.
        """
        linefmt = '    %25r | %4s : %4s | %4.1f | %4.1f | %4.1f'
        headfmt = '    %25s | %4s : %4s | %4s | %4s | %4s'
        header = headfmt % ('Parameters', 'p1', 'p0', 'min', 'max', 'avg')

        print '  Resolution ratios:'
        print header
        print '    ' + '-' * (len(header) - 4)
        for a, p in it.product(self.algorithms, self.properties):
            r0 = self.resolutions[0]
            for r1 in self.resolutions[1:]:
                y0 = self[r0, a, p].aggr('OIjJFMP')
                y1 = self[r1, a, p].aggr('OIjJFMP')
                n = min(len(y1), len(y0))
                y0 = y0[:n]
                y1 = y1[:n]
                print linefmt % ((a, p), r1, r0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))

        print '  Algorithm ratios:'
        print header
        print '    ' + '-' * (len(header) - 4)
        for r, p in it.product(self.resolutions, self.properties):
            a0 = self.algorithms[0]
            for a1 in self.algorithms[1:]:
                y0 = self[r, a0, p].aggr('OIjJFMP')
                y1 = self[r, a1, p].aggr('OIjJFMP')
                n = min(len(y1), len(y0))
                y0 = y0[:n]
                y1 = y1[:n]
                print linefmt % ((r, p), a1, a0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))

        print '  Propsmodes ratios:'
        print header
        print '    ' + '-' * (len(header) - 4)
        for combo in it.product(self.resolutions, self.algorithms):
            p0 = self.properties[0]
            for p1 in self.properties[1:]:
                y0 = self[r, a, p0].aggr('OIjJFMP')
                y1 = self[r, a, p1].aggr('OIjJFMP')
                n = min(len(y1), len(y0))
                y0 = y0[:n]
                y1 = y1[:n]
                print linefmt % (combo, p1, p0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))


def spiral(resol, segments, direction='out'):
    """Creates a spiral pattern, given the image resolution and the number of segments.

    Arguments
    ---------
    resol : int
        The output will be a square image with width and height equal to this parameter.

    segments : int
        The number of spiral segments. It may also be thought as the number of turns minus one.

    direction : `'in'` or `'out'`
        The spiral direction. If `'in'`, segments are laid out from the image center towards the borders. If `'out'`,
        they are laid out in the opposite direction.

    Returns
    -------
    image : :class:`numpy.array` of :attr:`numpy.uint8`
    """

    def draw(image, order, fg):
        """Spiral of order segments in a square image of resol x resol pixels.
        """
        patch = image
        for i in range(order):
            if i % 4 == 0:
                patch[0, :] = fg
                if i > 0:
                    patch = patch[:, 2:]
            elif i % 4 == 1:
                patch[:, -1] = fg
                patch = patch[2:, :]
            elif i % 4 == 2:
                patch[-1, :] = fg
                patch = patch[:, :-2]
            else:
                patch[:, 0] = fg
                patch = patch[:-2, :]

    image = np.zeros((resol, resol), dtype=np.uint8)

    if direction == 'in':
        draw(image, segments, 255)
    elif direction == 'out':
        draw(image, resol - 1, 255)
        draw(image, resol - 1 - segments, 0)

    return image


def ridge(size, levels):
    """Creates a ridge pattern, given resolution and crest level.

    Arguments
    ---------
    size : int
        Corresponds to the resolution of the square output image, and is also used to determine the size of the spiral
        pattern. The horizontal width of the pattern instance will be :code:`size/2+1` pixels.

    levels : int
        Ridge crest level.

    Returns
    -------
    image : :class:`numpy.ndarray` of :class:`numpy.uint8`
    """
    def crest(levels):
        size = (levels, 2**(levels-1)+1)
        image = np.zeros(size, np.uint8)
        periods = [2**l for l in range(levels)]
        for i, p in enumerate(reversed(periods)):
            image[i:, ::p] = 255
        return image

    image = np.zeros((size, size))
    top = crest(levels)
    th, tw = top.shape
    if tw >= size / 2 + 1:
        raise ValueError('Cannot make a level %d crisp with only %dx%d pixels' % (levels, size, size))
    th2 = 2 * th
    tw2 = size / 2 + 1
    image[:th2, :tw2] = skimage.transform.resize(top, output_shape=(th2, tw2), order=0, preserve_range=True).astype(np.uint8)
    for col, row in enumerate(range(th2, th2 + tw2)):
        image[row, col:(col+tw2)] = 255
    return image


def scaleridge(levels):
    """A factory producing functions that create scaled ridge patterns.

    Arguments
    ---------
    levels : int
        Ridge pattern level.

    Returns
    -------
    function(resol, scale)
        A function that takes resolution and scale (a float between 0 and 1) as parameters and produces a ridge pattern
        that is scaled by the specified factor.
    """
    def function(resol, scale):
        image = np.zeros((resol, resol), np.uint8)
        pattern = ridge(resol, levels)
        y, x = np.nonzero(pattern)
        h, w = np.max(y), np.max(x)
        pattern = pattern[:h, :w]
        pattern = skimage.transform.resize(pattern, output_shape=(int(h * scale), int(w * scale)), order=0, preserve_range=True).astype(np.uint8)
        h, w = pattern.shape
        image[:h, :w] = pattern
        return image
    return function


def multiridge_n(levels, divisions):
    """Returns a function f(r, n) that produces a ridge shape with
    ``ridge(r, levels)`` and divides it in 4**d smaller copies and keeps only
    the first k copies.
    """
    def function(resol, figures):
        return splitridge(size=resol, level=levels, subdivisions=divisions, instances=figures)
    return function


def multiridge(levels):
    """Returns a function f(r, d) that produces a ridge shape with
    ``ridge(r, levels)`` and divides it in 4**d smaller copies that sum up the
    same number of pixels of the original shape.
    """
    def function(resol, divisions):
        return splitridge(size=resol, level=levels, subdivisions=divisions)
    return function


def splitridge(size, level=1, subdivisions=0, instances=None, instance_prob=1.0, merge_prob=0.0):
    """Creates ridge patterns of requested size and level, with optional subdivision, random instancing and connection.

    The `subdivisions` and `instances` parameters control instance subdivision and replication. When `instance_prob` is
    `1.0` (default), the base pattern is subdivided `subdivision` times, generating :code:`4**subdivision` potential
    instances, but only the first `instances` are kept.

    The `instance_prob` and `merge_prob` parameters are used for probabilistic pattern generation. The first controls
    the likelihood of a single pattern instance being kept, whereas the second controls the likelihood of two adjacent
    pattern instances that were kept being merged into a single connected component, by lighting the pixel that
    separates them. This is only valid for horizontally and vertically adjacent pattern instances, since the spacing
    between adjacent pattern instances is one pixel wide, thus making it impossible to merge two diagonally adjacent
    pattern instances without also merging the adjacent patterns in the opposite diagonal. Note that these probabilities
    are applied to the potential pattern instances generated by the `subdivisions` and `instances` parameters.

    Parameters
    ----------
    level : int
        Ridge pattern level. Beware that the maximum level accepted depends on the image size, as required by the
        :func:`cuccl.profile.shapes.ridge` function.

    size : int
        Square size of output image.

    subdivisions : int
        Number of pattern subdivisions. The maximum number of generated instances will be :code:`4**subdivisions`.

    instances : int, optional
        Maximum number of instances to draw. This should be a number between 0 and :code:`4**subdivisions` (inclusive).
        If `None` is passed (default), :code:`4**subdivisions` will be assumed.

    Examples
    --------
    Generate a single level 5 pattern in a 1024x1024 image:
        >>> splitridge(size=1024, level=5)

    Generate a level 3 pattern (in a 1024x1024 image) and subdivide it twice:
        >>> splitridge(size=1024, level=3, subdivisions=2)

    The same as before, but only keep the 7 last instances
        >>>  splitridge(size=1024, level=3, subdivisions=2, instances=7)

    Generate a level 3 pattern (in a 1024x1024 image), subdivide it twice and keep each instance with a probability of
    50%:
        >>> splitridge(size=1024, level=3, subdivisions=2, instance_prob=0.5)
    """
    # Argument handling
    if instances is None:
        instances = 4 ** subdivisions

    if not (0 <= instances <= 4**subdivisions):
        raise ValueError('0 <= instances <= 4**subdivisions')

    # Output array
    image = np.zeros((size, size), np.uint8)

    instance_matrix = np.zeros((2**subdivisions, 2**subdivisions), dtype=np.bool)

    # Instance generation
    instance_size = size / 2 ** subdivisions
    instance_pixels = ridge(instance_size - 2, level)
    instance_height, instance_width = instance_pixels.shape
    instance_mask = np.nonzero(instance_pixels)

    # Instance crest isolation
    instance_crest_width = (instance_size - 2) / 2 + 1
    instance_crest_height = 2 * level + instance_crest_width

    # Calculation of instance corner positions
    instance_corners_x = range(0, 2 ** subdivisions * (instance_crest_width + 2), (instance_crest_width + 2))
    instance_corners_y = range(0, 2 ** subdivisions * (instance_crest_height + 2), (instance_crest_height + 2))

    # Instance replication
    instance_corners = it.product(
        enumerate(instance_corners_y),
        enumerate(instance_corners_x)
    )
    for k, ((i, corner_y), (j, corner_x)) in enumerate(instance_corners):
        # Maximum number of instances
        if k == instances:
            break

        if npr.random() <= instance_prob:
            image_patch = image[corner_y:(corner_y + instance_height), corner_x:(corner_x + instance_width)]
            image_patch[instance_mask] = instance_pixels[instance_mask]
            instance_matrix[i, j] = True

    # Instance merging
    instance_corners = it.product(
        enumerate(instance_corners_y),
        enumerate(instance_corners_x)
    )
    for (i, corner_y), (j, corner_x) in instance_corners:
        for a, b in [(i-1, j-1), (i, j-1)]:
            if a < 0 or b < 0:
                continue
            elif npr.random() <= merge_prob:
                if a == i:
                    image[corner_y, corner_x-2:corner_x] = 255
                else:
                    image[corner_y-2:corner_y, corner_x] = 255

    return image
