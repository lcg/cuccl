# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Profiles the CuCCL library using Ridge Pattern scaling.
"""

import cuccl.profile
import cuccl.profile.patterns
import numpy as np
import sys


class ScalingProfilingOptions(cuccl.profile.patterns.ImageProfilingOptions):
    @classmethod
    def parser(cls):
        parser = cuccl.profile.patterns.ImageProfilingOptions.parser()

        # Options for some pattern types
        parser.add_argument('-l', '--level', type=int, default=3)

        return parser


class ScalingElement(cuccl.profile.patterns.ImageProfileElement):
    @property
    def filepattern(self):
        return ScalingProfile.filepattern.format(
            resolution=self.resolution,
            algorithm=self.algorithm,
            properties=self.properties,
            level=self.level,
        )

    @property
    def level(self):
        return self.session.level

    @property
    def paramrange(self):
        return np.linspace(0, 1.0, 17)[1:]

    @property
    def shapefunc(self):
        return cuccl.profile.patterns.scaleridge(self.level)


class ScalingProfile(cuccl.profile.patterns.ImageProfile):
    element_class = ScalingElement
    filepattern = '-'.join([
        'scaling',
        'resol:{resolution}',
        'alg:{algorithm}',
        'props:{properties}-s',
        'level:{level}',
        'scale:{{:.2f}}',
    ])
    name = 'ridge pattern scaling'
    options_class = ScalingProfilingOptions
    parameter_name = 'foreground occupancy'
    short_name = 'scaling'


if __name__ == '__main__':
    args = ScalingProfile.options_class.get()

    profile = ScalingProfile(args)

    profile.run()

    if args.just_profile:
        sys.exit(0)

    profile.gen_plots(xfactor=0.25)
    profile.print_stats()