# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Profiles the CuCCL library using Spiral Patterns with a increasing lengths.
"""

import cuccl.profile
import cuccl.profile.patterns
import numpy as np
import sys


def spiral_out_fg(res, steps=16):
    return spiral_out_range(res, steps) * (spiral_out_range(res, steps) - 1) / (2.0 * res**2)


def spiral_out_range(res, steps=16):
    return np.linspace(0, res, steps + 1).astype(np.int)


class SpiralElement(cuccl.profile.patterns.ImageProfileElement):
    def __init__(self, *args, **kwargs):
        cuccl.profile.patterns.ImageProfileElement.__init__(self, *args, **kwargs)
        self._fg_to_segments = dict(zip(spiral_out_fg(self.resolution), spiral_out_range(self.resolution)))

    @property
    def filepattern(self):
        return SpiralProfile.filepattern.format(
            resolution=self.resolution,
            algorithm=self.algorithm,
            properties=self.properties,
        )

    @property
    def paramrange(self):
        return np.array(sorted(self._fg_to_segments.keys()))

    @property
    def shapefunc(self):
        def inner(resolution, foreground):
            segments = self._fg_to_segments[foreground]
            return cuccl.profile.patterns.spiral(resolution, segments, 'out')
        return inner


class SpiralProfile(cuccl.profile.patterns.ImageProfile):
    element_class = SpiralElement
    filepattern = '-'.join([
        'spiral',
        'resol:{resolution}',
        'alg:{algorithm}',
        'props:{properties}-s',
        'param:{{:.2f}}',
    ])
    name='spiral pattern scaling'
    parameter_name='foreground occupancy'
    short_name = 'spiral'


if __name__ == '__main__':
    args = SpiralProfile.options_class.get()

    profile = SpiralProfile(args)

    profile.run()

    if args.just_profile:
        sys.exit(0)

    profile.gen_plots()
    profile.print_stats()
