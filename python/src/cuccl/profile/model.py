# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Processing time prediction model fitting.
"""

# Must be imported first, because cuccl.profile loads it with pgf backend

import argparse
import itertools
import cuccl.profile
import matplotlib as matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy
import os
import pandas
import re
import scipy.optimize
import textwrap


def func(x, *c):
    """Computes the value of a generic linear function with an arbitrary number of variables.

    The number of variables is determined by the first dimension of the input vector `x`, which should match the length
    of the `c` vector. The value of the function is computed on the given data points by multiplying the point
    coordinates by the linear coefficients. A constant factor may be implemented by padding the `x` vector with a
    homogenizing coordinate.

    Parameters
    ----------
    x : (M, N) :class:`numpy.ndarray`
        An array of N data points on M variables. If a (M,)-shaped array is given, it is interpreted as a single data
        point. For a linear function of a single variable, you should pass a (1, N) array.

    c : (M,) :class:`numpy.array` numbers
        The linear coefficients. The i-th element is interpreted as the coefficient of the i-th variable.

    Returns
    -------
    y : (N,) :class:`numpy.ndarray`
        The value of the linear function computed on the N data points.

    Examples
    --------
        >>> import numpy as np
        >>> from cuccl.profile.model import func

    Compute the value of the function :math:`f(x, y) = 2 + 3x - 4y` on points :math:`(2, 4), (4, 5), (-8, -7)`:

        >>> coefficients = [2, 3, -4]
        >>> points = np.array([
        ...     [1, 2, 4],
        ...     [1, 4, 5],
        ...     [1, -8, -7]
        ... ]).transpose()
        >>> func(points, coefficients)
        array([-8, -6,  6])

    """
    x = numpy.array(x)
    x = x.reshape((x.shape[0], -1))
    c = numpy.array(c).reshape((x.shape[0], 1))
    return numpy.sum(c * x, axis=0)


class DataLoader(object):
    pattern_classes = [
        'instancing',
        'leveling',
        'random_instancing',
        'random_merging',
        'scaling',
        'spiral',
        'subdivision',
    ]
    video_classes = [
        'segtrack',
        'pascal',
        'davis',
        'coco',
    ]
    known_resolutions = [1024, 2048, 4096, 8192]

    def __init__(self, algorithm, properties, resolutions=None):
        self.algorithm = algorithm
        self.properties = properties
        if resolutions is not None:
            self.resolutions = resolutions
        else:
            self.resolutions = self.known_resolutions

    def load_all_points(self, query=None):
        return pandas.concat([
            self.load_pattern_points(query=query),
            self.load_video_points(query=query),
        ])

    def load_pattern_points(self, pattern_classes=pattern_classes, query=None):
        return self._load_points(
            pattern='patterns:' + ','.join(sorted(pattern_classes)),
            exclude_classes=['video'],
            include_classes=pattern_classes,
            query=query,
        )

    def load_video_points(self, video_classes=video_classes, query=None):
        return self._load_points(
            pattern='videos:' + ','.join(sorted(video_classes)),
            only_videos=video_classes,
            include_classes=['video'],
            query=query,
        )

    def _load_points(self, pattern, only_videos=None, exclude_classes=None, include_classes=None, query=None):
        if self.properties in ['yes', 'late']:
            props_load = self.properties
        elif self.properties in ['no']:
            props_load = 'late'

        pattern_file_path = '{}/{pattern}-alg:{alg}-props:{props}-resol:{resol}.npy'.format(
            cuccl.profile.Profile.CACHEDIR,
            pattern=pattern,
            alg=self.algorithm,
            props=props_load,
            resol=','.join(str(x) for x in sorted(self.resolutions))
        )

        if os.path.exists(pattern_file_path):
            with open(pattern_file_path, 'rb') as pattern_file:
                data_points = numpy.load(pattern_file)
        else:
            point_list = []

            for cache_path in os.listdir(cuccl.profile.Profile.CACHEDIR):
                profile_class = cache_path[:cache_path.find('-')]
                resolution = int(re.search(r'resol:(\d+)', cache_path).group(1))
                algorithm = re.search(r'alg:(\w+)', cache_path).group(1)
                properties = re.search(r'props:(\w+)', cache_path).group(1)

                if self.resolutions is not None and resolution not in self.resolutions:
                    continue

                if algorithm != self.algorithm or properties != props_load:
                    continue

                if profile_class == 'video':
                    video_name = re.search(r'video-(\w+)', cache_path).group(1)
                    if only_videos is not None and video_name not in only_videos:
                        continue

                if exclude_classes is not None and profile_class in exclude_classes:
                    continue

                if include_classes is not None and profile_class not in include_classes:
                    continue

                with open(cuccl.profile.Profile.CACHEDIR + '/' + cache_path, 'rb') as cache_file:
                    cache = numpy.load(cache_file).ravel()
                point_list.append(cache)

            data_points = numpy.hstack(point_list)

            with open(pattern_file_path, 'wb') as pattern_file:
                numpy.save(pattern_file, data_points)

        data_frame = pandas.DataFrame(data_points)

        if self.properties == 'no':
            data_frame.eval('C = C - M - P', inplace=True)

        if query is not None:
            data_frame = data_frame.query(query)

        return data_frame


class Solution(object):
    def __init__(self, source_data, target_data, **coefficients):
        self.source_data = source_data
        self.target_data = target_data
        self.coefficients = coefficients

    def __repr__(self):
        return '{}({})'.format(
            self.__class__.__name__,
            ', '.join('{}={}'.format(*pair) for pair in self.coefficients.items())
        )

    def __call__(self, x):
        x_selected = numpy.vstack([x[attr] for attr in self.coefficients])
        return func(x_selected, *self.coefficients.values())

    def error(self, x_target, y_target, relative=False):
        y_estimate = self(x_target)
        error = y_estimate - y_target
        if relative:
            return error / y_target
        else:
            return error

    def sqr_error(self, x_target, y_target):
        return (self(x_target) - y_target)**2


class ModelFitting(object):
    # What goes into the fitting process
    attributes = [
        'homog',
        'i',
        'fg',
        'cccount',
        'avg_ccsize',
        'max_ccsize',
        'pixels',
    ]

    def __init__(self, algorithm, properties, resolutions=None, source_type='patterns', target_type='videos', pattern_classes=DataLoader.pattern_classes, video_classes=DataLoader.video_classes, combine_sources=True, extra_query=None):
        self.algorithm = algorithm
        self.properties = properties
        self.resolution = resolutions

        self.source_type = source_type
        self.target_type = target_type

        self.pattern_classes = pattern_classes
        self.video_classes = video_classes

        self.combine_sources = combine_sources
        self.extra_query = extra_query

        self.data_loader = DataLoader(algorithm=algorithm, properties=properties, resolutions=resolutions)
        self.solutions = []
        
    def compute_fit(self, x_attributes=attributes, y_attribute='C', sorting='mean_abs'):
        if self.target_type == 'patterns':
            target_data = self.data_loader.load_pattern_points(pattern_classes=args.patterns)
        elif self.target_type == 'videos':
            target_data = self.data_loader.load_video_points(video_classes=args.videos)
        if self.extra_query is not None:
            target_data = target_data.query(self.extra_query)
        
        parameter_combinations = []
        for k in range(1, len(x_attributes) + 1):
            for params in itertools.combinations(x_attributes, k):
                parameter_combinations.append(params)

        if self.source_type == 'patterns':
            source_classes = self.pattern_classes
        else:
            source_classes = self.video_classes
            
        source_classes_combinations = []
        if self.combine_sources:
            for k in range(1, len(source_classes) + 1):
                for sources in itertools.combinations(source_classes, k):
                    source_classes_combinations.append(sources)
        else:
            source_classes_combinations.append(source_classes)   

        for sources in source_classes_combinations:
            if self.source_type == 'patterns':
                source_data = self.data_loader.load_pattern_points(pattern_classes=sources)
            elif self.source_type == 'videos':
                source_data = self.data_loader.load_video_points(video_classes=sources)
            if self.extra_query is not None:
                source_data = source_data.query(self.extra_query)

            initial_coefs = {param: 0.0 for param in x_attributes}
            initial_coefs['homog'] = 0.0
            for attr in x_attributes:
                if attr != 'homog':
                    y_max = numpy.max(source_data[y_attribute])
                    y_min = numpy.min(source_data[y_attribute])
                    x_max = numpy.max(source_data[attr])
                    x_min = numpy.min(source_data[attr])

                    if x_max > x_min:
                        slope = (y_max - y_min) / (x_max - x_min)
                        constant = y_max - x_max * slope
                    else:
                        slope = 0
                        constant = numpy.mean(source_data[y_attribute])

                    initial_coefs[attr] = slope
                    initial_coefs['homog'] += constant

            for params in parameter_combinations:
                xdata = source_data[list(params)].T
                ydata = source_data[y_attribute]
                p0 = numpy.array([initial_coefs[attr] for attr in params]) / len(params)

                opt_constants, _ = scipy.optimize.curve_fit(func, xdata=xdata, ydata=ydata, p0=p0)
                solution = Solution(
                    source_data=source_data,
                    target_data=target_data,
                    **dict(zip(params, opt_constants))
                )

                self.solutions.append(solution)

        if sorting == 'sqr_error':
            self.solutions.sort(
                key=lambda sol: numpy.sum(sol.sqr_error(target_data, target_data[y_attribute]))
            )
        elif sorting == 'mean_abs':
            self.solutions.sort(
                key=lambda sol: numpy.mean(numpy.abs(sol.error(target_data, target_data[y_attribute])))
            )

    def plot_error(self, solution, target_data, y_attribute):
        error = solution.error(target_data, target_data[y_attribute], True)

        plt.hist(error, 100)
        plt.xlim(-1.0, 1.0)

        if self.properties == 'late':
            algname = 'Late ' + self.algorithm.upper()
        elif self.properties == 'yes':
            algname = 'Early ' + self.algorithm.upper()
        elif self.properties == 'no':
            algname = 'Ordinary ' + self.algorithm.upper()
        plt.title('Relative error distribution for ' + algname)
        plt.grid()
        plt.show()


def command_estimate(args):
    y_attribute = 'C'

    for alg, props in itertools.product(args.algorithms, args.properties):
        print '{alg}:{props}:{res!r}'.format(alg=alg, props=props, res=args.resolutions)

        fitting = ModelFitting(
            algorithm=alg,
            properties=props,
            resolutions=args.resolutions,
            source_type=args.source,
            target_type=args.target,
            pattern_classes=args.patterns,
            video_classes=args.videos,
            combine_sources=args.combine_sources,
        )

        fitting.compute_fit(x_attributes=args.attributes, y_attribute=y_attribute, sorting=args.sorting)

        for k, solution in enumerate(fitting.solutions[:args.rank]):
            if args.source == 'patterns':
                source_classes = solution.source_data['type'].unique()
            elif args.source == 'videos':
                source_classes = solution.source_data['param'].unique()

            if args.sorting == 'mean_abs':
                source_error = numpy.mean(numpy.abs(solution.error(
                    x_target=solution.source_data,
                    y_target=solution.source_data[y_attribute],
                    relative=True
                )))
                target_error = numpy.mean(numpy.abs(solution.error(
                    x_target=solution.target_data,
                    y_target=solution.target_data[y_attribute],
                    relative=True
                )))

                print textwrap.dedent('''
                    #{k} solution: {solution!r}
                        Source: {source_type}: {source_classes}
                            Points: {source_points}
                            Error: mean={source_error:.1f}%
                        Target: {target_type}
                            Points: {target_points}
                            Error: mean={target_error:.1f}%
                        Extra query: {query}''').format(
                    k=k + 1,
                    solution=solution,
                    source_type=args.source, target_type=args.target,
                    source_classes=', '.join(source_classes),
                    source_points=len(solution.source_data), target_points=len(solution.target_data),
                    source_error=100 * source_error,
                    target_error=100 * target_error,
                    query=args.query
                )
            elif args.sorting == 'sqr_error':
                source_error = solution.error(
                    x_target=solution.source_data,
                    y_target=solution.source_data[y_attribute],
                    relative=True
                )
                target_error = solution.error(
                    x_target=solution.target_data,
                    y_target=solution.target_data[y_attribute],
                    relative=True
                )

                source_error_mean = numpy.mean(source_error)
                target_error_mean = numpy.mean(target_error)

                source_error_std = numpy.std(source_error)
                target_error_std = numpy.std(target_error)

                print textwrap.dedent('''
                    #{k} solution: {solution!r}
                        Source: {source_type}: {source_classes}
                            Points: {source_points}
                            Error: mean={source_error_mean:.1f}%; std={source_error_std:.1f}%
                        Target: {target_type}
                            Points: {target_points}
                            Error: mean={target_error_mean:.1f}%; std={target_error_std:.1f}%
                        Extra query: {query}''').format(
                    k=k + 1,
                    solution=solution,
                    source_type=args.source, target_type=args.target,
                    source_classes=', '.join(source_classes),
                    source_points=len(solution.source_data), target_points=len(solution.target_data),
                    source_error_mean=100 * source_error_mean, source_error_std=100 * source_error_std,
                    target_error_mean=100 * target_error_mean, target_error_std=100 * target_error_std,
                    query=args.query
                )
            if args.plot:
                fitting.plot_error(solution, target_data=solution.target_data, y_attribute=y_attribute)


def command_explore(args):
    for alg, props in itertools.product(args.algorithms, args.properties):
        if props == 'yes':
            title = 'Early '+ alg.upper()
        else:
            title = 'Late ' + alg.upper()

        data_loader = DataLoader(algorithm=alg, properties=props, resolutions=args.resolutions)

        print 'Loading pattern points...'
        patterns = pandas.DataFrame(
            data_loader.load_pattern_points(pattern_classes=args.patterns, query=args.query)
        )

        print 'Loading video points...'
        videos = pandas.DataFrame(
            data_loader.load_video_points(video_classes=args.videos, query=args.query)
        )

        print 'Preparing plots...'
        investigate_all_compare(
            patterns, videos,
            resolutions=resolutions,
            title_a='%s (patterns)' % title,
            title_b='%s (videos)' % title,
            linestyle='--',
            marker='o',
        )


def investigate_all(dataset, resolutions=DataLoader.known_resolutions, samples=20, cc=30, title=None, **plot_opts):
    investigate_pixels(dataset, resolutions=resolutions, title=title, **plot_opts)
    investigate_fg(dataset, resolutions=resolutions, n=samples, title=title, **plot_opts)
    investigate_i(dataset, resolutions=resolutions, title=title, **plot_opts)
    investigate_cccount(dataset, resolutions=resolutions, n=cc, title=title, **plot_opts)
    investigate_avg_ccsize(dataset, resolutions=resolutions, n=samples, title=title, **plot_opts)
    investigate_max_ccsize(dataset, resolutions=resolutions, n=samples, title=title, **plot_opts)
    plt.show()


def investigate_all_compare(dataset_a, dataset_b, resolutions=DataLoader.known_resolutions, samples=20, cc=30, title_a=None, title_b=None, **plot_opts):
    fig, axes = plt.subplots(2, 3, figsize=(18, 10), sharex='col', sharey='col')

    # fig, axes = plt.subplots(2, 1, sharex=True, sharey=True, figsize=(6, 10))
    investigate_pixels(dataset_a, resolutions=resolutions, title=title_a, ax=axes[0, 0], **plot_opts)
    investigate_pixels(dataset_b, resolutions=resolutions, title=title_b, ax=axes[1, 0], **plot_opts)

    # fig, axes = plt.subplots(2, 1, sharex=True, sharey=True, figsize=(6, 10))
    investigate_i(dataset_a, resolutions=resolutions, title=title_a, ax=axes[0, 1], **plot_opts)
    investigate_i(dataset_b, resolutions=resolutions, title=title_b, ax=axes[1, 1], **plot_opts)

    # fig, axes = plt.subplots(2, 1, sharex=True, sharey=True, figsize=(6, 10))
    investigate_cccount(dataset_a, resolutions=resolutions, n=cc, title=title_a, ax=axes[0, 2], **plot_opts)
    investigate_cccount(dataset_b, resolutions=resolutions, n=cc, title=title_b, ax=axes[1, 2], **plot_opts)

    fig, axes = plt.subplots(2, 3, figsize=(18, 10), sharex='col', sharey='col')

    # fig, axes = plt.subplots(2, 1, sharex=True, sharey=True, figsize=(6, 10))
    investigate_fg(dataset_a, resolutions=resolutions, n=samples, title=title_a, ax=axes[0, 0], **plot_opts)
    investigate_fg(dataset_b, resolutions=resolutions, n=samples, title=title_b, ax=axes[1, 0], **plot_opts)

    # fig, axes = plt.subplots(2, 1, sharex=True, sharey=True, figsize=(6, 10))
    investigate_avg_ccsize(dataset_a, resolutions=resolutions, n=samples, title=title_a, ax=axes[0, 1], **plot_opts)
    investigate_avg_ccsize(dataset_b, resolutions=resolutions, n=samples, title=title_b, ax=axes[1, 1], **plot_opts)

    # fig, axes = plt.subplots(2, 1, sharex=True, sharey=True, figsize=(6, 10))
    investigate_max_ccsize(dataset_a, resolutions=resolutions, n=samples, title=title_a, ax=axes[0, 2], **plot_opts)
    investigate_max_ccsize(dataset_b, resolutions=resolutions, n=samples, title=title_b, ax=axes[1, 2], **plot_opts)

    plt.show()


def investigate_avg_ccsize(dataset, resolutions=DataLoader.known_resolutions, n=20, ax=None, title=None, **plot_opts):
    if ax is None:
        plt.figure()
        ax = plt.gca()

    for res in resolutions:
        factor = (1024.0 / res) ** 2
        x = numpy.linspace(0, 1.0, n, endpoint=False)
        y = [
            numpy.mean(dataset.query('pixels == %d' % (res ** 2)).query(
                '%f <= avg_ccsize / %d < %f' % (float(i) / n, res ** 2, float(i + 1) / n))['C'] * factor) for i in
            range(n)
        ]
        ax.plot(x + 0.5 / n, y, label='Resolution $%d^2$' % res, **plot_opts)
    ax.grid()
    ax.legend(loc='best')
    if title is None:
        ax.set_xlabel('Average CC occupancy')
    else:
        ax.set_xlabel('Average CC occupancy (%s)' % title)


def investigate_cccount(dataset, resolutions=DataLoader.known_resolutions, n=30, ax=None, title=None, **plot_opts):
    if ax is None:
        plt.figure()
        ax = plt.gca()

    for res in resolutions:
        factor = (1024.0 / res) ** 2
        ax.plot(
            range(n + 1),
            [numpy.mean(dataset.query('pixels == %d' % (res ** 2)).query('cccount == %d' % i)['C'] * factor) for i in range(n + 1)],
            label='Resolution $%d^2$' % res,
            **plot_opts
        )
    ax.grid()
    ax.legend(loc='best')
    if title is None:
        ax.set_xlabel('Number of CCs')
    else:
        ax.set_xlabel('Number of CCs (%s)' % title)


def investigate_fg(dataset, resolutions=DataLoader.known_resolutions, n=20, ax=None, title=None, **plot_opts):
    if ax is None:
        plt.figure()
        ax = plt.gca()

    for res in resolutions:
        factor = (1024.0 / res) ** 2
        ax.plot(
            0.5 / n + numpy.linspace(0, 1.0, n, endpoint=False),
            [numpy.mean(dataset.query('pixels == %d' % (res ** 2)).query(
                '%f <= fg < %f' % (float(i) / n, float(i + 1) / n))['C'] * factor) for i in range(n)],
            label='Resolution $%d^2$' % res,
            **plot_opts
        )
    ax.grid()
    ax.legend(loc='best')
    if title is not None:
        ax.set_xlabel('Foreground occupancy')
    else:
        ax.set_xlabel('Foreground occupancy (%s)' % title)


def investigate_i(dataset, resolutions=DataLoader.known_resolutions, ax=None, title=None, **plot_opts):
    if ax is None:
        plt.figure()
        ax = plt.gca()

    for res in resolutions:
        factor = (1024.0 / res) ** 2
        ax.plot(
            range(1, 7),
            [numpy.mean(dataset.query('pixels == %d' % (res ** 2)).query('i == %d' % itr)['C'] * factor) for itr in
             range(1, 7)],
            label='Resolution $%d^2$' % res,
            **plot_opts
        )
    ax.grid()
    ax.legend(loc='best')
    if title is None:
        ax.set_xlabel('Iterations')
    else:
        ax.set_xlabel('Iterations (%s)' % title)


def investigate_max_ccsize(dataset, resolutions=DataLoader.known_resolutions, n=20, ax=None, title=None, **plot_opts):
    if ax is None:
        plt.figure()
        ax = plt.gca()

    for res in resolutions:
        factor = (1024.0 / res) ** 2
        ax.plot(
            0.5 / n + numpy.linspace(0, 1.0, n, endpoint=False),
            [numpy.mean(dataset.query('pixels == %d' % (res ** 2)).query(
                '%f <= max_ccsize / %d < %f' % (float(i) / n, res ** 2, float(i + 1) / n))['C'] * factor) for i in
             range(n)],
            label='Resolution $%d^2$' % res,
            **plot_opts
        )
    ax.grid()
    ax.legend(loc='best')
    if title is None:
        ax.set_xlabel('Maximum CC occupancy')
    else:
        ax.set_xlabel('Maximum CC occupancy (%s)' % title)


def investigate_pixels(dataset, resolutions=DataLoader.known_resolutions, ax=None, title=None, **plot_opts):
    if ax is None:
        plt.figure()
        ax = plt.gca()

    ax.plot(
        [res ** 2 for res in resolutions],
        [numpy.mean(dataset.query('pixels == %d' % (res ** 2))['C']) for res in resolutions],
        **plot_opts
    )
    ax.grid()
    if title is None:
        ax.set_xlabel('Pixels')
    else:
        ax.set_xlabel('Pixels (%s)' % title)


if __name__ == '__main__':
    algorithms = ['le', 'tm']
    properties = ['no', 'yes', 'late']
    resolutions = [1024, 2048, 4096, 8192]

    # ------------------------------------------------------------------------------------------------------------------
    # Overall program parser
    # ------------------------------------------------------------------------------------------------------------------
    parser = argparse.ArgumentParser(
        description='CuCCL performance prediction model fitting'
    )
    subparsers = parser.add_subparsers(dest='command')

    # ------------------------------------------------------------------------------------------------------------------
    # Common options parser
    # ------------------------------------------------------------------------------------------------------------------
    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument(
        '-r', '--resolutions',
        nargs='*',
        type=int,
        choices=resolutions,
        default=resolutions,
        help='Selected resolutions used. With the "estimate" sub-command, this will imply which resolutions will be '
             'thrown in together in the optimization process. With the "explore" sub-command, this will imply which '
             'resolutions will be plotted, but each resolution will be considered separetely when averaging.',
    )
    common_parser.add_argument(
        '-a', '--algorithms',
        nargs='+',
        choices=algorithms,
        default=algorithms,
        help='Selected algorithms. Use "le" for "Label Equivalence" and "tm" for "Tile Merging".',
    )
    common_parser.add_argument(
        '-p', '--properties',
        nargs='+',
        choices=properties,
        default=properties,
        help='Selected properties computation modes. Use "yes" for "Early Computation" and "late" for "Late '
             'Computation".',
    )
    common_parser.add_argument(
        '-q', '--query',
        default=None,
        help='Specify extra filtering criteria for loaded data points. The criteria should be specified in the form of '
             'a Python expression involving the image/video cached variables, for example: use "fg < 0.3" to filter out'
             ' data points with a total foreground occupancy above 30%; use "i == 2 or i == 3" to select only data '
             'points taking 2 or 3 iterations; use "C > 1900" to filter out data points with a total computation time '
             'below 1900 nanoseconds.',
    )
    common_parser.add_argument(
        '-v', '--videos',
        nargs='+',
        choices=DataLoader.video_classes,
        default=DataLoader.video_classes,
        help='Selected video datasets. With the "estimate" sub-command, all videos will be used for estimating the '
             'error and ranking the solutions.',
    )
    common_parser.add_argument(
        '--patterns',
        nargs='*',
        choices=DataLoader.pattern_classes,
        default=DataLoader.pattern_classes,
        help='Restrict synthetic image classes used in the "estimate" and "explore" sub-commands. By default, all knwon'
             ' classes will be used.',
    )
    common_parser.add_argument(
        '--non-random-patterns',
        dest='patterns',
        action='store_const',
        const=[pattern for pattern in DataLoader.pattern_classes if 'random' not in pattern],
        help='Restrict synthetic image classes used in the "estimate" and "explore" sub-commands to all classes which '
             'do not present randomized behavior.',
    )
    common_parser.add_argument(
        '--random-patterns',
        dest='patterns',
        action='store_const',
        const=[pattern for pattern in DataLoader.pattern_classes if 'random' in pattern],
        help='Restrict synthetic image classes used in the "estimate" and "explore" sub-commands to all classes which '
             'present randomized behavior.',
    )
    common_parser.add_argument(
        '--ridge-patterns',
        action='store_const',
        const=[pattern for pattern in DataLoader.pattern_classes if pattern != 'spiral'],
        help='Restrict synthetic image classes used in the "estimate" and "explore" sub-commands to ridge pattern '
             'classes, e.g. excludes spiral patterns.',
    )

    # ------------------------------------------------------------------------------------------------------------------
    # 'estimate' sub-command parser
    # ------------------------------------------------------------------------------------------------------------------
    estimate_parser = subparsers.add_parser(
        'estimate',
        parents=[common_parser],
        help='Estimate optimal parameters for prediction model, print coefficients, and optionally plot error against '
             'target data points. The model will be estimated from "source points", specified via the --source option, '
             'and the solutions will be ranked by the mean absolute prediction error on a set of "target points", '
             'specified via the --target option.',
    )
    estimate_parser.add_argument(
        '-t', '--attributes',
        nargs='+',
        choices=ModelFitting.attributes,
        default=ModelFitting.attributes,
        help='Restrict image parameters that will be used for searching for an optimal performance prediction function.'
             ' By default, all combinations of all known parameters will be considered.',
    )
    estimate_parser.add_argument(
        '--plot',
        default=False,
        action='store_const',
        const=True,
        help='Plot histograms of errors computed on target data points, for each ranked solution, for each algorithm.'
    )
    estimate_parser.add_argument(
        '--rank',
        type=int,
        metavar='N',
        default=1,
        help='Print all but the best N solutions (considering the mean absolute prediction error on the target data '
             'points.',
    )
    estimate_parser.add_argument(
        '--source',
        default='patterns',
        choices=['patterns', 'videos'],
        help='Which points to use for estimating the prediction function. By default, synthetic image data points will '
             'be used, and it is possible to further restrict them using the --patterns, --random-patterns, and '
             '--non-random-patterns options. However, it is also possible to use video data points, and further '
             'restrict the set of videos with the --videos option.',
    )
    estimate_parser.add_argument(
        '--target',
        default='videos',
        choices=['patterns', 'videos'],
        help='Which points to use for calculating the prediction function error. By default, video data points will be '
             'used, and it is possible to further restrict the set of videos with the --videos option. However, it is '
             'also possible to use synthetic image data points, and further restrict them using the --patterns, '
             '--random-patterns, and --non-random-patterns options.',
    )
    estimate_parser.add_argument(
        '--combine-sources',
        action='store_const',
        const=True,
        default=False,
        help='Test all combinations of source data classes. If source data is "patterns", all combinations of synthetic '
             'data will be tried. If source data is "videos", all combinations of video datasets will be tried. This '
             'does not affect target data, which are always the complete set specified with --target (optionally '
             'filtered by --query).'
    )
    estimate_parser.add_argument(
        '--sorting',
        choices=['mean_abs', 'sqr_error'],
        default='mean_abs',
        help='Method for sorting solutions: "mean_abs" sorts by mean absolute error on target dataset; "sqr_error" '
             'sorts by total squared error on target dataset, which indeed corresponds to the metric used by the least '
             'squares optimizer, but does not necessarily give the best results in terms of error histograms.'
    )

    # ------------------------------------------------------------------------------------------------------------------
    # 'explore' sub-command parser
    # ------------------------------------------------------------------------------------------------------------------
    explore_parser = subparsers.add_parser(
        'explore',
        parents=[common_parser],
        help='Visualize the impact of image parameters on processing times. Average performance will be plotted against'
             ' each known image parameter, both for synthetic image data points and video data points.',
    )

    args = parser.parse_args()

    if args.command == 'estimate':
        command_estimate(args)
    elif args.command == 'explore':
        command_explore(args)
