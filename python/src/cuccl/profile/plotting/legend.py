# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2017 Pedro de Souza Asad
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/psa-exe/cuccl/>.

"""Produces a legend for CuCCL profiling plots.
"""

import cuccl.profile
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

import cuccl.profile.patterns
import cuccl.profile.plotting

if __name__ == '__main__':
    args = cuccl.profile.patterns.ImageProfilingOptions.get()

    figure, axes = cuccl.profile.plotting.subplots(1, 1, mode='double', aspect=0.12, frameon=False)
    axes = axes[0, 0]

    if args.layout in ['1', '2']:
        axes.plot([], [], 'k--', label='Ordinary time (no properties)')
        axes.fill_between([], [], color=(0, 0, 0, 0), label='  ')
        axes.fill_between([], [], color=(0, 0, 0, 0), label='  ')
    elif args.layout in ['3']:
        # axes.plot([], [], color=args.colors['late'], linestyle='--', label='Late properties time')
        # axes.plot([], [], color=args.colors['early'], linestyle='--', label='Early properties time')
        axes.fill_between([], [], color=(0, 0, 0, 0), label='  ')
        axes.fill_between([], [], color=(0, 0, 0, 0), label='  ')
        axes.plot([], [], color=(0, 0, 0, 0), label='  ')
    axes.fill_between([], [], color=args.colors['O'], label='CPU overhead')

    axes.fill_between([], [], color=args.colors['I'], label='Prelabel/Local Merge')
    axes.fill_between([], [], color=args.colors['R'], label='Sequential Relabeling')

    axes.fill_between([], [], color=args.colors['f'], label='First Analysis/Path Compression')
    axes.fill_between([], [], color=args.colors['F'], label='Later Analysis/Path Compression')

    axes.fill_between([], [], color=args.colors['j'], label='First Scan/Global Merge')
    axes.fill_between([], [], color=args.colors['J'], label='Later Scan/Global Merge')

    axes.fill_between([], [], color=args.colors['M'], label='Memset (not in Early LE)')
    axes.fill_between([], [], color=args.colors['P'], label='Properties Sum (Late only)')


    axes.xaxis.set_visible(False)
    axes.yaxis.set_visible(False)

    axes.axis('off')
    legend = axes.legend(loc='upper left', ncol=3)

    cuccl.profile.plotting.savefig(
        figure,
        'profile/out/plots/legend-s',
        legend,
    )
    plt.close(figure)
