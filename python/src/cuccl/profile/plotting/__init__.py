# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.
import matplotlib as mpl
mpl.rcParams.update({					# setup matplotlib to use latex for output
    "axes.labelsize": 8,				# LaTeX default is 10pt font.
    "figure.autolayout": True,
    "font.family": 'serif',
    "font.monospace": [],
    "font.sans-serif": [],
    "font.size": 8,
    "font.serif": [],					# blank entries should cause plots to inherit fonts from the document
    "legend.fontsize": 8,				# Make the legend/label fonts a little smaller
    "pgf.preamble": [
        r"\usepackage[utf8x]{inputenc}",	# use utf8 fonts becasue your computer can handle it :)
        r"\usepackage[T1]{fontenc}",		# plots will be generated using this preamble
    ],
    "pgf.texsystem": "lualatex",		# change this if using xetex or lautex
    "text.usetex": True,				# use LaTeX to write all text
    "xtick.labelsize": 8,
    "ytick.labelsize": 8,
})
mpl.use('Agg')
import numpy as np
from matplotlib import pyplot as plt

GOLDEN_RATIO = 0.6180339887498949


def figure(scale=1.0, mode='single', aspect=GOLDEN_RATIO, **kwargs):
    """Creates a figure.

    Parameters
    ----------
    **kwargs :
        Extra parameters to the :func:`matplotlib.pyplot.figure` function.

    See :func:`figsize` for other parameters
    """
    kwargs.update({	'figsize': figsize(scale, mode, aspect) })
    return plt.figure(**kwargs)


def figsize(scale=1.0, mode='single', aspect=GOLDEN_RATIO):
    """Calculates figure sizes.

    Parameters
    ----------
    scale : number (0-1)
        Width of figure, as a fraction of the text column.

    mode : ``'single'`` or ``'double'``
        Whether it's a single or double-column figure.

    aspect : number
        Aspect ratio (height / width) of figure.

    Notes
    -----
    A good choice for the aspect ratio when multiple plots are present in the
    figure is ``GOLDEN_RATIO`` multiplied by the ratio between the number of
    rows and the number columns.
    """

    # These measures were obtained with the \the\linewidth LaTeX command for
    # the svjour2.cls Springer-Verlag journal template
    line_width = {
        'single': 244.30945, # Line width in single column mode
        'double': 500.0,	 # Line width in double column mode
    }

    fig_width_pt = line_width[mode] # Get this from LaTeX using \the\textwidth
    inches_per_pt = 1.0/72.27       # Convert pt to inch

    fig_width = fig_width_pt * inches_per_pt * scale # width in inches
    fig_height = fig_width * aspect                  # height in inches

    return (fig_width,fig_height)


def subplots(rows, cols, scale=1.0, mode='single', aspect=GOLDEN_RATIO, **kwargs):
    """Creates a figure and related axes.

    Parameters
    ----------
    rows : integer
        Number of subplot rows.

    cols : integer
        Number of subplot cols.

    **kwargs :
        Extra parameters to the :func:`matplotlib.pyplot.subplots` function.

    See :func:`figsize` for other parameters
    """
    kwargs.update({	'figsize': figsize(scale, mode, 1.0 * rows * aspect / cols) })
    fig, axes = plt.subplots(rows, cols, **kwargs)
    axes = np.array(axes, dtype=object).reshape((rows, cols))
    return fig, axes


def savefig(figure, filename, *extra_artists, **kwargs):
    """Saves a figure.

    Parameters
    ----------
    figure : :class:`matplotlib.figure.Figure` instance

    filename : string

    *extra_artists :
        list of :class:`matplotlib.artist.Artist` instances Extra elements such
        as legends that should be accounted for when automatically adjusting
        the layout.
    """
    if 'onlypdf' in kwargs and not kwargs['onlypdf']:
        figure.savefig('%s.pgf' % filename,
            bbox_extra_artists=extra_artists,
            bbox_inches='tight'
        )

    figure.savefig('%s.pdf' % filename,
        bbox_extra_artists=extra_artists,
        bbox_inches='tight'
    )
