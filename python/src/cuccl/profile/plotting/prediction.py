import cuccl.profile.model as cpm
import cuccl.profile.plotting as cpp
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

# Minimizing the mean absolute error
models = {
    ('le', 'no'): {
        'cccount': 5.37761118937,
        'max_ccsize': 0.000240294136555,
        'pixels': 0.000257893269375,
    },
    ('le', 'yes'): {
        'max_ccsize': 0.00146979018622,
        'avg_ccsize':-0.000267670108018,
        'pixels': 0.000480333112302,
    },
    ('le', 'late'): {
        'max_ccsize': 0.0022978060859,
        'pixels': 0.000274319409644,
    },
    ('tm', 'no'): {
        'max_ccsize': 0.000316197882804,
        'homog': 260.699239593,
        'i': -68.0084613352,
        'fg': 667.182668106,
        'cccount': 5.51623626518,
        'pixels': 0.000206487103409,
    },
    ('tm', 'yes'): {
        'i': -93.966144211,
        'max_ccsize': 0.00062535461821,
        'fg': 824.181646647,
        'pixels': 0.000379359866632,
        'homog': 353.408681745
    },
    ('tm', 'late'): {
        'i': -69.6826141628,
        'max_ccsize': 0.00310523657066,
        'avg_ccsize': -0.000867160919192,
        'pixels': 0.000233560940451,
        'homog': 340.397243194,
    },
}

# Minimizing the sum of squared errors
# models = {
#     ('le', 'no'): {
#         'homog': -5672.568491590,
#         'i': 1720.06801949,
#         'max_ccsize': 0.000173984617995,
#         'pixels': 0.000313407469464
#     },
#     ('le', 'yes'): {
#         'homog': -244.629320033,
#         'avg_ccsize': 0.000655427175346,
#         'cccount': 7.07205788224,
#         'max_ccsize': 0.00103700682449,
#         'pixels': 0.000548211381094},
#     ('le', 'late'): {
#         'homog': -99.9981537785,
#         'cccount': 14.5724563385,
#         'max_ccsize': 0.00240566981177,
#         'pixels': 0.000450348953718
#     },
#     ('tm', 'no'): {
#         'homog': 233.8163227280,
#         'i': -55.9097640129,
#         'max_ccsize': 0.000385910114075,
#         'pixels': 0.000238062717085
#     },
#     ('tm', 'yes'): {
#         'homog': 0.000000000,
#         'max_ccsize': 0.000798278174259,
#         'pixels': 0.00046565428289
#     },
#     ('tm', 'late'): {
#         'homog': 149.237910775,
#         'cccount': 12.4179527392,
#         'i': -47.65131656,
#         'max_ccsize': 0.00261714474651,
#         'pixels': 0.000419620802017
#     },
# }

algorithms = 'le tm'.split()
properties = 'no yes late'.split()

fig, axes = cpp.subplots(len(properties), len(algorithms), sharex=True, sharey=True, scale=1.0, mode='single', aspect=0.8)

hist_limits = (-2, 2)
hist_bins = np.linspace(hist_limits[0], hist_limits[1], 101)

for i, props in enumerate(properties):
    for j, alg in enumerate(algorithms):
        loader = cpm.DataLoader(alg, props)
        video_points = loader.load_video_points(query='fg < 0.3')

        model = models[alg, props]

        c_vector = np.array(model.values()).reshape((1, len(model)))
        x_vector = np.array(video_points[model.keys()])
        y_vector = np.sum(c_vector * x_vector, axis=1)
        z_vector = np.array(video_points['C'])

        errors = []

        mean_res = video_points.groupby('pixels')['C'].mean()

        for res in [1024, 2048, 4096, 8192]:
            rel_error = (video_points.query('pixels==%d' % res**2)['C'] - mean_res[res**2]) / video_points.query('pixels==%d' % res**2)['C']
            errors.extend(rel_error)

        model_error = (y_vector - z_vector) / z_vector
        point_error = np.array(errors)

        axes[i, j].hist(model_error, bins=hist_bins, alpha=0.5, label='Model error', weights=np.ones_like(z_vector)/len(z_vector), density=False)
        axes[i, j].hist(point_error, bins=hist_bins, fill=False, histtype='step', label='Resolution error', weights=np.ones_like(z_vector)/len(z_vector), density=False)

        axes[i, j].set_xlim(*hist_limits)
        axes[i, j].grid()
        axes[0, j].set_title(alg.upper())
        axes[0, 0].set_xticks(np.linspace(hist_limits[0], hist_limits[1], 5))
        if props == 'no':
            axes[i, 0].set_ylabel('Ordinary')
        elif props == 'yes':
            axes[i, 0].set_ylabel('Early')
        elif props == 'late':
            axes[i, 0].set_ylabel('Late')

plt.show()
