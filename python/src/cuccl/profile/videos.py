# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Profiles the CuCCL library using AVI image collections.
"""

import cuccl.profile
import cuccl.profile.model
import cuccl.profile.patterns.instancing
import itertools as it
import os
import sys
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import re
import scipy.interpolate as sci
import skvideo.io
import subprocess as sp

import cuccl.profile.plotting
from cuccl.profile import openCSV


class VideoProfilingOptions(cuccl.profile.ProfilingOptions):
    @classmethod
    def parser(cls):
        parser = cuccl.profile.ProfilingOptions.parser()

        # Options for videos
        parser.add_argument('-v', '--videos', type=str, default=['segtrack', 'pascal', 'davis', 'coco'], nargs='+', help='Datasets (avi file without extension) to evalute. At least one video required for the video script. Ignored for other scripts.')

        # Options for plotting-related sub-commands (videos-only)
        parser.add_argument('--video-sampling', type=float, help='Rate for randomly selecting video frames for plotting (for huge datasets).', default=1.0)
        parser.add_argument('--only-iteration', type=int, help='Only select video frames having a specific iteration count', nargs='+', default=range(1, 6))
        parser.add_argument('--cccount-range', type=int, help='Filter frames by the number of connected components (inclusive range, on both ends).', nargs=2, default=[0, 2 ** 32 - 1])

        return parser

    @property
    def prefix(self):
        return 'profile/data/'

    @property
    def subdivision_level(self):
        return 2
    
    @property
    def ylim(self):
        return {
            1024: (0, 1),
            2048: (0, 15),
            4096: (0, 20.0),
            8192: (0, 100.0),
            16384: (0, 150.0),
        }


class VideoProfileElement(cuccl.profile.ProfileElement):

    # TODO: Should return information about a single frame
    # @abc.abstractmethod
    # def __getitem__(self, index):
    #     pass

    # TODO: Should return information about all frames
    # @abc.abstractmethod
    # def __iter__(self):
    #     pass

    # TODO: Should return the number of frames
    # @abc.abstractmethod
    # def __len__(self):
    #     pass

    @property
    def filepattern(self):
        return VideoProfile.filepattern.format(
            resolution=self.resolution,
            algorithm=self.algorithm,
            properties=self.properties,
        )

    @property
    def paramrange(self):
        return self.session.videos

    def run(self):
        """Lower-level profiling function, accepting various synthetic pattern specifications.

        This routine profiles a family of synthetic test patterns parametrized by a single variable. It requires the user
        to specify a univariate function that spans the pattern family, and a range of parameters on which the function is
        evaluated to generate specific instances of the family. Profiling sub-products are stored in pre-defined directories
        using a file name pattern also supplied by the user. This routine may recompile the CuCCL driver if its resolution
        does not match the required profiling resolution. Progress is reported by writing to standard output.
        """
        self.session.checkDirectories()

        # Run profiler and generate csv output
        if self.session.withmemory:
            driver_mode = 'prof'
        else:
            driver_mode = 'prof-no-mem'

        for i, param in enumerate(self.paramrange):
            profile_path = self.profPath(param)
            dump_path = self.dumpPath(param)
            video_path = self.videoPath(param)

            if os.path.exists(profile_path) and os.path.exists(dump_path) and not self.session.overwrite:
                continue

            self.session.checkBuildResolution(self.resolution)

            if self.properties in ['yes', 'late']:
                driver_command = ' '.join([
                    '{bin_dir}/cuccl-driver',
                    '-i {video_path}',
                    '-a {algorithm}',
                    '-p {properties} -s',
                    '-m {driver_mode}',
                    '--dump.csv={dump_path}',
                ]).format(
                    bin_dir=self.session.BINDIR,
                    video_path=video_path,
                    algorithm=self.algorithm,
                    properties=self.properties,
                    driver_mode=driver_mode,
                    dump_path=dump_path,
                )
            else:
                driver_command = ' '.join([
                    '{bin_dir}/cuccl-driver',
                    '-i {video_path}',
                    '-a {algorithm}',
                    '-p {properties} -s',
                    '-m {driver_mode}',
                ]).format(
                    bin_dir=self.session.BINDIR,
                    video_path=video_path,
                    algorithm=self.algorithm,
                    properties=self.properties,
                    driver_mode=driver_mode,
                )

            profiler_command = '{}', '--csv', '-u us', '--print-gpu-trace', '--log-file {}', '{}'.format(
                self.session.PROFILER,
                profile_path,
                driver_command
            )

            sp.call(profiler_command.split(), stdout=self.session.stdout, stderr=self.session.stderr)

            self.session.stdout.write('\tProfiling: %5.1f%% completed\r' % (100.0 * (i + 1) / (len(self.paramrange))))
            self.session.stdout.flush()

    @property
    def stats(self):
        if self._stats is None:

            video_sizes = {}
            for param in self.paramrange:
                mprobe = skvideo.io.mprobe(self.videoPath(param))
                ffprobe = skvideo.io.ffprobe(self.videoPath(param))
                if mprobe:
                    video_sizes[param] = int(mprobe['Video']['Frame_count'])
                elif ffprobe:
                    video_sizes[param] = int(ffprobe['video']['@nb_frames'])
                else:
                    # TODO: dirty fix because skvideo.io's probing methods sometimes do not work
                    known_video_sizes = {
                        'coco': 122219,
                        'davis': 6208,
                        'pascal': 2913,
                        'segtrack': 1067,
                    }
                    video_sizes[param] = known_video_sizes[param]

            stats_shape = (sum(video_sizes.values()),)

            self._stats = np.zeros(stats_shape, self.stats_dtype)

            self._stats['homog'] = 1.0
            self._stats['pixels'] = self.resolution**2
            self._stats['type'] = self.session.short_name

            offset = 0
            # Empty statistics
            for i, param in enumerate(self.paramrange):
                profile_path = self.profPath(param)
                cache_path = self.cachePath(param)
                video_size = video_sizes[param]

                if os.path.exists(cache_path):
                    with open(cache_path, 'rb') as cache_file:
                        self._stats[offset:offset+video_size] = np.load(cache_file)
                else:
                    self._stats[offset:offset + video_size]['param'] = param

                    frame_index = offset
                    frame_begin = 0
                    last_kernel_name = ''
                    last_kernel_start = 0

                    for kernel_index, kernel_launch in enumerate(openCSV(profile_path)):
                        if kernel_launch['name'] is None:
                            continue

                        kernel_name = kernel_launch['name']
                        kernel_time = float(kernel_launch['duration'])
                        kernel_start = float(kernel_launch['start'])

                        # Start of frame: record time, and reset compute and memcpy accumulators
                        if re.search(r'frameBegin', kernel_name):
                            # TODO: Bug: frameBegin is issued by driver after first HtoD operation
                            frame_begin = last_kernel_start

                        # End of frame: calculate overhead and increment frame/parameter indices
                        if re.search(r'frameEnd', kernel_name):
                            compute_time = self._stats['C'][frame_index]
                            dtoh_time = self._stats['D'][frame_index]
                            htod_time = self._stats['U'][frame_index]
                            frame_time = kernel_start + kernel_time - frame_begin

                            self._stats['O'][frame_index] = frame_time - (compute_time + htod_time + dtoh_time)

                            frame_index += 1

                        # Account for memcpy DtoH operations
                        if re.search(r'memcpy DtoH', kernel_name):
                            self._stats['D'][frame_index] += kernel_time

                        # Account for memcpy HtoD operations
                        if re.search(r'memcpy HtoD', kernel_name):
                            self._stats['U'][frame_index] += kernel_time

                        # Account for memset operations
                        if re.search(r'memset', last_kernel_name):
                            memset_time = kernel_start - last_kernel_start
                            self._stats['M'][frame_index] += memset_time
                            self._stats['C'][frame_index] += memset_time

                        # Account for Analysis/PathCompression operation and increment iteration counter
                        if re.search(r'::(analysisKernel|UpdateBorders)', kernel_name):
                            if self._stats['i'][frame_index] == 0:
                                self._stats['f'][frame_index] += kernel_time
                            else:
                                self._stats['F'][frame_index] += kernel_time
                            self._stats['i'][frame_index] += 1
                            self._stats['C'][frame_index] += kernel_time

                        # Account for Prelabel/LocalMerge operation
                        if re.search('::prelabelKernel', kernel_name):
                            self._stats['I'][frame_index] += kernel_time
                            self._stats['C'][frame_index] += kernel_time

                        # Account for Scan/GlobalMerge operation
                        if re.search(r'::(scanKernel|MergeTiles)', kernel_name):
                            if self._stats['i'][frame_index] == 0:
                                self._stats['j'][frame_index] += kernel_time
                            else:
                                self._stats['J'][frame_index] += kernel_time
                            self._stats['C'][frame_index] += kernel_time

                        # Account for SequentialRelabeling operation
                        if re.search(r'thrust::|::(markRoots|relabel)Kernel', kernel_name):
                            self._stats['R'][frame_index] += kernel_time
                            self._stats['C'][frame_index] += kernel_time

                        # Account for PropertiesSum operation
                        if re.search(r'::transferPropsKernel', kernel_name):
                            self._stats['P'][frame_index] += kernel_time
                            self._stats['C'][frame_index] += kernel_time

                        last_kernel_name = kernel_name
                        last_kernel_start = kernel_start

                    if self.properties in ['yes', 'late']:
                        dump_file = open(self.dumpPath(param))
                        dump_file.readline()

                        for j, line in enumerate(dump_file):
                            self._stats['ccsize'][offset + j] = []
                            if line:
                                sizes = [int(x) for x in line.rstrip().split(',') if x]

                                self._stats['cccount'][offset + j] = len(sizes)
                                self._stats['ccsize'][offset + j].extend(sizes)
                                self._stats['fg'][offset + j] = 1.0 * np.sum(sizes) / self.resolution**2
                                if sizes:
                                    self._stats['avg_ccsize'][offset + j] = np.mean(sizes)
                                    self._stats['max_ccsize'][offset + j] = np.max(sizes)
                                else:
                                    self._stats['avg_ccsize'][offset + j] = 0
                                    self._stats['max_ccsize'][offset + j] = 0

                    with open(cache_path, 'wb') as cache_file:
                        np.save(cache_file, self._stats[offset:offset+video_size])

                offset += video_size

        return self._stats

    def videoPath(self, param):
        return '{}/{}.avi'.format(self.session.VIDEODIR, param)


class VideoProfile(cuccl.profile.Profile):
    # TODO: change this to a non working dir-dependent value
    VIDEODIR = os.getcwd() + '/profile/data'

    element_class = VideoProfileElement
    filepattern = '-'.join([
        'video-{{}}',
        'resol:{resolution}',
        'alg:{algorithm}',
        'props:{properties}-s'
    ])
    name = 'Videos'
    options_class = VideoProfilingOptions
    parameter_name = 'Video dataset'
    short_name = 'videos'

    def gen_plots(self):
        # TODO: Change this for model fitting
        prediction = {}
        for itr in self.options.only_iteration:
            options = cuccl.profile.patterns.instancing.InstancingProfile.options_class(
                resolutions=self.resolutions,
                algorithms=self.algorithms,
                properties=self.properties,
                level=itr,
                subdivision=self.options.subdivision_level
            )
            prediction[itr] = cuccl.profile.patterns.instancing.InstancingProfile(options)
            prediction[itr].run()
            
        for res in self.resolutions:
            # ==============================================================================
            # Execution time / prediction scatter plots (colored by number of iterations)
            # ==============================================================================
            rows = len(self.properties)
            cols = len(self.algorithms)
    
            colors = {1: '#c22326', 2: '#f37338', 3: '#fdb632', 4: '#027878', 5: '#801638'}
            styles = {1: ':', 2: ':', 3: '-.', 4: '--', 5: '-'}
            pointsize = 4
            alpha = 0.3
    
            all_time_errors = {}
            all_time_values = {}
    
            # ------------------------------
            # Scatter plot of execution time
            # ------------------------------
            figure_time, axes_time = cuccl.profile.plotting.subplots(rows, cols, mode='single', aspect=1.0, sharex=True, sharey=True)
            for i, props in enumerate(self.properties):
                for j, alg in enumerate(self.algorithms):
                    ref = (res, 'le', props)
                    key = (res, alg, props)
    
                    all_time_errors[key] = []
                    all_time_values[key] = []
    
                    # Filtering by number of LE iterations
                    for itr in self.options.only_iteration:
                        mask = (self[ref].stats['i'] == itr)
    
                        # Filtering by random sampling of points
                        mask = np.bitwise_and(mask, npr.random(len(self[ref].stats['i'])) <= self.options.video_sampling)
    
                        # Filtering by number of connected components
                        mask = np.bitwise_and(
                            mask,
                            np.bitwise_and(
                                self[key].stats['cccount'] >= self.options.cccount_range[0],
                                self[key].stats['cccount'] <= self.options.cccount_range[1]
                            )
                        )
    
                        if self.options.show_title:
                            axes_time[0][j].set_title(alg.upper(), fontsize=10)
    
                        # As a function of total foreground occupancy
                        # mask = np.bitwise_and(mask, self[key].stats['fg'] <= 0.2)
                        xvals = self[key].stats['fg'][mask]
                        xest = np.mean(prediction[itr][res, alg, props].stats['fg'], axis=1)
    
                        # As a function of connected component count
                        # xvals = self[key].stats['cccount'][mask]
                        # xest = np.mean(prediction[itr][res, alg, props].stats['cccount'], axis=1)
    
                        # As a function of average component size
                        # xvals = self[key].stats['avg_ccsize'][mask]
                        # xest = np.mean(prediction[itr][res, alg, props].stats['avg_ccsize'], axis=1)
    
                        # As a function of the largest component's size
                        # xvals = self[key].stats['max_ccsize'][mask]
                        # xest = np.mean(prediction[itr][res, alg, props].stats['max_ccsize'], axis=1)

                        if len(xvals) == 0:
                            continue

                        yvals = self[key].stats['C'][mask] / 1000
                        yest = np.mean(prediction[itr][res, alg, props].stats['C'], axis=1) / 1000

                        prediction_function = sci.interp1d(xest, yest, fill_value='extrapolate')

                        all_time_values[key].extend(yvals)
                        all_time_errors[key].extend(prediction_function(xvals) - yvals)

                        axes_time[i][j].scatter(xvals, yvals, pointsize, colors[itr], alpha=alpha, marker='o', edgecolor='none', label='%d iter.' % itr)
                        axes_time[i][j].plot(xest, yest, color='k', linestyle=styles[itr])

                        # Prediction using estimated model parameters for Early LE on 4096 resolution
                        # f = cuccl.profile.scripts.model_fitting.func
                        # xest = np.vstack([
                        #     self[key].stats['i'][mask],
                        #     self[key].stats['fg'][mask],
                        #     self[key].stats['cccount'][mask],
                        #     self[key].stats['avg_ccsize'][mask],
                        #     self[key].stats['max_ccsize'][mask],
                        # ])
                        # const = [
                        #     1.32008226804, # const
                        #     2.54e-01, # i
                        #     7.96e-07, # fg
                        #     1.12e-04, # cccount
                        #     1.40e+02, # avg_ccsize
                        #     -1.40e+02, # max_ccsize
                        # ]
                        # const = [
                        #     7.11152748755, # const
                        #     1.34e-06, # fg
                        # ]
                        # const = [
                        #     3.56439495229, #const
                        #     9.12e-07, #fg
                        #     7.80e-07, #avg_ccsize
                        # ]
                        #
                        # yest = f(xest, *const)
                        #
                        # all_time_values[key].extend(yvals)
                        # all_time_errors[key].extend(yest - yvals)
                        # axes_time[i][j].scatter(xvals, yest - yvals, pointsize, colors[itr], alpha=alpha, marker='o', edgecolor='none', label='%d iter.' % itr)
                        # axes_time[i][j].plot(xest, yest, color='k', linestyle=styles[itr])
    
                    # axes_time[i][j].set_xlim(0, 0.2)
                    # axes_time[i][j].set_ylim(*self.options.ylim[res])
                    axes_time[i][j].grid()
    
                    if self.options.show_title:
                        axes_time[0][j].set_title(alg.upper(), fontsize=10)
    
                    if self.options.show_xlabels:
                        axes_time[-1][j].set_xlabel(r'\% of foreground', fontsize=10)
    
                    if self.options.show_ylabels:
                        if props == 'no':
                            axes_time[i][0].set_ylabel(r'Ordinary performance (ms)')
                        elif props == 'late':
                            axes_time[i][0].set_ylabel(r'Late performance (ms)')
                        elif props == 'yes':
                            axes_time[i][0].set_ylabel(r'Early performance (ms)')
    
            cuccl.profile.plotting.savefig(
                figure_time,
                'profile/out/plots/videos_time:%s-resol:%d' % (','.join(sorted(self.videos)), res),
                onlypdf=True,
            )
            plt.close(figure_time)
    
            # ------------------------------------------------
            # Normalized histogram of estimated time deviation
            # ------------------------------------------------
            figure_error, axes_error = cuccl.profile.plotting.subplots(rows, cols, mode='single', aspect=1.0, sharex=True, sharey=True)
            for i, props in enumerate(self.properties):
                for j, alg in enumerate(self.algorithms):
                    key = (res, alg, props)

                    if len(all_time_errors[key]) == 0:
                        continue

                    relative_error = 100 * np.divide(all_time_errors[key], all_time_values[key])
    
                    axes_error[i][j].hist(relative_error, 50)
    
                    this_xlim = np.max(np.abs([np.min(relative_error), np.max(relative_error)]))
                    axes_error[i][j].set_xlim(-this_xlim, this_xlim)
                    # axes_error[i][j].set_xlim(-3, 3)
                    axes_error[i][j].grid()
                    
                    if self.options.show_title:
                        axes_error[0][j].set_title(alg.upper(), fontsize=10)
    
                    if self.options.show_xlabels:
                        axes_error[-1][j].set_xlabel(r'Relative error (%)')
    
                    if self.options.show_ylabels:
                        if props == 'no':
                            axes_error[i][0].set_ylabel(r'Ordinary incidence')
                        elif props == 'late':
                            axes_error[i][0].set_ylabel(r'Late incidence')
                        elif props == 'yes':
                            axes_error[i][0].set_ylabel(r'Early incidence')
    
            cuccl.profile.plotting.savefig(
                figure_error,
                'profile/out/plots/videos_error:%s-resol:%d' % (','.join(sorted(self.videos)), res),
                onlypdf=True,
            )
            plt.close(figure_error)
    
            # ------
            # Legend
            # ------
            figure_legend, axes_legend = cuccl.profile.plotting.subplots(1, 1, mode='single', aspect=0.05, frameon=False)
            axes_legend = axes_legend[0, 0]
            for itr in range(1, 6):
                axes_legend.scatter([], [], pointsize, colors[itr], marker='o', edgecolor='none', label='%d iter.' % itr)
                axes_legend.plot([], [], color='k', linestyle=styles[itr], label='%d iter.' % itr)
            axes_legend.xaxis.set_visible(False)
            axes_legend.yaxis.set_visible(False)
            axes_legend.axis('off')
            legend = axes_legend.legend(loc='upper left', scatterpoints=1, ncol=5)
    
            cuccl.profile.plotting.savefig(
                figure_legend,
                'profile/out/plots/videos-legend',
                legend,
                onlypdf=True,
            )
            plt.close(figure_legend)
    
            # ----------
            # Histograms
            # ----------
            if key != 'no':
                figure_hist, axes_hist = cuccl.profile.plotting.subplots(1, 4, mode='double', aspect=1.0)
                axes_hist = axes_hist[0]
    
                key = (res, 'le', props)
    
                axes_hist[0].hist(100 * self[key].stats['fg'])
                axes_hist[0].set_xlabel(r'(a) Fg. occupancy (\%)')
                axes_hist[0].grid()
                for tick in axes_hist[0].get_xticklabels():
                    tick.set_rotation(30)
    
                axes_hist[1].hist(self[key].stats['cccount'][self[key].stats['cccount'] < 30])
                axes_hist[1].set_xlabel('(b) CC count')
                axes_hist[1].grid()
    
                axes_hist[2].hist(100 * np.hstack(self[key].stats['ccsize']) / res ** 2)
                axes_hist[2].set_xlabel(r'(c) CC size (fg. \%)')
                axes_hist[2].grid()
                for tick in axes_hist[2].get_xticklabels():
                    tick.set_rotation(30)
    
                axes_hist[3].hist(self[key].stats['i'], [0.5, 1.5, 2.5, 3.5, 4.5, 5.5])
                axes_hist[3].set_xlabel('(d) LE iterations')
                axes_hist[3].grid()
    
                cuccl.profile.plotting.savefig(
                    figure_hist,
                    'profile/out/plots/videos_hist:%s-resol:%d' % (','.join(sorted(self.videos)), res)
                )
                plt.close(figure_hist)
    
    def print_stats(self):
        linefmt = '    %20r | %4s : %4s | %4.1f | %4.1f | %4.1f'
        headfmt = '    %20s | %4s : %4s | %4s | %4s | %4s'
        header = headfmt % ('Parameters', 'p1', 'p0', 'min', 'max', 'avg')
    
        print '  Resolution ratios:'
        print header
        print '    ' + '-' * (len(header) - 4)
        combinations = list(it.product(self.algorithms, self.properties))
        for combo in combinations:
            a, p = combo
            r0 = self.resolutions[0]
            for r1 in self.resolutions[1:]:
                y0 = self[r0, a, p].stats['C']
                y1 = self[r1, a, p].stats['C']
                n = min(len(y1), len(y0))
                y0 = y0[:n]
                y1 = y1[:n]
                print linefmt % (combo, r1, r0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))
    
        print '  Algorithm ratios:'
        print header
        print '    ' + '-' * (len(header) - 4)
        combinations = list(it.product(self.resolutions, self.properties))
        for combo in combinations:
            r, p = combo
            a0 = self.algorithms[0]
            for a1 in self.algorithms[1:]:
                y0 = self[r, a0, p].stats['C']
                y1 = self[r, a1, p].stats['C']
                n = min(len(y1), len(y0))
                y0 = y0[:n]
                y1 = y1[:n]
                print linefmt % (combo, a1, a0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))


if __name__ == '__main__':
    args = VideoProfile.options_class.get()
    
    profile = VideoProfile(args)
    
    profile.run()

    if args.just_profile:
        sys.exit(0)

    profile.gen_plots()
    profile.print_stats()
