# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

"""Profiles various Label Equivalence Scan neighborhood types in the CuCCL library.
"""

import copy
import cuccl.profile.image
import cuccl.profile.patterns
import math
import numpy as np
import tqdm

# =============================================================================
# Single Level 1 Ridge Pattern subdivided recursively.
# =============================================================================
import cuccl.profile.plotting

args = cuccl.profile.ProfilingOptions.get()

resol_proggress = tqdm.tqdm(args.resolutions)

for resolution in resol_proggress:
    resol_proggress.set_description('Profiling scan modes; resol={}'.format(resolution))

    maxdiv = int(np.log(resolution) / np.log(2)) - 3

    commonparams = {
        'resolution' : resolution,
        'repeat'     : 100,
        'algorithm'  : 'le',
        'paramrange' : np.arange(0, maxdiv + 1),
        'shapefunc'  : cuccl.profile.patterns.multiridge(1),
    }

    p4_params = copy.copy(commonparams)
    f4_params = copy.copy(commonparams)
    f8_params = copy.copy(commonparams)

    p4_params.update({
        'filepattern': 'scanmode_subdivision-resol:%d-mode:p4-subdiv:%%d' % resolution,
        'additionalparams': '--pruneNeighborsInFirstScan=true --useNwNeighborsInFirstcan=false --properties=no'
    })

    f4_params.update({
        'filepattern': 'scanmode_subdivision-resol:%d-mode:f4-subdiv:%%d' % resolution,
        'additionalparams': '--pruneNeighborsInFirstScan=false --useNwNeighborsInFirstcan=true --properties=no'
    })

    f8_params.update({
        'filepattern': 'scanmode_subdivision-resol:%d-mode:f8-subdiv:%%d' % resolution,
        'additionalparams': '--pruneNeighborsInFirstScan=false --useNwNeighborsInFirstcan=false --properties=no'
    })

    cuccl.profile.image.profile(**p4_params)
    cuccl.profile.image.profile(**f4_params)
    cuccl.profile.image.profile(**f8_params)

    p4_stats = cuccl.profile.image.stats(**p4_params)
    f4_stats = cuccl.profile.image.stats(**f4_params)
    f8_stats = cuccl.profile.image.stats(**f8_params)

    paramrange = commonparams['paramrange']

    fig, axes = cuccl.profile.plotting.subplots(1, 2, scale=1.0, aspect=1.2, sharey=True)
    ax1, ax2 = axes[0]
    ax1.plot(paramrange, p4_stats['j'] / 1000, marker='o', label='P4')
    ax1.plot(paramrange, f4_stats['j'] / 1000, marker='o', label='F4')
    ax1.plot(paramrange, f8_stats['j'] / 1000, marker='o', label='F8')
    ax1.legend(loc='upper left')
    ax1.set_xlabel('(a) Subdivisions')
    ax1.set_ylabel(r'Kernel time (ms)')
    ax1.grid()

    # ==============================================================================
    # Increasing instances of a Level 1 Ridge Pattern recursively subdivided a fixed
    # number of times.
    # ==============================================================================

    # maxdiv = int(np.log(resolution) / np.log(2)) - 4
    step = 4**maxdiv / 16

    commonparams = {
        'resolution' : resolution,
        'repeat'     : 10,
        'algorithm'  : 'le',
        'paramrange' : np.arange(step, 4**maxdiv+1, step),
        'shapefunc'  : cuccl.profile.patterns.multiridge_n(1, maxdiv),
    }

    p4_params = copy.copy(commonparams)
    f8_params = copy.copy(commonparams)
    f4_params = copy.copy(commonparams)

    p4_params.update({
        'filepattern': 'scanmode_instancing-resol:%d-mode:p4-inst:%%d' % resolution,
        'additionalparams': '--pruneNeighborsInFirstScan=true --useNwNeighborsInFirstcan=false --properties=no'
    })

    f4_params.update({
        'filepattern': 'scanmode_instancing-resol:%d-mode:f4-inst:%%d' % resolution,
        'additionalparams': '--pruneNeighborsInFirstScan=false --useNwNeighborsInFirstcan=true --properties=no'
    })

    f8_params.update({
        'filepattern': 'scanmode_instancing-resol:%d-mode:f8-inst:%%d' % resolution,
        'additionalparams': '--pruneNeighborsInFirstScan=false --useNwNeighborsInFirstcan=false --properties=no'
    })

    cuccl.profile.image.profile(**p4_params)
    cuccl.profile.image.profile(**f8_params)
    cuccl.profile.image.profile(**f4_params)

    p4_stats = cuccl.profile.image.stats(**p4_params)
    f8_stats = cuccl.profile.image.stats(**f8_params)
    f4_stats = cuccl.profile.image.stats(**f4_params)

    paramrange = commonparams['paramrange']

    ax2.plot(paramrange, p4_stats['j'] / 1000, label='P4')
    ax2.plot(paramrange, f8_stats['j'] / 1000, label='F8')
    ax2.plot(paramrange, f4_stats['j'] / 1000, label='F4')
    ax2.legend(loc='upper left')
    ax2.set_xlabel(r'(b) Instances ($\times 2^{%d})' % math.log(step, 2))
    ax2.set_xticks(paramrange[::2])
    ax2.set_xticklabels(['%d' % (p / step) for p in paramrange[::2]])
    ax2.grid()

    cuccl.profile.plotting.savefig(fig, 'profile/out/plots/scanmode-resol:%d' % resolution)
