# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2018 Pedro de Souza Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/lcg/cuccl/>.

# TODO: This script is still not minimally conformant with the future API

"""Profiles various Label Equivalence Scan flag modes in the CuCCL library.
"""

import copy
import cuccl.profile.image
import math
import numpy as np
import tqdm

from cuccl.profile.patterns import ridge, multiridge_n

if __name__ == '__main__':
    args = cuccl.profile.ProfilingOptions.get()

    # =============================================================================
    # Single Ridge Pattern with increasing level
    # =============================================================================
    resol_proggress = tqdm.tqdm(args.resolutions)

    for resolution in args.resolutions:
        resol_proggress.set_description('Profiling scan modes; resol={}'.format(resolution))

        commonparams = {
            'resolution' : resolution,
            'repeat'     : 100,
            'algorithm'  : 'le',
            'paramrange' : np.arange(1, int(math.log(resolution, 2))),
            'shapefunc'  : ridge,
        }

        global_params = copy.copy(commonparams)
        local_params = copy.copy(commonparams)

        global_params.update({
            'filepattern'     : 'scanflag_leveling-resol:%d-flag:global-level:%%d' % resolution,
            'additionalparams': '--useLocalFlagInScan=false --properties=no'
        })

        local_params.update({
            'filepattern'     : 'scanflag_leveling-resol:%d-flag:local-level:%%d' % resolution,
            'additionalparams': '--useLocalFlagInScan=true --properties=no'
        })

        cuccl.profile.image.profile(**global_params)
        cuccl.profile.image.profile(**local_params)

        global_stats = cuccl.profile.image.stats(**global_params)
        local_stats = cuccl.profile.image.stats(**local_params)

        paramrange = commonparams['paramrange']

        fig, axes = cuccl.profile.subplots(1, 2, scale=1.0, mode='single', aspect=1.2, sharey=True)
        ax1, ax2 = axes[0]

        ax1.plot(paramrange, global_stats['J'] / 1000, 'b-', label='Global')
        ax1.plot(paramrange, local_stats['J']  / 1000, 'g-', label='Local')
        ax1.legend(loc='upper left')
        ax1.set_xlabel(r'(a) Ridge level')
        ax1.set_ylabel(r'Processing time (ms)')
        # ax1.set_title('Performance of later scans\nvs ridge Leveling')
        ax1.grid()

        # =============================================================================
        # Increasing number of instances of a Level 5 Ridge Pattern subdivided
        # recursively.
        # =============================================================================

        subdiv = int(math.log(resolution, 2) - 1) - 5
        step = 4**subdiv/16

        commonparams = {
            'resolution' : resolution,
            'repeat'     : 10,
            'algorithm'  : 'le',
            'paramrange' : np.arange(step, 4**subdiv+1, step),
        }

        global5_params = copy.copy(commonparams)
        global3_params = copy.copy(commonparams)
        local5_params = copy.copy(commonparams)
        local3_params = copy.copy(commonparams)

        global5_params.update({
            'shapefunc'  : multiridge_n(5, subdiv),
            'filepattern'     : 'scanflag_instancing-resol:%d-flag:global-level:5-inst:%%d' % resolution,
            'additionalparams': '--useLocalFlagInScan=false --properties=no'
        })

        global3_params.update({
            'shapefunc'  : multiridge_n(3, subdiv),
            'filepattern'     : 'scanflag_instancing-resol:%d-flag:global-level:3-inst:%%d' % resolution,
            'additionalparams': '--useLocalFlagInScan=false --properties=no'
        })

        local5_params.update({
            'shapefunc'  : multiridge_n(5, subdiv),
            'filepattern'     : 'scanflag_instancing-resol:%d-flag:local-level:5-instancs:%%d' % resolution,
            'additionalparams': '--useLocalFlagInScan=true --properties=no'
        })

        local3_params.update({
            'shapefunc'  : multiridge_n(3, subdiv),
            'filepattern'     : 'scanflag_instancing-resol:%d-flag:local-level:3-instancs:%%d' % resolution,
            'additionalparams': '--useLocalFlagInScan=true --properties=no'
        })

        cuccl.profile.image.profile(**global5_params)
        cuccl.profile.image.profile(**global3_params)
        cuccl.profile.image.profile(**local5_params)
        cuccl.profile.image.profile(**local3_params)

        global5_stats = cuccl.profile.image.stats(**global5_params)
        global3_stats = cuccl.profile.image.stats(**global3_params)
        local5_stats = cuccl.profile.image.stats(**local5_params)
        local3_stats = cuccl.profile.image.stats(**local3_params)

        paramrange = commonparams['paramrange']

        ax2.plot(paramrange, global3_stats['J'] / 1000, 'b^-', markeredgewidth=0.0, label='L3 global')
        ax2.plot(paramrange, local3_stats ['J'] / 1000, 'g^-', markeredgewidth=0.0, label='L3 local' )
        ax2.plot(paramrange, global5_stats['J'] / 1000, 'bp-', markeredgewidth=0.0, label='L5 global')
        ax2.plot(paramrange, local5_stats ['J'] / 1000, 'gp-', markeredgewidth=0.0, label='L5 local' )
        ax2.set_xticks(paramrange[::2])
        ax2.set_xticklabels(['%d' % (p / step) for p in paramrange[::2]])
        ax2.legend(loc='upper left', numpoints=1, fontsize=6, markerscale=1.00)
        ax2.set_xlabel(r'(b) Instances ($\times 2^{%d}$)' % math.log(step, 2))
        # ax2.set_title('Performance of later scans\nvs ridge instancing')
        ax2.grid()

        cuccl.profile.savefig(fig, 'profile/out/plots/scanflag-resol:%d' % resolution)
