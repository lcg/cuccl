#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# CuCCL - Connected Component Labeling in CUDA
# Copyright (C) 2018  Pedro Asad
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or suggestions regarding this software, visit
# <http://gitlab.com/lcg/cuccl/>.

import os
import sys
thisdir = os.path.dirname(__file__)
sys.path.append(os.path.join(thisdir, 'src'))
import cuccl
from setuptools import find_packages, setup


def get_authors():
    return 'Pedro Asad'


def get_author_emails():
    return 'pasad@lcg.ufrj.br'


def get_readme():
    with open(os.path.join(thisdir, 'README.md')) as readme_file:
        return readme_file.read()
    return ''


setup(
    name='cuccl',
    version=cuccl.version.short,
    license='GPL-3.0',
    description='Python 2.7 wrapper for the CuCCL (Connected Components Labeling in CUDA) library.',
    long_description=get_readme(),
    url='http://lcg.gitlab.io/cuccl',

    author=get_authors(),
    author_email=get_author_emails(),

    keywords='gpu,cuda,connected components',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Environment :: X11 Applications',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Operating System :: Unix',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries',
        'Topic :: System :: Benchmark',
    ],

    package_dir={
        '': 'src',
    },
    packages=find_packages('src'),

    python_requires='>=2.7, <3',
    install_requires=[
        'frozendict>=1.2',
        'matplotlib>=2.1',
        'numpy>=1.14',
        'pandas>=0.22.0',
        'scipy>=1.0.0',
        'scikit-image>=0.13.1',
        'scikit-video>=1.1.10',
        'tqdm>=4.23.4',
    ],
)
