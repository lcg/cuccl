CuCCL profiling
===============

This package contains profiling tools for the [CuCCL][cuccl] library. 
It is not a Python wrapper around the [CuCCL][cuccl] library (yet).
This is not meant for standalone installation: you should download and install the library, then install the corresponding version of this package from the `python` subdirectory.

---

# Installation

Make sure you have OpenCV 2.4 installed, along with Python bindings.
Since Python bindings for this version of OpenCV can not be installed using `pip`, the `setup.py` file does not specify OpenCV 2.4 as an installation dependency, and you'll have to make sure things work by yourself.
In Ubuntu, it suffices to install the `python-opencv` package. 
You can test that OpenCV 2.4 Python bindings are installed by typing `import cv2` in a Python2.7 shell.

The CUDA/C++ library currently works only with fixed resolutions, so pick a target resolution for the first compilation.
The profiling scripts will recompile the library for other resolutions as need. 
Assuming you're in the root of the repository, you can do

```bash
mkdir build && cd build
cmake .. \
    -DCMAKE_INSTALL_PREFIX=$HOME/.local \  # Assuming a local installation
    -DBUILD_DOCS=0 \                       # Prevent building docs
    -DRESOLUTION=1024                      # Choose any resolution
make && make install
```

Finally, from the same directory as this README file, install this Python package with:

```bash
pip install --user .  # This assumes a local (user) installation. You may use a virtualenv instead. 
```

After that, your package should be importable from Python 

```python2.7
import cuccl.profile
```

Note that the top-level `cuccl` package is importable, but it doesn't contain anything useful yet.

---

# Profiling

The currently available profiling scripts are:

* [`cuccl.profile.scripts.instance_prob`](python/cuccl/profile/scripts/instance_prob.py)
* [`cuccl.profile.scripts.instancing`](python/cuccl/profile/scripts/instancing.py)
* [`cuccl.profile.scripts.leveling`](python/cuccl/profile/scripts/leveling.py)
* [`cuccl.profile.scripts.memprof`](python/cuccl/profile/scripts/memprof.py)
* [`cuccl.profile.scripts.merge_prob`](python/cuccl/profile/scripts/merge_prob.py)
* [`cuccl.profile.scripts.scaling`](python/cuccl/profile/scripts/scaling.py)
* [`cuccl.profile.scripts.scanflag`](python/cuccl/profile/scripts/scanflag.py)
* [`cuccl.profile.scripts.scanmodes`](python/cuccl/profile/scripts/scanmodes.py)
* [`cuccl.profile.scripts.seqprof`](python/cuccl/profile/scripts/seqprof.py)
* [`cuccl.profile.scripts.spiral`](python/cuccl/profile/scripts/spiral.py)
* [`cuccl.profile.scripts.subdivision`](python/cuccl/profile/scripts/subdivision.py)
* [`cuccl.profile.scripts.videos`](python/cuccl/profile/scripts/videos.py)

Check their docstrings or run them with the `--help` switch to get more details on running them.
Notice that plot generation only takes place after the profiling is finished.
If no profiling files are missing for a specific invocation, plotting is performed.
If you wish to suppress plot generation and just profile the library, run the scripts with the `--just-profile` flag.

When profiling, **make sure:** 
* To run the profile scripts from the directory that contains the `build` directory where the sources where compiled with CMake (it will be called again to recompile for desired resolutions)
* To have a directory path `profile/out` accessible from the current directory (plots, CSV files, etc. will be placed in subdirectories of it)

The NVIDIA profiler is currently hard-configured to the `/usr/local/cuda/bin/nvprof` path.
If your NVIDIA toolkit is installed with different settings, you'll have to change the `PROFILER` global variable in the [`cuccl.profile.image`](python/src/cuccl/profile/image.py) and [`cuccl.profile.video`](python/src/cuccl/profile/video.py) modules.

## Examples

Profiling and plotting performance with Ridge Pattern instancing in resolutions 1024, 2048, 4096, and 8192, using 50 repetitions per pattern:

```bash
python -m cuccl.profile.scripts.instancing -r 1024 2048 4096 8192 -n 50
```

---

[CuCCL]: https://gitlab.com/lcg/cuccl
